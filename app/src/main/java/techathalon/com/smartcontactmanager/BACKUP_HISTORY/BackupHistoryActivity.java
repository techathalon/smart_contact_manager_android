package techathalon.com.smartcontactmanager.BACKUP_HISTORY;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.HELPER_FILES.HelperActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.HttpHelper1;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class BackupHistoryActivity extends HelperActivity implements View.OnClickListener {
    private LinearLayout lin_previous_backup;
    private SharedPreferences pref, passwordCheck, emailPrefs, userDataPrefs;
    private SharedPreferences.Editor userDataPrefsEditor;
    private String user_id, email_id;
    String emailSentEmailId = "", backup_id = "";
    private LayoutInflater lyt_inflater;
    private ImageView img_slide_in, img_slide_out;
    View lyt_view;
    private ViewFlipper activeFlipper;
    private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
    private boolean pendingPublishReauthorization = false;
    private int totalBackup = 0;
    private String downloadcb_id = "", delete_backup_id = "";
    JSONArray downloadJsonArray;
    Bundle bundle;
    DownloadFile downloadfile;
    JSONObject innerObject;
    int CHECK_PASSWORD_NUMBER = 0, check = 0, count = 0;
    Dialog downloadingDialog;
    LinearLayout noBackupAvailable, historyView;
    JSONObject jsonObject;
    JSONArray jsonArray, historyData;
    HashMap<String, String> hashMap;
    HttpHelper1 httpHelper1;

    AlarmHandler alarmHandler;
    ArrayList<AlarmData> alarmDataArrayList;
    AlarmData alarmData;

    Calendar currentTime, endTime;
    Date dateObject;
    SimpleDateFormat dateFormatObject;
    AlertDialog.Builder builder;
    AlertDialog alert;

    AdView adView1, adView2, adView3;
    AdRequest adRequest;

    @Override
    public void setBackApiResponse1(String url, String object) {
        super.setBackApiResponse1(url, object);
        if (url.equals(Constants.RESEND_EMAIL_URL)) {
            if (object != null && !object.equals("")) {
                try {
                    JSONObject jsonObject = new JSONObject(object);
                    String success = jsonObject.optString("success");
                    if (success.equalsIgnoreCase("true")) {
                        Toast.makeText(this, "Email has been sent to " + emailSentEmailId, Toast.LENGTH_LONG).show();
                        switch (check) {
                            case 1:
                                updateDBData1();
                                break;
                            case 2:
                                updateDBData2();
                                break;
                            case 3:
                                updateDBData3();
                                break;
                        }
                    } else {
                        Toast.makeText(this, "Email not sent", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backup_history_constraint);
        pref = getSharedPreferences("data", Context.MODE_PRIVATE);
        userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
        if(Constants.enable_tracker){
            SCMApplication.getInstance().trackScreenView("Backup History");
        }

        //TODO Here is the google ads enable the constant to true in Constant file in project
        if (Constants.enable_ads) {
            adView1 = findViewById(R.id.adView1);
            adView2 = findViewById(R.id.adView2);
            adView3 = findViewById(R.id.adView3);
            adRequest = new AdRequest.Builder().build();
            adView1.loadAd(adRequest);
            adView2.loadAd(adRequest);
            adView3.loadAd(adRequest);
        }

        emailPrefs = getSharedPreferences("email data", Context.MODE_PRIVATE);
        user_id = userDataPrefs.getString("user_id", "");
        email_id = userDataPrefs.getString("registered_email", "");
        lin_previous_backup = findViewById(R.id.lin_previous_backup);
        lyt_inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        passwordCheck = getSharedPreferences("LOGIN", 0);
        noBackupAvailable = findViewById(R.id.no_backup_available_textview);
        historyView = findViewById(R.id.history_view);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        if (getIntent().getExtras() != null) {
            bundle = getIntent().getExtras();
            String data = bundle.getString("history");
            try {
                historyData = new JSONArray();
                jsonObject = new JSONObject(data);
                jsonArray = jsonObject.getJSONArray("file_backup");

                for (int j = 0; j < jsonArray.length(); j++) {
                    historyData.put(jsonArray.getJSONObject(j));
                    if (j == 2)
                        break;
                }
                setPreviousBackupData(historyData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void setPreviousBackupData(final JSONArray arrFiles) {
        if (arrFiles.length() > 0) {
            downloadJsonArray = arrFiles;
            String last_backup_date = "";
            lin_previous_backup.removeAllViews();
            totalBackup = arrFiles.length();
            for (int i = 0; i < arrFiles.length(); i++) {
                lyt_view = lyt_inflater.inflate(R.layout.history_inf, null);
                try {
                    innerObject = arrFiles.getJSONObject(i);
                    lyt_view = lyt_inflater.inflate(R.layout.history_inf_constraint, null);
                    lyt_view.setTag("View_" + innerObject.optString("cb_id"));
                    TextView tv_created_date = lyt_view.findViewById(R.id.txt_date);
                    if (i == 0)
                        last_backup_date = innerObject.optString("created_date");
                    String created_date = innerObject.optString("created_date");

//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
//                    Date date = null;
//                    try {
//                        date = sdf.parse(created_date);
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                    sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.US);
//                    tv_created_date.setText(sdf.format(date));

                    String local_date = null;
                    try {
                        local_date = convert_to_local_date(created_date);
                    } catch (ParseException e) {
                        local_date = "00-00-0000 00:00";
                        e.printStackTrace();
                    }

                    tv_created_date.setText(local_date);
                    TextView tv_email = lyt_view.findViewById(R.id.txt_email);
                    tv_email.setText(innerObject.optString("email_id"));
                    emailSentEmailId = innerObject.optString("email_id");
                    final String file_name = innerObject.optString("file_name");
                    TextView tv_file_name = lyt_view.findViewById(R.id.txt_file_name);
                    tv_file_name.setText("  " + file_name);

                    LinearLayout more_slider = lyt_view.findViewById(R.id.more_slider);
                    more_slider.setTag("slider_" + innerObject.optString("cb_id"));
                    final ViewFlipper slide_flipper = lyt_view.findViewById(R.id.slider_flipper);
                    slide_flipper.setTag("flipper_" + (i + 1000));
                    img_slide_in = lyt_view.findViewById(R.id.img_slide_in);
                    img_slide_out = lyt_view.findViewById(R.id.img_slide_out);
                    img_slide_in.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hidePreviousOne(arrFiles, activeFlipper);
                            slide_flipper.setInAnimation(BackupHistoryActivity.this, R.anim.in_from_right);
                            slide_flipper.setOutAnimation(BackupHistoryActivity.this, R.anim.out_to_right);
                            slide_flipper.setDisplayedChild(1);
                            activeFlipper = slide_flipper;
                        }
                    });
                    img_slide_out.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            slide_flipper.setInAnimation(BackupHistoryActivity.this, R.anim.in_from_left);
                            slide_flipper.setOutAnimation(BackupHistoryActivity.this, R.anim.out_to_right);
                            slide_flipper.setDisplayedChild(0);
                            activeFlipper = null;
                        }
                    });
                    final ImageView img_slide_download = lyt_view.findViewById(R.id.img_slide_download);
                    img_slide_download.setTag(innerObject.optString("cb_id"));
                    img_slide_download.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            slide_flipper.setInAnimation(BackupHistoryActivity.this, R.anim.in_from_left);
                            slide_flipper.setOutAnimation(BackupHistoryActivity.this, R.anim.out_to_right);
                            slide_flipper.setDisplayedChild(0);
                            activeFlipper = null;
                            downloadcb_id = img_slide_download.getTag().toString();

                            if (passwordCheck.getString("password", "").equalsIgnoreCase("")) {
                                Intent intent = new Intent(BackupHistoryActivity.this, PinActivity.class);
                                intent.putExtra("first time", "yes");
                                if (bundle != null) {
                                    intent.putExtras(bundle);
                                }
                                startActivityForResult(intent, CHECK_PASSWORD_NUMBER);
                            } else {
                                Intent intent = new Intent(BackupHistoryActivity.this, PinActivity.class);
                                intent.putExtra("check password", "check");
                                if (bundle != null) {
                                    intent.putExtras(bundle);
                                }
                                startActivityForResult(intent, CHECK_PASSWORD_NUMBER);
                            }
                        }
                    });
                    final ImageView img_slide_delete = lyt_view.findViewById(R.id.img_slide_delete);
                    img_slide_delete.setTag(innerObject.optString("cb_id"));
                    img_slide_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            slide_flipper.setInAnimation(BackupHistoryActivity.this, R.anim.in_from_left);
                            slide_flipper.setOutAnimation(BackupHistoryActivity.this, R.anim.out_to_right);
                            slide_flipper.setDisplayedChild(0);
                            activeFlipper = null;

                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(BackupHistoryActivity.this);
                            alertBuilder.setMessage("Are you sure you want to delete this file?");
                            alertBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    if (isNetworkAvailable()) {
                                        try {
                                            for (int i1 = 0; i1 < arrFiles.length(); i1++) {
                                                JSONObject jsonObject = arrFiles.getJSONObject(i1);
                                                String tag = img_slide_delete.getTag().toString();
                                                if (tag.equals(jsonObject.optString("cb_id"))) {
                                                    delete_backup_id = jsonObject.optString("cb_id");
                                                    deleteBackup(jsonObject.optString("cb_id"));
                                                    break;
                                                }
                                            }
                                            if (activeFlipper != null) {
                                                activeFlipper.setDisplayedChild(0);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                            alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog alert = alertBuilder.create();
                            alert.show();
                        }
                    });
                    final ImageView img_resend_email = lyt_view.findViewById(R.id.img_slide_resend);
                    img_resend_email.setTag(innerObject.optString("cb_id"));
                    img_resend_email.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            slide_flipper.setInAnimation(BackupHistoryActivity.this, R.anim.in_from_left);
                            slide_flipper.setOutAnimation(BackupHistoryActivity.this, R.anim.out_to_right);
                            slide_flipper.setDisplayedChild(0);
                            activeFlipper = null;
                            backup_id = img_resend_email.getTag().toString();
                            alarmHandler = new AlarmHandler(getApplicationContext());
                            if (alarmHandler.getAllRecords().size() == 0) {
                                resendEmail();
                                check = 1;
                            } else if (!alarmHandler.checkExists("backup_id", backup_id)) {
                                resendEmail();
                                check = 1;
                            } else {
                                alarmDataArrayList = alarmHandler.getSingleRecord(backup_id);
                                alarmData = alarmDataArrayList.get(0);
                                String alarm_fire_time = "";
                                count = Integer.parseInt(alarmData.getCount());
                                alarm_fire_time = alarmData.getAlarm_fire_time();
                                if (!alarm_fire_time.equals("")) {
                                    try {
                                        endTime = Calendar.getInstance();
                                        currentTime = Calendar.getInstance();

                                        dateFormatObject = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");
                                        dateObject = dateFormatObject.parse(alarm_fire_time);
                                        String sdsds = alarm_fire_time.substring(alarm_fire_time.indexOf(" "));
                                        sdsds = sdsds.replace(" ", "");
                                        sdsds = sdsds.substring(0, 2);
                                        endTime.setTime(dateObject);
                                        endTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sdsds));
                                        if (currentTime.compareTo(endTime) < 0) {
                                            if (count == 3) {
                                                Date date1, date2;
                                                date1 = endTime.getTime();
                                                date2 = currentTime.getTime();
                                                printDifference(date2, date1);
//                                                Toast.makeText(getApplicationContext(), "You have exceeded your limit", Toast.LENGTH_SHORT).show();
                                            } else if (count < 3) {
                                                resendEmail();
                                                check = 2;
                                            }
                                        } else if (currentTime.compareTo(endTime) > 0) {
                                            check = 3;
                                            resendEmail();
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    resendEmail();
                                    check = 2;
                                }
                            }
                        }
                    });
                    lin_previous_backup.addView(lyt_view);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Date date = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

            try {
                date = sdf.parse(last_backup_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sdf = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            SharedPreferences.Editor edit = pref.edit();
            edit.putString("last_backup_date", sdf.format(date));
            edit.apply();

            userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
            userDataPrefsEditor = userDataPrefs.edit();
            userDataPrefsEditor.putString("last_backup_date", sdf.format(date));
            userDataPrefsEditor.apply();
        } else {
            historyView.setVisibility(View.GONE);
            noBackupAvailable.setVisibility(View.VISIBLE);
        }
    }

    private void printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

//        System.out.println("startDate : " + startDate);
//        System.out.println("endDate : " + endDate);
//        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

//        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
//        different = different % minutesInMilli;

//        long elapsedSeconds = different / secondsInMilli;

//        System.out.printf(
//                "%d days, %d hours, %d minutes, %d seconds%n",
//                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setIcon(R.drawable.latestappicon);
        builder.setCancelable(false);
        builder.setMessage("You have exceeded you today's limit, now you can try after " + elapsedHours + " hour " +
                elapsedMinutes + " minutes later.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert = builder.create();
        alert.show();
    }

    private void resendEmail() {
        hashMap = new HashMap<>();
        hashMap.put("user_id", user_id);
        hashMap.put("cb_id", backup_id);
        httpHelper1 = new HttpHelper1(Constants.RESEND_EMAIL_URL, hashMap,
                BackupHistoryActivity.this, "Sending email please wait...");
    }

    private void updateDBData1() {
        alarmDataArrayList = new ArrayList<>();
        alarmData = new AlarmData();
        alarmData.setBackup_id(backup_id);
        alarmData.setCount("1");
        alarmData.setAlarm_fire_time("");
        alarmDataArrayList.add(alarmData);
        alarmHandler.syncDatabase(alarmDataArrayList);
    }

    private void updateDBData2() {
        Calendar calendar = null;
        String addedDateString = "";
        DateFormat dateFormat = null;
        if (count == 2) {
            calendar = Calendar.getInstance();
            Log.d("sampleerere", "current time " + calendar.getTime());
            dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");
            calendar.add(Calendar.HOUR, 24);
            Log.d("sampleerere", "added time " + calendar.getTime());
            Date addedDate = calendar.getTime();
            addedDateString = dateFormat.format(addedDate);
            Log.d("sampleerere", "added time " + addedDateString);
        }
        alarmDataArrayList = new ArrayList<>();
        AlarmData alarmData1 = new AlarmData();
        alarmData1.setBackup_id(backup_id);
        alarmData1.setCount((Integer.parseInt(alarmData.getCount()) + 1) + "");
        alarmData1.setAlarm_fire_time(addedDateString);
        alarmDataArrayList.add(alarmData1);
        alarmHandler.syncDatabase(alarmDataArrayList);
    }

    private void updateDBData3() {
        alarmDataArrayList = new ArrayList<>();
        AlarmData alarmData1 = new AlarmData();
        alarmData1.setBackup_id(backup_id);
        alarmData1.setCount(1 + "");
        alarmData1.setAlarm_fire_time("");
        alarmDataArrayList.add(alarmData1);
        alarmHandler.syncDatabase(alarmDataArrayList);
    }

    public void hidePreviousOne(JSONArray arrFiles, ViewFlipper active) {
        if (active != null) {
            active.setDisplayedChild(0);
        }
    }

    private static void createApplicationFolder() {
        File f = new File(Environment.getExternalStorageDirectory(), File.separator + "SCMContactBackup/");
        f.mkdirs();
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
    }

    private void deleteBackup(final String cb_id) {
        new AsyncTask<Void, String, String>() {
            @Override
            protected String doInBackground(Void... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("cb_id", cb_id));
                try {
                    HttpParams httpParameters = new BasicHttpParams();
                    HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
                    HttpConnectionParams.setSoTimeout(httpParameters, 20000);

                    DefaultHttpClient client = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Constants.DELETE_CONTACT_BACKUP_URL);
                    httppost.setParams(httpParameters);
                    HttpResponse response;
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    response = client.execute(httppost);
                    InputStream data = response.getEntity().getContent();

                    if (data != null) {
                        StringBuilder sb = new StringBuilder();
                        int b;
                        while ((b = data.read()) != -1) {
                            sb.append((char) b);
                        }
                        data.close();
                        return sb.toString();
                    } else {
                        return "";
                    }

                } catch (ConnectTimeoutException e) {
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            handler.sendEmptyMessage(0);
                        }
                    }).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            public void onPostExecute(String result) {
                if (result != null && !result.equals("")) {
                    try {
                        JSONObject data = new JSONObject(result);
                        if (data.optString("success").equalsIgnoreCase("true")) {
                            View tempView = null;
                            for (int i = 0; i < lin_previous_backup.getChildCount(); i++) {
                                View view = lin_previous_backup.getChildAt(i);
                                ImageView img_slide_delete = (ImageView) view.findViewById(R.id.img_slide_delete);
                                if (img_slide_delete.getTag().equals(cb_id)) {
                                    tempView = view;
                                    break;
                                }
                            }
                            lin_previous_backup.removeView(tempView);
                            alarmHandler = new AlarmHandler(getApplicationContext());
                            if (alarmHandler.checkExists("backup_id", delete_backup_id))
                                alarmHandler.delete(delete_backup_id);
                            Toast.makeText(getApplicationContext(), data.optString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    totalBackup--;
                    if (totalBackup <= 0) {
                        historyView.setVisibility(View.GONE);
                        noBackupAvailable.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.some_error_occurred_msg), Toast.LENGTH_LONG)
                            .show();
                }
            }

        }.execute();
    }

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            Toast.makeText(BackupHistoryActivity.this, "Please try again after some time", Toast.LENGTH_LONG).show();
        }
    };

    private class DownloadFile extends AsyncTask<String, Integer, Long> {
        String string, filename;
        String folder_path = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        public DownloadFile(String string, String filename) {
            this.string = string;
            this.filename = filename;
        }

        @Override
        protected Long doInBackground(String... params) {
            try {
                URL url = new URL(string);
                URLConnection conection = url.openConnection();
                conection.connect();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                createApplicationFolder();
                folder_path = Constants.folder_path;
                OutputStream output = new FileOutputStream(Constants.folder_path + filename);
                byte data[] = new byte[4096];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        public void onPostExecute(Long result) {
            if (downloadingDialog != null && downloadingDialog.isShowing())
            downloadingDialog.dismiss();
            showSnackBar(folder_path + filename, 1); //TODO Snackbar not working need to work
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
        finish();
    }

    private void displayDownloadDialog() {
        downloadingDialog = new Dialog(BackupHistoryActivity.this);
        if (downloadingDialog.getWindow() != null)
            downloadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        downloadingDialog.getWindow().setBackgroundDrawableResource(R.drawable.custom_loader_background);
        downloadingDialog.setCancelable(false);
        downloadingDialog.setContentView(R.layout.downloading_dialog);
        downloadingDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHECK_PASSWORD_NUMBER && resultCode == 1) {
//            String folder_path = getFolderPath(BackupHistoryActivity.this);
            final String folder_path = Constants.folder_path;
            String specific_folder = "'"+"SCMContactBackup"+"'"+" Folder In your memory";
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(BackupHistoryActivity.this);
//            alertBuilder.setMessage("Your backup file will be downloaded to " + folder_path);
            alertBuilder.setMessage("Your backup file will be downloaded to " + specific_folder);
            alertBuilder.setPositiveButton("Download", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    createApplicationFolder();
                    try {
                        for (int i1 = 0; i1 < downloadJsonArray.length(); i1++) {
                            final JSONObject jsonObject = downloadJsonArray.getJSONObject(i1);
                            if (downloadcb_id.equals(jsonObject.optString("cb_id"))) {
                                File mediaStorageDir = new File(Constants.folder_path);
                                if (!mediaStorageDir.exists()) {
                                    if (isNetworkAvailable()) {
                                        displayDownloadDialog();
                                        downloadfile = new DownloadFile(Constants.BASEURL + "public/uploads/contacts/"
                                                + jsonObject.optString("file_name"), jsonObject.optString("file_name"));
                                        downloadfile.execute("");
                                    } else {
                                        Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    File futureStudioIconFile = new File(Constants.folder_path + jsonObject.optString("file_name"));
                                    if (!futureStudioIconFile.exists()) {
                                        if (isNetworkAvailable()) {
                                            displayDownloadDialog();
                                            downloadfile = new DownloadFile(Constants.BASEURL + "public/uploads/contacts/"
                                                    + jsonObject.optString("file_name"), jsonObject.optString("file_name"));
                                            downloadfile.execute("");
                                        } else {
                                            Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        showSnackBar(Constants.folder_path + jsonObject.optString("file_name"), 2); //TODO Snackbar not working need to work
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();

        } else if (requestCode == CHECK_PASSWORD_NUMBER && resultCode == 5) {
//            String folder_path = getFolderPath(BackupHistoryActivity.this);
            String folder_path = Constants.folder_path;
            String specific_folder = "'"+"SCMContactBackup"+"'"+" Folder In your memory";
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(BackupHistoryActivity.this);
//            alertBuilder.setMessage("Your backup file will be downloaded to " + folder_path);
            alertBuilder.setMessage("Your backup file will be downloaded to " + specific_folder);
            alertBuilder.setPositiveButton("Download", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    createApplicationFolder();
                    try {
                        for (int i1 = 0; i1 < downloadJsonArray.length(); i1++) {
                            final JSONObject jsonObject = downloadJsonArray.getJSONObject(i1);
                            if (downloadcb_id.equals(jsonObject.optString("cb_id"))) {
                                File mediaStorageDir = new File(Constants.folder_path);
                                if (!mediaStorageDir.exists()) {
                                    if (isNetworkAvailable()) {
                                        displayDownloadDialog();
                                        downloadfile = new DownloadFile(Constants.BASEURL + "public/uploads/contacts/" +
                                                jsonObject.optString("file_name"), jsonObject.optString("file_name"));
                                        downloadfile.execute("");
                                    } else {
                                        Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    File futureStudioIconFile = new File(Constants.folder_path + jsonObject.optString("file_name"));
                                    if (!futureStudioIconFile.exists()) {
                                        if (isNetworkAvailable()) {
                                            displayDownloadDialog();
                                            downloadfile = new DownloadFile(Constants.BASEURL + "public/uploads/contacts/" +
                                                    jsonObject.optString("file_name"), jsonObject.optString("file_name"));
                                            downloadfile.execute("");
                                        } else {
                                            Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        showSnackBar(Constants.folder_path + jsonObject.optString("file_name"), 2); //TODO Snackbar not working need to work
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        }
    }

    private void showSnackBar(final String path, int check) {
        Snackbar snackbar = null;
        switch (check) {
            case 1: {
//                snackbar = new Snackbar(BackupHistoryActivity.this, "File downloaded to " +
//                        path, "Import file", new View.OnClickListener() {
//                   @Override
//                    public void onClick(View v) {
//                        onClickEvent(path);
//                    }
//
//                });

                View view = (View) findViewById(R.id.backup_activity);
                snackbar.make(view , "File downloaded to " + path , Snackbar.LENGTH_SHORT).setAction("Import file", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClickEvent(path);
                    }
                }).show();

            }
            break;
            case 2: {
//                snackbar = new Snackbar(BackupHistoryActivity.this, "File already exist to " +
//                        path, "Import file", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        onClickEvent(path);
//                    }
//                });

                View view = findViewById(R.id.backup_activity);
                snackbar.make(view , "File downloaded to " + path , Snackbar.LENGTH_SHORT).setAction("Import file", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClickEvent(path);
                    }
                }).show();
            }
            break;
            default:
                Log.d("defaultcase", "" + check);
        }
//        if (snackbar != null) {
////            snackbar.setMessageTextSize(14);
////            snackbar.setIndeterminate(true);
////            snackbar.show();
//        }
        if (activeFlipper != null) {
            activeFlipper.setDisplayedChild(0);
        }
    }

    private void onClickEvent(String path) {

//        PackageManager packageManager = getPackageManager();
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.setType("file/*");
//
//        List<ResolveInfo> list = packageManager.queryIntentActivities(intent,0);
//
//        if(list.size() > 0){
////            Toast.makeText(this , "There are package manager" , Toast.LENGTH_SHORT).show();
//            final File file = new File(path);
//            Intent i = new Intent();
//            i.setAction(Intent.ACTION_VIEW);
//            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////            Uri uri = Uri.parse(path);
//            Uri uri = Uri.fromFile(file);
//            i.setDataAndType(uri, "text/x-vcard");
//            startActivity(Intent.createChooser(intent , "Open File Explorer"));
//        }

        try {
            final File file = new File(path);
            Intent i = new Intent();
            i.setAction(android.content.Intent.ACTION_VIEW);
//            i.setAction(android.content.Intent.ACTION_PICK);
            i.setDataAndType(Uri.fromFile(file), "text/x-vcard");
            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.estrongs.android.pop"));
            startActivity(intent);
        }
    }

    private static String convert_to_local_date(String date_parameter) throws ParseException {
        SimpleDateFormat format_server = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format_server.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = format_server.parse(date_parameter);
        SimpleDateFormat format_local = new SimpleDateFormat("dd MMM yyyy HH:mm:ss"); //TODO kind of any date format which we need to show
        format_local.setTimeZone(TimeZone.getDefault());
        String result_date = format_local.format(date);
        return result_date;
    }


}
package techathalon.com.smartcontactmanager.BACKUP_HISTORY;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

public class AlarmHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "alarm";
    private static final String TABLE_ALARM = "forgotData";
    private static final String KEY_BACKUP_ID = "backup_id";
    private static final String KEY_COUNT = "count";
    private static final String KEY_ALARM_FIRE_TIME = "alarm_fire_time";

    public AlarmHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public boolean checkExists(String dbfield, String fieldValue) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String Query = "Select * from " + TABLE_ALARM + " where " + dbfield + " = '" + fieldValue + "'";
        Cursor cursor = sqLiteDatabase.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    @SuppressLint("LogConditional")
    public void syncDatabase(List<AlarmData> chatHistoryDataModelList) {
        ArrayList<AlarmData> addedList = new ArrayList<>();
        for (AlarmData chat : chatHistoryDataModelList) {
            if (checkExists(KEY_BACKUP_ID, chat.getBackup_id())) {
                updateOTP(chat);
            } else
                addedList.add(chat);
        }
        insertChat(addedList);
    }

    public boolean insertChat(ArrayList<AlarmData> chatArrayList) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String sql = "INSERT INTO " + TABLE_ALARM + "(" + KEY_BACKUP_ID + ", " + KEY_COUNT
                + ", " + KEY_ALARM_FIRE_TIME
                + ") VALUES (?,?,?);";
        SQLiteStatement statement = sqLiteDatabase.compileStatement(sql);
        sqLiteDatabase.beginTransaction();
        for (int i = 0; i < chatArrayList.size(); i++) {
            statement.clearBindings();
            AlarmData chat = chatArrayList.get(i);
            statement.bindString(1, chat.getBackup_id());
            statement.bindString(2, chat.getCount());
            statement.bindString(3, chat.getAlarm_fire_time());
            statement.execute();
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
        return true;
    }

    public boolean updateOTP(AlarmData forgotData) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_BACKUP_ID, forgotData.getBackup_id());
        contentValues.put(KEY_COUNT, forgotData.getCount());
        contentValues.put(KEY_ALARM_FIRE_TIME, forgotData.getAlarm_fire_time());
        sqLiteDatabase.update(TABLE_ALARM, contentValues, KEY_BACKUP_ID + "  = ? ", new String[]{forgotData.getBackup_id()});
        return true;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ALARM + "("
                + KEY_BACKUP_ID + " TEXT, " + KEY_COUNT + " TEXT, "
                + KEY_ALARM_FIRE_TIME + " TEXT)";
        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ALARM);
        onCreate(sqLiteDatabase);
    }

    public ArrayList<AlarmData> getAllRecords() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ArrayList<AlarmData> array_list = new ArrayList<AlarmData>();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_ALARM, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            AlarmData model = new AlarmData();
            model.setBackup_id(cursor.getString(cursor.getColumnIndex(KEY_BACKUP_ID)));
            model.setCount(cursor.getString(cursor.getColumnIndex(KEY_COUNT)));
            model.setAlarm_fire_time(cursor.getString(cursor.getColumnIndex(KEY_ALARM_FIRE_TIME)));
            array_list.add(model);
            cursor.moveToNext();
        }
        cursor.close();
        return array_list;
    }

    public ArrayList<AlarmData> getSingleRecord(String backup_id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ArrayList<AlarmData> chatDataModels = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_ALARM + " where " + KEY_BACKUP_ID + " = " + backup_id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            AlarmData model = new AlarmData();
            model.setBackup_id(cursor.getString(cursor.getColumnIndex(KEY_BACKUP_ID)));
            model.setCount(cursor.getString(cursor.getColumnIndex(KEY_COUNT)));
            model.setAlarm_fire_time(cursor.getString(cursor.getColumnIndex(KEY_ALARM_FIRE_TIME)));
            chatDataModels.add(model);
            cursor.moveToNext();
        }
        cursor.close();
        return chatDataModels;
    }

    public void delete(String id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from " + TABLE_ALARM + " where " + KEY_BACKUP_ID + " = " + id);
    }
}
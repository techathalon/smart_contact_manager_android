package techathalon.com.smartcontactmanager.BACKUP_HISTORY;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.HELPER_FILES.HelperActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.HttpHelper1;
import techathalon.com.smartcontactmanager.HELPER_FILES.MainActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.PefUtils;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.SETTING.SettingsActivity;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class PinActivity extends HelperActivity implements View.OnClickListener {
    private TextView btn_ok, btn_cancel, btn_clear, enter_pin_text;
    private ImageView img_pin1, img_pin2, img_pin3, img_pin4;
    private TextView txt_forgot_pin;
    private LinearLayout lin_password;
    StringBuilder password = new StringBuilder();
    private SharedPreferences pref, emailPrefs, userDataPrefs;
    private Boolean settings = false;
    Bundle bundle;
    HttpHelper1 httpHelper1;
    HashMap<String, String> hashMap;
    Intent commonIntent;

    @Override
    public void setBackApiResponse1(String url, String object) {
        super.setBackApiResponse1(url, object);
        if (url.equals(Constants.CHECK_PASSWORD_URL)) {
            if (object != null && !object.equals("")) {
                try {
                    JSONObject jsonObject = new JSONObject(object);
                    String success = jsonObject.getString("success");
                    if (success.equalsIgnoreCase("true")) {
                        if (getIntent().hasExtra("check password")) {
                            if (getIntent().getExtras().getString("check password").equalsIgnoreCase("check")) {
                                commonIntent = new Intent(PinActivity.this, BackupHistoryActivity.class);
                                setResult(1, commonIntent);
                                finish();
                            }
                        }
                    } else {
                        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
                        lin_password.startAnimation(shake);
                        img_pin1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactivecircle1));
                        img_pin2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactivecircle1));
                        img_pin3.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactivecircle1));
                        img_pin4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactivecircle1));
                        password = new StringBuilder();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, getString(R.string.some_error_occurred_msg), Toast.LENGTH_SHORT).show();
            }
        } else if (url.equals(Constants.SEND_PASSWORD_URL)) {
            if (object != null && !object.equals("")) {
                try {
                    JSONObject jsonObject = new JSONObject(object);
                    String success = jsonObject.getString("success");

                    if (success.equalsIgnoreCase("true")) {
                        Toast.makeText(this, "Password sent successfully to "
                                        + userDataPrefs.getString("registered_email", "") + ", check your inbox ",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (url.equals(Constants.SAVE_PASSWORD_URL)) {
            if (object != null && !object.equals("")) {
                try {
                    JSONObject jsonObject = new JSONObject(object);
                    String success = jsonObject.getString("success");
                    if (success.equalsIgnoreCase("true")) {

                        if (getIntent().hasExtra("first time")) {
                            if (getIntent().getExtras().getString("first time").equalsIgnoreCase("yes")) {
                                if (settings) {
//                                    commonIntent = new Intent(this, SettingsActivity.class);
                                    commonIntent = new Intent(PinActivity.this, MainActivity.class);
                                    commonIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //TODO in order to kill the first Main activity in android
                                    startActivity(commonIntent);
                                } else {
                                    commonIntent = new Intent(PinActivity.this, BackupHistoryActivity.class);
                                    setResult(5, commonIntent);
                                }
                                finish();
                            }
                        }
                        SharedPreferences.Editor editEmail = emailPrefs.edit();
                        editEmail.putString("first time", "no");
                        editEmail.apply();
                        JSONArray jsonArray = jsonObject.getJSONArray("user_details");
                        if (jsonArray.length() > 0) {
                            JSONObject innerObject = jsonArray.getJSONObject(0);
                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("user_id", innerObject.getString("user_id"));
                            map.put("u_firstname", innerObject.getString("u_firstname"));
                            map.put("u_lastname", innerObject.getString("u_lastname"));
                            map.put("u_email", innerObject.getString("u_email"));
                            map.put("registered_email", innerObject.getString("registered_email"));
                            map.put("facebook_id", innerObject.getString("facebook_id"));
                            map.put("password", innerObject.getString("password"));
                            map.put("birthday", innerObject.getString("u_birthday"));
                            PefUtils.saveRegisteredData(pref, map);
                            Constants.registered_email = innerObject.getString("registered_email");
                            Constants.isLogin = true;
                            SharedPreferences.Editor edit = pref.edit();
                            edit.putBoolean("isupdated", true);
                            edit.apply();
                        }
                        Constants.isPasswordValidated = true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        if(Constants.enable_tracker){
            SCMApplication.getInstance().trackScreenView("Pin Security");
        }
        userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        enter_pin_text = findViewById(R.id.text1);
        btn_ok.setOnClickListener(this);
        btn_clear = findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(this);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(this);

        img_pin1 = findViewById(R.id.img_pin1);
        img_pin2 = findViewById(R.id.img_pin2);

        img_pin3 = findViewById(R.id.img_pin3);
        img_pin4 = findViewById(R.id.img_pin4);
        txt_forgot_pin = findViewById(R.id.txt_forgot_pin);

        txt_forgot_pin.setOnClickListener(this);
        lin_password = findViewById(R.id.lin_password);

        if (getIntent().getExtras() != null && getIntent().hasExtra("history")) {
            bundle = getIntent().getExtras();
        }
        if (getIntent().hasExtra("first time")) {
            if (getIntent().getExtras().getString("first time").equalsIgnoreCase("yes")) {
                enter_pin_text.setText("Set 4 digit PIN");
                txt_forgot_pin.setVisibility(View.GONE);
            }
        } else
            txt_forgot_pin.setVisibility(View.VISIBLE);

        if (getIntent().hasExtra("from settings") && getIntent().getExtras().getString("from settings").equalsIgnoreCase("settings")) {
            settings = true;
        }
        pref = getSharedPreferences("LOGIN", 0);
        emailPrefs = getSharedPreferences("email data", Context.MODE_PRIVATE);
    }

    public void pinClicked(View v) {
        if (password.length() < 4) {
            switch (password.length()) {
                case 0:
                    img_pin1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.activepin1));
                    break;
                case 1:
                    img_pin2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.activepin1));
                    break;
                case 2:
                    img_pin3.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.activepin1));
                    break;
                case 3:
                    img_pin4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.activepin1));
                    break;
            }
            password.append(v.getTag().toString());
        }
        if (password.length() == 4) {
            String previousPassword = pref.getString("password", "");
            String entered_password = password.toString();
            if (previousPassword.equalsIgnoreCase("")) {
                hashMap = new HashMap<>();
                hashMap.put("registered_email", userDataPrefs.getString("registered_email", ""));
                hashMap.put("password", entered_password);
                httpHelper1 = new HttpHelper1(Constants.SAVE_PASSWORD_URL, hashMap, PinActivity.this,
                        "Saving your password...");
            } else {
                hashMap = new HashMap<>();
                hashMap.put("registered_email", userDataPrefs.getString("registered_email", ""));
                hashMap.put("password", entered_password);
                httpHelper1 = new HttpHelper1(Constants.CHECK_PASSWORD_URL, hashMap, PinActivity.this,
                        "Validating your password...");
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_clear) {
            img_pin1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactivecircle1));
            img_pin2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactivecircle1));
            img_pin3.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactivecircle1));
            img_pin4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactivecircle1));
            password = new StringBuilder();
        } else if (v == btn_cancel) {
            if (getIntent().hasExtra("check password") && getIntent().getExtras().getString("check password").equalsIgnoreCase("check")) {
                commonIntent = new Intent(PinActivity.this, BackupHistoryActivity.class);
                commonIntent.putExtras(bundle);
                setResult(2, commonIntent);
                finish();
            } else {
                if (settings)
                    commonIntent = new Intent(PinActivity.this, MainActivity.class);
                else
                    commonIntent = new Intent(PinActivity.this, BackupHistoryActivity.class);
                if (bundle != null)
                    commonIntent.putExtras(bundle);
                startActivity(commonIntent);
                finish();
            }
        } else if (v == txt_forgot_pin) {
            hashMap = new HashMap<>();
            hashMap.put("registered_email", userDataPrefs.getString("registered_email", ""));
            httpHelper1 = new HttpHelper1(Constants.SEND_PASSWORD_URL, hashMap, PinActivity.this,
                    "Sending password...");
        }
    }

    public void onBackPressed() {
        if (getIntent().hasExtra("check password") && getIntent().getExtras().getString("check password").equalsIgnoreCase("check")) {
            commonIntent = new Intent(PinActivity.this, BackupHistoryActivity.class);
            commonIntent.putExtras(bundle);
            setResult(2, commonIntent);
            finish();
        } else {
            if (settings){
//                commonIntent = new Intent(this, MainActivity.class);
                finish();  //TODO this was added in ofder to math the drawer layout in android
            }

            else{
                commonIntent = new Intent(PinActivity.this, BackupHistoryActivity.class);
                if (bundle != null)
                    commonIntent.putExtras(bundle);
                startActivity(commonIntent);
                finish();
            }

        }
//        super.onBackPressed();
//        commonIntent = new Intent(this, MainActivity.class);
//        startActivity(commonIntent);
//        finish();
    }
}
package techathalon.com.smartcontactmanager.BACKUP_HISTORY;

import java.io.Serializable;

public class AlarmData implements Serializable {
    private String backup_id = "", count = "", alarm_fire_time = "";

    public void setBackup_id(String backup_id) {
        this.backup_id = backup_id;
    }

    public String getBackup_id() {
        return backup_id;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getCount() {
        return count;
    }

    public void setAlarm_fire_time(String alarm_fire_time) {
        this.alarm_fire_time = alarm_fire_time;
    }

    public String getAlarm_fire_time() {
        return alarm_fire_time;
    }
}
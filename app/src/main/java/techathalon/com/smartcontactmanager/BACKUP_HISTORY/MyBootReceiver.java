package techathalon.com.smartcontactmanager.BACKUP_HISTORY;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.NOTIFICATION.NotificationAction;
import techathalon.com.smartcontactmanager.NOTIFICATION.NotificationBroadcastReceiver;
import techathalon.com.smartcontactmanager.R;

public class MyBootReceiver extends BroadcastReceiver {
    ArrayList<AlarmData> alarmDataArrayList;
    AlarmHandler alarmHandler;
    String data = "", data1 = "", addedDateString = "";
    Calendar c2, c3, calendar;
    SimpleDateFormat dateFormat;
    Date date, date1, addedDate;
    AlarmManager am4, alarmManager;
    Intent ai4, alarmIntent;
    PendingIntent pi4, pendingIntent;
    final static int RQS_1 = 1;

    SharedPreferences reschedulePrefs, userDataPrefs;
    SharedPreferences.Editor reschedulePrefsEditor;
    Context mContext;
    public int MY_NOTIFICATION_ID = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        alarmHandler = new AlarmHandler(context);
        alarmDataArrayList = alarmHandler.getAllRecords();

        reschedulePrefs = context.getSharedPreferences("reschedule", Context.MODE_PRIVATE);
        data1 = reschedulePrefs.getString("alarm_time", "");
        if (!data1.equals("")) {
            c2 = Calendar.getInstance();
            c3 = Calendar.getInstance();
            dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");
            try {
                date1 = dateFormat.parse(data1);
                String sdsds = data1.substring(data1.indexOf(" "));
                sdsds = sdsds.replace(" ", "");
                sdsds = sdsds.substring(0, 2);
                c3.setTime(date1);
                c3.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sdsds));
                data = dateFormat.format(date1);
                if (c3.compareTo(c2) > 0) {
                    am4 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    ai4 = new Intent(context, NotificationBroadcastReceiver.class);
                    ai4.putExtra("option", reschedulePrefs.getString("option", ""));

                    pi4 = PendingIntent.getBroadcast(context, RQS_1, ai4, PendingIntent.FLAG_ONE_SHOT);
                    am4.cancel(pi4);
                    pi4.cancel();
                    am4 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    pi4 = PendingIntent.getBroadcast(context, RQS_1, ai4, PendingIntent.FLAG_ONE_SHOT);

                    if (data1.contains("PM") || data1.contains("pm")) {
                        c3.set(Calendar.AM_PM, Calendar.PM);
                    } else {
                        c3.set(Calendar.AM_PM, Calendar.AM);
                    }
                    data = c3.get(Calendar.DAY_OF_MONTH) + "";
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        am4.set(AlarmManager.RTC_WAKEUP, c3.getTimeInMillis(), pi4);
                    } else {
                        am4.setExact(AlarmManager.RTC_WAKEUP, c3.getTimeInMillis(), pi4);
                    }
                    reschedulePrefsEditor = reschedulePrefs.edit();
                    reschedulePrefsEditor.putString("alarm_time", data1);
                    reschedulePrefsEditor.putString("option", reschedulePrefs.getString("option", ""));
                    reschedulePrefsEditor.apply();
                } else if (c3.compareTo(c2) < 0) {
                    userDataPrefs = context.getSharedPreferences("userData", Context.MODE_PRIVATE);
                    if (userDataPrefs.getBoolean("isLogin", false))
                        fireNotification();
                    switch (Integer.parseInt(reschedulePrefs.getString("option", ""))) {
                        case 5: {
                            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                            alarmIntent = new Intent(context, NotificationBroadcastReceiver.class);
                            alarmIntent.putExtra("option", "5");
                            pendingIntent = PendingIntent.getBroadcast(context, RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
                            alarmManager.cancel(pendingIntent);
                            pendingIntent.cancel();

                            alarmIntent = new Intent(context, NotificationBroadcastReceiver.class);
                            alarmIntent.putExtra("option", "5");
                            pendingIntent = PendingIntent.getBroadcast(context, RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
                            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                            calendar = Calendar.getInstance();
                            dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");
                            date = dateFormat.parse(data1);
                            data = dateFormat.format(date);
                            calendar.add(Calendar.HOUR, 120);
//                            calendar.add(Calendar.MINUTE, 1);

                            addedDate = calendar.getTime();
                            addedDateString = dateFormat.format(addedDate);
                            reschedulePrefs = context.getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                            reschedulePrefsEditor = reschedulePrefs.edit();
                            reschedulePrefsEditor.putString("alarm_time", addedDateString);
                            reschedulePrefsEditor.putString("option", "5");
                            reschedulePrefsEditor.apply();

                            if (data1.contains("PM") || data1.contains("pm")) {
                                calendar.set(Calendar.AM_PM, Calendar.PM);
                            } else {
                                calendar.set(Calendar.AM_PM, Calendar.AM);
                            }
                            data = calendar.get(Calendar.DAY_OF_MONTH) + "";
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            } else {
                                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            }
                        }
                        break;
                        case 10: {
                            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                            alarmIntent = new Intent(context, NotificationBroadcastReceiver.class);
                            alarmIntent.putExtra("option", "10");
                            pendingIntent = PendingIntent.getBroadcast(context, RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
                            alarmManager.cancel(pendingIntent);
                            pendingIntent.cancel();

                            alarmIntent = new Intent(context, NotificationBroadcastReceiver.class);
                            alarmIntent.putExtra("option", "10");
                            pendingIntent = PendingIntent.getBroadcast(context, RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
                            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                            calendar = Calendar.getInstance();
                            date = dateFormat.parse(data1);
                            data = dateFormat.format(date);
                            calendar.add(Calendar.HOUR, 240);
//                            calendar.add(Calendar.MINUTE, 2);

                            addedDate = calendar.getTime();
                            addedDateString = dateFormat.format(addedDate);
                            reschedulePrefs = context.getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                            reschedulePrefsEditor = reschedulePrefs.edit();
                            reschedulePrefsEditor.putString("alarm_time", addedDateString);
                            reschedulePrefsEditor.putString("option", "10");
                            reschedulePrefsEditor.apply();

                            if (data1.contains("PM") || data1.contains("pm")) {
                                calendar.set(Calendar.AM_PM, Calendar.PM);
                            } else {
                                calendar.set(Calendar.AM_PM, Calendar.AM);
                            }
                            data = calendar.get(Calendar.DAY_OF_MONTH) + "";
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            } else {
                                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            }
                        }
                        break;
                        case 15: {
                            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                            alarmIntent = new Intent(context, NotificationBroadcastReceiver.class);
                            alarmIntent.putExtra("option", "15");
                            pendingIntent = PendingIntent.getBroadcast(context, RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
                            alarmManager.cancel(pendingIntent);
                            pendingIntent.cancel();

                            alarmIntent = new Intent(context, NotificationBroadcastReceiver.class);
                            alarmIntent.putExtra("option", "15");
                            pendingIntent = PendingIntent.getBroadcast(context, RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
                            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                            calendar = Calendar.getInstance();
                            date = dateFormat.parse(data1);
                            data = dateFormat.format(date);
                            calendar.add(Calendar.HOUR, 360);
//                            calendar.add(Calendar.MINUTE, 3);

                            addedDate = calendar.getTime();
                            addedDateString = dateFormat.format(addedDate);
                            reschedulePrefs = context.getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                            reschedulePrefsEditor = reschedulePrefs.edit();
                            reschedulePrefsEditor.putString("alarm_time", addedDateString);
                            reschedulePrefsEditor.putString("option", "15");
                            reschedulePrefsEditor.apply();

                            if (data1.contains("PM") || data1.contains("pm")) {
                                calendar.set(Calendar.AM_PM, Calendar.PM);
                            } else {
                                calendar.set(Calendar.AM_PM, Calendar.AM);
                            }
                            data = calendar.get(Calendar.DAY_OF_MONTH) + "";
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            } else {
                                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            }
                        }
                        break;
                        case 30: {
                            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                            alarmIntent = new Intent(context, NotificationBroadcastReceiver.class);
                            alarmIntent.putExtra("option", "30");
                            pendingIntent = PendingIntent.getBroadcast(context, RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
                            alarmManager.cancel(pendingIntent);
                            pendingIntent.cancel();

                            alarmIntent = new Intent(context, NotificationBroadcastReceiver.class);
                            alarmIntent.putExtra("option", "30");
                            pendingIntent = PendingIntent.getBroadcast(context, RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
                            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                            calendar = Calendar.getInstance();
                            date = dateFormat.parse(data1);
                            data = dateFormat.format(date);
                            calendar.add(Calendar.HOUR, 720);
//                            calendar.add(Calendar.MINUTE, 4);

                            addedDate = calendar.getTime();
                            addedDateString = dateFormat.format(addedDate);
                            reschedulePrefs = context.getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                            reschedulePrefsEditor = reschedulePrefs.edit();
                            reschedulePrefsEditor.putString("alarm_time", addedDateString);
                            reschedulePrefsEditor.putString("option", "30");
                            reschedulePrefsEditor.apply();

                            if (data1.contains("PM") || data1.contains("pm")) {
                                calendar.set(Calendar.AM_PM, Calendar.PM);
                            } else {
                                calendar.set(Calendar.AM_PM, Calendar.AM);
                            }
                            data = calendar.get(Calendar.DAY_OF_MONTH) + "";
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            } else {
                                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            }
                        }
                        break;
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void fireNotification() {
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(ns);
        CharSequence contentTitle = Constants.APP_NAME;
        CharSequence contentText = "Do contact backup";
        Intent notificationIntent = new Intent(mContext, NotificationAction.class);
        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, 0);

        Notification.Builder builder = new Notification.Builder(mContext)
                .setContentTitle(contentTitle)
                .setContentIntent(contentIntent)
                .setContentText(contentText)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setSmallIcon(getNotificationIcon());

        if (Build.VERSION.SDK_INT < 16) {
            mNotificationManager.notify(MY_NOTIFICATION_ID, builder.getNotification());
        } else {
            mNotificationManager.notify(MY_NOTIFICATION_ID, builder.build());
        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? android.R.drawable.sym_contact_card : R.drawable.ic_launcher12;
    }
}
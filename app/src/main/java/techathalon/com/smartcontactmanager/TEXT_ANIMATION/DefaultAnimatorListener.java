package techathalon.com.smartcontactmanager.TEXT_ANIMATION;

import android.animation.Animator;

/**
 * Created by Kush Thakkar on 1/29/2018.
 */

public class DefaultAnimatorListener implements Animator.AnimatorListener {
    @Override
    public void onAnimationStart(Animator animation) {
        // no-op
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        // no-op
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        // no-op
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
        // no-op
    }
}

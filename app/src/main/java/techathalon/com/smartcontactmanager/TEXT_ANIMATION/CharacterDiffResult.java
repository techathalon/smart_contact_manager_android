package techathalon.com.smartcontactmanager.TEXT_ANIMATION;

/**
 * Created by Kush Thakkar on 1/29/2018.
 */

public class CharacterDiffResult {
    public char c;
    public int  fromIndex;
    public int  moveIndex;
}

package techathalon.com.smartcontactmanager.TEXT_ANIMATION;

/**
 * Created by Kush Thakkar on 1/29/2018.
 */

public interface AnimationListener {
    void onAnimationEnd(HTextView hTextView);
}

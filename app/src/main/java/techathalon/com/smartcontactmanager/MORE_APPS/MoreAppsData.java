package techathalon.com.smartcontactmanager.MORE_APPS;

import java.io.Serializable;

public class MoreAppsData implements Serializable {
    private String appImage, appName, url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setAppImage(String appImage) {
        this.appImage = appImage;
    }

    public String getAppImage() {
        return appImage;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppName() {
        return appName;
    }
}
package techathalon.com.smartcontactmanager.MORE_APPS;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.HELPER_FILES.HelperActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.MainActivity;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class MoreAppsActivity extends HelperActivity implements AdapterView.OnItemClickListener {
    GridView moreAppsGridview;
    ArrayList<MoreAppsData> moreAppsDataArrayList;
    MoreAppsAdapter adapter;
    private PagerAdapter mPagerAdapter;
    private ViewPager mPager;
    LinearLayout lin_swipe_left, lin_swipe_right;
    int width;
    MoreAppsData moreAppsData;
    Intent commonIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        width = metrics.widthPixels;
        setContentView(R.layout.more_apps_screen);
        lin_swipe_left = (LinearLayout) findViewById(R.id.lin_swipe_left);
        lin_swipe_right = (LinearLayout) findViewById(R.id.lin_swipe_right);
        lin_swipe_right.setVisibility(View.GONE);

        if(Constants.enable_tracker){
            SCMApplication.getInstance().trackScreenView("More Apps");
        }

        moreAppsDataArrayList = new ArrayList<>();

        moreAppsGridview = (GridView) findViewById(R.id.more_app_gridview);
        moreAppsGridview.setOnItemClickListener(this);
        MoreAppsData moreAppsData;

        moreAppsData = new MoreAppsData();
        moreAppsData.setAppName("Smash Balls Hit");
        moreAppsData.setAppImage("ball_hit");
        moreAppsData.setUrl("com.techathalon.hitme");
        moreAppsDataArrayList.add(moreAppsData);

        moreAppsData = new MoreAppsData();
        moreAppsData.setAppName("Birthdays & Contacts (B&C)");
        moreAppsData.setAppImage("birthcontact");
        moreAppsData.setUrl("com.techathalon.birthdaycontact");
        moreAppsDataArrayList.add(moreAppsData);

        moreAppsData = new MoreAppsData();
        moreAppsData.setAppName("Ball Hits - Fire");
        moreAppsData.setAppImage("ball_hit_pro");
        moreAppsData.setUrl("allinonegame.techathalon.com.smashballhit");
        moreAppsDataArrayList.add(moreAppsData);

        moreAppsData = new MoreAppsData();
        moreAppsData.setAppName("Techathalon Software Solutions");
        moreAppsData.setAppImage("techathalon1");
        moreAppsData.setUrl("techathalon.com.techathalon");
        moreAppsDataArrayList.add(moreAppsData);

        adapter = new MoreAppsAdapter(getApplicationContext(), moreAppsDataArrayList);
        moreAppsGridview.setAdapter(adapter);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.addOnPageChangeListener(null);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // lin_swipe_left.setVisibility(View.GONE);
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    lin_swipe_left.setVisibility(View.VISIBLE);
                    lin_swipe_right.setVisibility(View.GONE);
                } else if (position > 0 && position < moreAppsDataArrayList.size() - 1) {
                    lin_swipe_right.setVisibility(View.VISIBLE);
                    lin_swipe_left.setVisibility(View.VISIBLE);
                } else {
                    lin_swipe_left.setVisibility(View.GONE);
                    lin_swipe_right.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        int storepagewidth = getResources().getDimensionPixelOffset(R.dimen.viewpager_margin_new);
        mPager.setOffscreenPageLimit(moreAppsDataArrayList.size());
        mPager.setClipToPadding(false);
        mPager.setPadding(50, 50, 50, 50);
        mPager.setPageMargin(25);
        mPager.setPageTransformer(true, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View view, float position) {
                final float scale = position < 0 ? position + 1f : Math.abs(1f - position);
                view.setScaleX(scale);
                view.setScaleY(scale);
                view.setPivotX(view.getWidth() * 0.5f);
                view.setPivotY(view.getHeight() * 0.5f);
                view.setAlpha(position < -1f || position > 1f ? 0f : 1f - (scale - 1f));
            }
        });
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            moreAppsData = moreAppsDataArrayList.get(position);
            Bundle bundle = new Bundle();
            bundle.putString("appName", moreAppsData.getAppName());
            bundle.putString("appImage", moreAppsData.getAppImage());
            bundle.putString("appUrl", moreAppsData.getUrl());
            MoreAppFragment moreAppFragment = MoreAppFragment.getInstance();
            moreAppFragment.setArguments(bundle);
            return moreAppFragment;
        }

        @Override
        public int getCount() {
            return moreAppsDataArrayList.size();
        }

    }

//    @Override
//    public void onBackPressed() {
////        commonIntent = new Intent(MoreAppsActivity.this, MainActivity.class);
////        startActivity(commonIntent);
////        finish();
//    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String name = moreAppsDataArrayList.get(position).getAppName();
        if (name.equals("Birthday & Contact Manager")) {
            commonIntent = new Intent(Intent.ACTION_VIEW);
            commonIntent.setData(Uri.parse("market://details?id=com.techathalon.birthdaycontact"));
            startActivity(commonIntent);
        } else if (name.equals("Store Grunt")) {
            commonIntent = new Intent(Intent.ACTION_VIEW);
            commonIntent.setData(Uri.parse("market://details?id=com.client.storegrunt"));
            startActivity(commonIntent);
        } else if (name.equals("Birthday Matters") || name.equals("Bollywood Songs Quiz") || name.equals("HBollywood Celebrities") || name.equals("Bollywood Movies Quiz")
                || name.equals("HBollywood") || name.equals("Project Book") || name.equals("Pocket Points") || name.equals("North Park") || name.equals("Models America")
                || name.equals("Fyndit")) {
            Toast.makeText(getApplicationContext(), "Coming soon", Toast.LENGTH_SHORT).show();
        }
    }
}
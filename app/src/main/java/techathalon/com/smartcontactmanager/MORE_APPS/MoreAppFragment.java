package techathalon.com.smartcontactmanager.MORE_APPS;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import techathalon.com.smartcontactmanager.R;

public class MoreAppFragment extends Fragment {
    public static MoreAppFragment moreAppFragment;
    Activity activiy;
    String appName1 = "", appImage1 = "", appUrl1 = "";
    View view;
    ImageView appImage;
    TextView appName;
    LinearLayout item_layout;
    int resourceId;
    Button installNow;

    public MoreAppFragment() {
        // Required empty public constructor
    }

    public static MoreAppFragment getInstance() {
        moreAppFragment = new MoreAppFragment();
        return moreAppFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activiy = (Activity) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activiy = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_more_app, container, false);
        appImage = (ImageView) view.findViewById(R.id.icon);
        appName = (TextView) view.findViewById(R.id.name);
        item_layout = (LinearLayout) view.findViewById(R.id.item_layout);
        if (getArguments() != null) {
            appName1 = getArguments().getString("appName");
            appImage1 = getArguments().getString("appImage");
            appUrl1 = getArguments().getString("appUrl");
        }
        resourceId = activiy.getResources().getIdentifier(appImage1, "drawable", activiy.getPackageName());
        appImage.setImageResource(resourceId);
        appName.setSelected(true);
        appName.setText(appName1);
        item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + appUrl1)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + appUrl1)));
                }
            }
        });
        installNow = (Button) view.findViewById(R.id.install);
        installNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + appUrl1)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + appUrl1)));
                }
            }
        });
        return view;
    }
}
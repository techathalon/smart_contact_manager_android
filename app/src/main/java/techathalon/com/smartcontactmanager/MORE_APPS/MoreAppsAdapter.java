package techathalon.com.smartcontactmanager.MORE_APPS;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import techathalon.com.smartcontactmanager.R;

public class MoreAppsAdapter extends BaseAdapter {
    Context context;
    ArrayList<MoreAppsData> moreAppsDataArrayList;
    LayoutInflater inflater;
    MoreAppsData moreAppsData;

    public MoreAppsAdapter(Context context, ArrayList<MoreAppsData> moreAppsDataArrayList) {
        this.context = context;
        this.moreAppsDataArrayList = moreAppsDataArrayList;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return moreAppsDataArrayList.size();
    }

    @Override
    public MoreAppsData getItem(int position) {
        return moreAppsDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder mViewHolder;
        moreAppsData = moreAppsDataArrayList.get(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.more_apps_item_view, parent, false);
            mViewHolder = new ViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        Resources resources = context.getResources();
        final int resourceId = resources.getIdentifier(moreAppsData.getAppImage(), "drawable", context.getPackageName());
        mViewHolder.appImage.setImageResource(resourceId);
        mViewHolder.appName.setSelected(true);
        mViewHolder.appName.setText(moreAppsData.getAppName());
        return convertView;
    }

    private class ViewHolder {
        TextView appName;
        ImageView appImage;

        public ViewHolder(View itemView) {
            appName = (TextView) itemView.findViewById(R.id.app_name);
            appImage = (ImageView) itemView.findViewById(R.id.app_image);
        }
    }
}
package techathalon.com.smartcontactmanager.NOTIFICATION;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.R;

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    public int MY_NOTIFICATION_ID = 1;
    String addedDateString = "";
    SharedPreferences reschedulePrefs;
    SharedPreferences.Editor reschedulePrefsEditor;
    Date addedDate;

    AlarmManager alarmManager, alarmManager1;
    Intent alarmIntent;
    PendingIntent pendingIntent;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? android.R.drawable.sym_contact_card : R.drawable.ic_launcher12;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String reminderSelected = intent.getExtras().getString("option", "");
        Log.d("SELECT TIMER", "Nootification timer : " + reminderSelected);
//        Toast.makeText(context, "inside notification", Toast.LENGTH_LONG).show();

        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(ns);
        CharSequence contentTitle = Constants.APP_NAME;
        CharSequence contentText = "Time for Backup";
        Intent notificationIntent = new Intent(context, NotificationAction.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "channel_id")
                .setContentTitle(contentTitle)
                .setContentIntent(contentIntent)
                .setContentText(contentText)
                .setSound(alarmSound)
                .setAutoCancel(true)
                .setSmallIcon(getNotificationIcon());

        if (Build.VERSION.SDK_INT < 16) {
            mNotificationManager.notify(MY_NOTIFICATION_ID, builder.getNotification());
        } else {
            mNotificationManager.notify(MY_NOTIFICATION_ID, builder.build());
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(1 /* Request Code */, builder.build());

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");
        Date date = new Date();
        String data = dateFormat.format(date);

        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmIntent = new Intent(context, NotificationBroadcastReceiver.class);
        alarmIntent.putExtra("option", reminderSelected);
        pendingIntent = PendingIntent.getBroadcast(context, 1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();

        Calendar calendar = Calendar.getInstance();
        if (!reminderSelected.equals("")) {
            if (reminderSelected.equalsIgnoreCase("daily")) {
                calendar.add(Calendar.HOUR, 120);
                if (data.contains("PM") || data.contains("pm")) {
                    calendar.set(Calendar.AM_PM, Calendar.PM);
                } else {
                    calendar.set(Calendar.AM_PM, Calendar.AM);
                }
            } else if (reminderSelected.equalsIgnoreCase("weekly")) {
                calendar.add(Calendar.HOUR, 240);
                if (data.contains("PM") || data.contains("pm")) {
                    calendar.set(Calendar.AM_PM, Calendar.PM);
                } else {
                    calendar.set(Calendar.AM_PM, Calendar.AM);
                }
            } else if (reminderSelected.equalsIgnoreCase("monthly")) {
                calendar.add(Calendar.HOUR, 720);
                if (data.contains("PM") || data.contains("pm")) {
                    calendar.set(Calendar.AM_PM, Calendar.PM);
                } else {
                    calendar.set(Calendar.AM_PM, Calendar.AM);
                }
            } else if (reminderSelected.equals("5") || reminderSelected.equals("10")
                    || reminderSelected.equals("15") || reminderSelected.equals("30")) {
                switch (Integer.parseInt(reminderSelected)) {
                    case 5:
                        calendar.add(Calendar.HOUR, 120);
                        //TODO by sonali
//                        calendar.add(Calendar.MINUTE, 1);

                        addedDate = calendar.getTime();
                        addedDateString = dateFormat.format(addedDate);
                        reschedulePrefs = context.getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                        reschedulePrefsEditor = reschedulePrefs.edit();
                        reschedulePrefsEditor.putString("alarm_time", addedDateString);
                        reschedulePrefsEditor.putString("option", "5");
                        reschedulePrefsEditor.apply();

                        if (data.contains("PM") || data.contains("pm")) {
                            calendar.set(Calendar.AM_PM, Calendar.PM);
                        } else {
                            calendar.set(Calendar.AM_PM, Calendar.AM);
                        }
                        break;
                    case 10:
                        calendar.add(Calendar.HOUR, 240);
//                        calendar.add(Calendar.MINUTE, 2);

                        addedDate = calendar.getTime();
                        addedDateString = dateFormat.format(addedDate);
                        reschedulePrefs = context.getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                        reschedulePrefsEditor = reschedulePrefs.edit();
                        reschedulePrefsEditor.putString("alarm_time", addedDateString);
                        reschedulePrefsEditor.putString("option", "10");
                        reschedulePrefsEditor.apply();

                        if (data.contains("PM") || data.contains("pm")) {
                            calendar.set(Calendar.AM_PM, Calendar.PM);
                        } else {
                            calendar.set(Calendar.AM_PM, Calendar.AM);
                        }
                        break;
                    case 15:
                        calendar.add(Calendar.HOUR, 360);
//                        calendar.add(Calendar.MINUTE, 3);

                        addedDate = calendar.getTime();
                        addedDateString = dateFormat.format(addedDate);
                        reschedulePrefs = context.getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                        reschedulePrefsEditor = reschedulePrefs.edit();
                        reschedulePrefsEditor.putString("alarm_time", addedDateString);
                        reschedulePrefsEditor.putString("option", "15");
                        reschedulePrefsEditor.apply();

                        if (data.contains("PM") || data.contains("pm")) {
                            calendar.set(Calendar.AM_PM, Calendar.PM);
                        } else {
                            calendar.set(Calendar.AM_PM, Calendar.AM);
                        }
                        break;
                    case 30:
                        calendar.add(Calendar.HOUR, 720);
//                        calendar.add(Calendar.MINUTE, 4);

                        addedDate = calendar.getTime();
                        addedDateString = dateFormat.format(addedDate);
                        reschedulePrefs = context.getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                        reschedulePrefsEditor = reschedulePrefs.edit();
                        reschedulePrefsEditor.putString("alarm_time", addedDateString);
                        reschedulePrefsEditor.putString("option", "30");
                        reschedulePrefsEditor.apply();

                        if (data.contains("PM") || data.contains("pm")) {
                            calendar.set(Calendar.AM_PM, Calendar.PM);
                        } else {
                            calendar.set(Calendar.AM_PM, Calendar.AM);
                        }
                        break;

                }
            } else {
                calendar.add(Calendar.HOUR, 120);
//                calendar.add(Calendar.MINUTE, 1);

                addedDate = calendar.getTime();
                addedDateString = dateFormat.format(addedDate);
                reschedulePrefs = context.getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                reschedulePrefsEditor = reschedulePrefs.edit();
                reschedulePrefsEditor.putString("alarm_time", addedDateString);
                reschedulePrefsEditor.putString("option", "5");
                reschedulePrefsEditor.apply();

                if (data.contains("PM") || data.contains("pm")) {
                    calendar.set(Calendar.AM_PM, Calendar.PM);
                } else {
                    calendar.set(Calendar.AM_PM, Calendar.AM);
                }
            }
            alarmIntent = new Intent(context, NotificationBroadcastReceiver.class);
            alarmIntent.putExtra("option", reminderSelected);
            pendingIntent = PendingIntent.getBroadcast(context, 1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager1 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
            } else {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
            }
        }

    }
}
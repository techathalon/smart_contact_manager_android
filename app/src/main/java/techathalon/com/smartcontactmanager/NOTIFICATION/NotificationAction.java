package techathalon.com.smartcontactmanager.NOTIFICATION;

import android.content.Intent;
import android.os.Bundle;

import techathalon.com.smartcontactmanager.CREATE_BACKUP.TakeBackupActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.CommonActivity;

public class NotificationAction extends CommonActivity {
    Intent commonIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commonIntent = new Intent(this, TakeBackupActivity.class);
        startActivity(commonIntent);
        finish();
    }
}
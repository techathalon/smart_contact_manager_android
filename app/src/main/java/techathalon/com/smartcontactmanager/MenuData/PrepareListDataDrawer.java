package techathalon.com.smartcontactmanager.MenuData;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.res.Resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import techathalon.com.smartcontactmanager.R;

public class PrepareListDataDrawer {
    Context context;
public PrepareListDataDrawer(Context context)
{
    this.context=context;
}
    ArrayList<String> menu;
    HashMap<String , List<String>> sub_menu;

    ArrayList<String> menu1;
    HashMap<String , List<String>> submenu1;

    public void prepare_data(){
        menu = new ArrayList<>();
        sub_menu = new HashMap<>();

        menu.add("Change Security Pin");
        menu.add(context.getString(R.string.setting1));  // Todo there is a switch button
        menu.add(context.getString(R.string.setting2)); //Todo there is a submenu
        menu.add("Contact Backup Option"); //Todo there is a submenu
        menu.add("Give Us Your Feed Back");
        menu.add("Let's Do Business");
        menu.add("Help");       //Todo here ther is submenu
        menu.add(context.getString(R.string.setting3));
        menu.add("Footer");

        // TODO now leta add sub menu

        ArrayList<String> reminder = new ArrayList<>();
        ArrayList<String> backup_option = new ArrayList<>();
        ArrayList<String> help = new ArrayList<>();

        reminder.add(context.getString(R.string.sett2));
        reminder.add(context.getString(R.string.sett2_10));
        reminder.add(context.getString(R.string.sett2_15));
        reminder.add(context.getString(R.string.sett2_30));

        backup_option.add("Email");
        backup_option.add("URL");
        backup_option.add("Address");

        help.add("Privacy Policy");
        help.add("FAQ");

        sub_menu.put(menu.get(2) , reminder);
        sub_menu.put(menu.get(3) , backup_option);
        sub_menu.put(menu.get(6) , help);
    }

    public HashMap get_submenu(){
        return sub_menu;
    }

    public ArrayList get_menu(){
        return menu;
    }

    public void prepare_data2(){
        menu1 = new ArrayList<>();
        submenu1 = new HashMap<>();
//        menu1.add("Login");
        menu1.add(context.getString(R.string.nav1));
        menu1.add(context.getString(R.string.nav2));
        menu1.add(context.getString(R.string.nav3));
        menu1.add(context.getString(R.string.nav4));
        menu1.add(context.getString(R.string.nav5));
        menu1.add(context.getString(R.string.nav6));
        menu1.add(context.getString(R.string.nav7));
        menu1.add("Footer");
//        menu1.add("hareeshkumar1207@gmail.com");

        ArrayList<String> setting_option = new ArrayList<>();
        ArrayList<String> rate_us = new ArrayList<>();
        ArrayList<String> about_us = new ArrayList<>();
        ArrayList<String> backup_option = new ArrayList<>();

//        setting_option.add("Profile");
//        setting_option.add("Show message merged");
//        setting_option.add("Contact backup option");
//        setting_option.add("Reset");

//        rate_us.add("On Google Play Store");
//        rate_us.add("On Facebook");

        backup_option.add("Email");
        backup_option.add("URL");
        backup_option.add("Address");

//        about_us.add("Contact Us");
        about_us.add("Privacy");
        about_us.add("FAQ");

//        submenu1.put(menu1.get(0) , setting_option);
        submenu1.put(menu1.get(1) , backup_option);
        submenu1.put(menu1.get(5) , about_us);
//        submenu1.put(menu1.get(4) , rate_us);
    }

    public HashMap get_submenu1(){
        return submenu1;
    }

    public ArrayList get_menu1(){
        return menu1;
    }


}

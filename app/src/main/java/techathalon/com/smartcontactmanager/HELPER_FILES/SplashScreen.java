package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;

import com.facebook.FacebookSdk;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Calendar;
import java.util.Locale;

import techathalon.com.smartcontactmanager.CREATE_BACKUP.DisplayContactActivity;
import techathalon.com.smartcontactmanager.NOTIFICATION.NotificationBroadcastReceiver;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class SplashScreen extends Activity {
    //    Intent commonIntent;
    Intent intent;
    InterstitialAd mInterstitialAd;

    private void loadInterstitialAd() {
        if (Constants.enable_ads) {
            mInterstitialAd = new InterstitialAd(SplashScreen.this);
            mInterstitialAd.setAdUnitId(getResources().getString(R.string.full_screen_ad_id));
            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        loadInterstitialAd();
        intent = getIntent();
        SharedPreferences adpref = getSharedPreferences("adpref", Context.MODE_PRIVATE);
        SharedPreferences.Editor adEditor = adpref.edit();
        adEditor.putInt("adcount", 0);
        adEditor.apply();
        final ProgressDialog dialog;
        dialog = new ProgressDialog(SplashScreen.this);
        dialog.setMessage("Loading..");
        dialog.setIndeterminate(false);
        dialog.setMax(100);
        dialog.setCancelable(false);
        dialog.show();
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;
        sharedPreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        boolean isInstalled = sharedPreferences.getBoolean("isAppInstalled", false);
        if (!isInstalled) {
            Intent alarmIntent = new Intent(this, NotificationBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
            AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Calendar calendar = Calendar.getInstance();

            calendar.setTimeInMillis(System.currentTimeMillis());
            manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 3 * 24 * 60 * 60 * 1000, pendingIntent);
            editor = sharedPreferences.edit();
            editor.putBoolean("isAppInstalled", true);
            editor.commit();
        }

        if (intent != null) {
            final Uri uri = intent.getData();
            if (uri != null) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                    commonIntent = new Intent();
//                    commonIntent.setClass(SplashScreen.this, MainActivity.class);
                        if (uri.getScheme().equals("smartcontactmanager") || uri.getHost().equals("homeactivity")) {
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                                mInterstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        intent = new Intent(SplashScreen.this, MainActivity.class);
                                        startActivity(intent);
                                    }
                                });
                                loadInterstitialAd();
                            } else {
                                intent = new Intent(SplashScreen.this, MainActivity.class);
                                startActivity(intent);
                            }
                        }
                    }
                }, 4000);


            } else {
                try {
                    FacebookSdk.sdkInitialize(getApplicationContext());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                    commonIntent = new Intent();
//                    commonIntent.setClass(SplashScreen.this, MainActivity.class);
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                                mInterstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        intent = new Intent(SplashScreen.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                loadInterstitialAd();
                            } else {
                                intent = new Intent(SplashScreen.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }, 4000);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                FacebookSdk.sdkInitialize(getApplicationContext());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                    commonIntent = new Intent();
//                    commonIntent.setClass(SplashScreen.this, MainActivity.class);
                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                            mInterstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    intent = new Intent(SplashScreen.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            loadInterstitialAd();
                        } else {
                            intent = new Intent(SplashScreen.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }, 4000);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
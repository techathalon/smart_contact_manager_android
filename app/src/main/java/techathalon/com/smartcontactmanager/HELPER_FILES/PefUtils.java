package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.content.SharedPreferences;

import java.util.HashMap;

public class PefUtils {
    public static void saveRegisteredData(SharedPreferences pref, HashMap<String, String> users) {
        SharedPreferences.Editor edit = pref.edit();
        edit.putBoolean("islogin", true);
        edit.putString("user_id", users.get("user_id"));
        edit.putString("u_firstname", users.get("u_firstname"));
        edit.putString("u_lastname", users.get("u_lastname"));
        edit.putString("u_email", users.get("u_email"));
        edit.putString("registered_email", users.get("registered_email"));
        edit.putString("facebook_id", users.get("facebook_id"));
        edit.putString("password", users.get("password"));
        edit.putString("birthday", users.get("birthday"));
        edit.apply();
    }
}
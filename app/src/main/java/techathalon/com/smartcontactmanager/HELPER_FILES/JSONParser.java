package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class JSONParser {
    Activity context;
    URL url;
    private String response = "";
    private HttpURLConnection conn = null;
    OutputStream os = null;
    private BufferedWriter writer = null;
    private BufferedReader br = null;
    private String line;
    private StringBuilder result;

    public String performPostCall(String requestURL, HashMap<String, String> postDataParams, Activity activity) {
        try {
            this.context = activity;
            url = new URL(requestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(20000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            os = conn.getOutputStream();
            writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            Log.d("requestURL", requestURL);
            writer.write(getPostDataString(postDataParams));
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null)
                    response += line;
            } else {
                response = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (response != null) {
            Log.d("onResponse", response);
            return response;
        } else {
            showError();
            return "";
        }
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            Log.d("Key=" + entry.getKey(), " Value = " + entry.getValue() + " ");
        }
        return result.toString();
    }

    private void showError() {
        final Activity activity = context;
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, "Connection Error occurred try again", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.os.Environment;

import java.io.File;

public class Constants {
    public static String APP_NAME = "Smart Contact Manager";
//    public static String BASEURL = "http://www.familybuds.com/cellphonemanager/";
    //TODO new api
    public static String BASEURL = "https://www.techathalon.com/cellphonemanager/";
    public static String PREVIOUS_BACKUP_URL = BASEURL + "api_home/get_previous_contact_backup/2/android";
    public static String DELETE_CONTACT_BACKUP_URL = BASEURL + "api_home/deleteBackedUpdata/2/android";
    public static String REGISTER_USER_URL = BASEURL + "api_home/registerUser/2/android";
    public static String SAVE_PASSWORD_URL = BASEURL + "api_home/savePassword/2/android";
    public static String CHECK_PASSWORD_URL = BASEURL + "api_home/checkPassword/2/android";
    public static String SEND_PASSWORD_URL = BASEURL + "api_home/sendPassword_oc/2/android";
    public static String RESEND_EMAIL_URL = BASEURL + "api_home/sendBackupAgain_oc/2/android";
    public static String CHANGE_PASSWORD_URL = BASEURL + "api_home/changePassword/2/android";

    public static String smartLink1 = "https://goo.gl/knEznc";
    public static String smartLink = "https://play.google.com/store/apps/details?id=techathalon.com.smartcontactmanager&hl=en";
    public static String folder_path = Environment.getExternalStorageDirectory() + File.separator + "SCMContactBackup/";
    public static String logged_in_fb_id = "";
    public static boolean isLogin = false;
    public static String registered_email = "";
    public static boolean isPasswordValidated = false;
    public static boolean isFirstTime = false;


    //TODO the above line are important search this key words in entire project it might be
    // commented just uncomment it and set the values to true while publishing it on play store
    //TODO For Developers set above to false
    public static boolean enable_ads = true;
    //TODO this is very important for the income of SCM , Please make it true while publishing the app on play store
    public static boolean enable_tracker = true;
    // TODO this is the tracker please enable it while publishing the app on play store to maintain
    //  the activity record on play store
}
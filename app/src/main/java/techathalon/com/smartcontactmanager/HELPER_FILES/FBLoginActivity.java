package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.util.Arrays;

import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class FBLoginActivity extends MainActivity {
    CallbackManager callbackManager;
    Intent commonIntent;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Constants.enable_tracker){
            SCMApplication.getInstance().trackScreenView("Facebook Sharing");
        }
        LoginAndPost();
    }

    public void LoginAndPost() {
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        ShareDialog shareDialog = new ShareDialog(FBLoginActivity.this);
                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                            @Override
                            public void onSuccess(Sharer.Result result) {
                                Toast.makeText(getApplicationContext(), "Posted successfully", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onCancel() {
                            }

                            @Override
                            public void onError(FacebookException e) {
                                Toast.makeText(FBLoginActivity.this, "Facebook error occurred " + e.toString(), Toast.LENGTH_LONG).show();
                            }

                        });
                        if (ShareDialog.canShow(ShareLinkContent.class)) {
                            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                    .setContentTitle(getString(R.string.app_name_with_space))
                                    .setContentDescription("Good News...!!! Smart Contact Manager.\nIt's FREE for few days.\n" + "Download " + Constants.smartLink)
                                    .setContentUrl(Uri.parse(Constants.smartLink))
                                    .setImageUrl(Uri.parse(Constants.BASEURL +"public/ic_launcher.png"))
                                    .build();

                            shareDialog.show(linkContent);
                        }
                    }

                    @Override
                    public void onCancel() {
                        commonIntent = new Intent(FBLoginActivity.this, MainActivity.class);
                        startActivity(commonIntent);
                        finish();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(FBLoginActivity.this, "Facebook error occurred " + exception.toString(), Toast.LENGTH_LONG).show();
                        commonIntent = new Intent(FBLoginActivity.this, MainActivity.class);
                        startActivity(commonIntent);
                        finish();
                    }
                }
        );
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_birthday"));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
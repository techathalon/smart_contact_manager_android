package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.google.android.gms.plus.PlusShare;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

@SuppressLint("ValidFragment")
public class AppSharerDialog extends DialogFragment {
    Activity activity;
    String appName, description, link, appIcon;
    GridView gridview;
    Button cancel, ok;
    Intent commonIntent;
    private final static String WHATSSAP_APP_ID = "com.whatsapp";

    @SuppressLint("ValidFragment")
    public AppSharerDialog(HashMap<String, String> infoMap, Activity activity) {
        this.activity = activity;
        this.appName = infoMap.get("name");
        this.description = infoMap.get("description");
        this.link = infoMap.get("link");
        this.appIcon = infoMap.get("app icon");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (Activity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getDialog().getWindow() != null)
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.app_sharer_back));
        View layoutView = inflater.inflate(R.layout.app_sharer, container, false);
        if(Constants.enable_tracker){
            SCMApplication.getInstance().trackScreenView("Share App");
        }
        gridview = (GridView) layoutView.findViewById(R.id.gridView);
        cancel = (Button) layoutView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ok = (Button) layoutView.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ArrayList<String> intentList = new ArrayList<>();
        intentList.add("facebook");
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        List<ResolveInfo> matches = activity.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {

            if (info.activityInfo.packageName.toLowerCase().startsWith("com.whatsapp")) {
                if (!intentList.contains("whatsapp"))
                    intentList.add("whatsapp");
            }
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.tencent.mm")) {
                if (!intentList.contains("wechat"))
                    intentList.add("wechat");
            }
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.viber.voip")) {
                if (!intentList.contains("viber2"))
                    intentList.add("viber2");
            }
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter.android")) {
                if (!intentList.contains("twitter"))
                    intentList.add("twitter");
            }
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.google.android.apps.plus")) {
                if (!intentList.contains("googleplus"))
                    intentList.add("googleplus");
            }
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.google.android.talk")) {
                if (!intentList.contains("hangouts"))
                    intentList.add("hangouts");
            }
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.android.mms")) {
                if (!intentList.contains("sms"))
                    intentList.add("sms");
            }
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.google.android.gm")) {
                if (!intentList.contains("mail"))
                    intentList.add("mail");
            }
        }
        AppSharerAdapter adapter = new AppSharerAdapter(activity, intentList);
        gridview.setAdapter(adapter);
        final String msg = "Good News...!!! " + appName + "\nIt's FREE for few days.\n" + "Download " + Constants.smartLink1;
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dismiss();
                switch (view.getTag().toString()) {
                    case "facebook": {
                        commonIntent = new Intent(activity, FBLoginActivity.class);
                        activity.startActivity(commonIntent);
                        activity.finish();
                    }
                    break;
                    case "whatsapp": {
                        try {
                            commonIntent = new Intent();
                            commonIntent.setPackage("com.whatsapp");
                            commonIntent.setAction("android.intent.action.SEND");
                            commonIntent.setType("text/plain");
                            commonIntent.putExtra("android.intent.extra.TEXT", msg);
                            activity.startActivity(commonIntent);
                        } catch (ActivityNotFoundException anfe) {
                            anfe.printStackTrace();
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=" +
                                            WHATSSAP_APP_ID)));
                        }
                    }
                    break;
                    case "wechat": {
                        commonIntent = new Intent();
                        commonIntent.setPackage("com.tencent.mm");
                        commonIntent.setAction("android.intent.action.SEND");
                        commonIntent.setType("text/plain");
                        commonIntent.putExtra("android.intent.extra.TEXT", msg);
                        activity.startActivity(commonIntent);
                    }
                    break;
                    case "Viber": {
                        commonIntent = new Intent();
                        commonIntent.setPackage("com.viber.voip");
                        commonIntent.setAction("android.intent.action.SEND");
                        commonIntent.setType("text/plain");
                        commonIntent.putExtra("android.intent.extra.TEXT", msg);
                        activity.startActivity(commonIntent);
                    }
                    break;
                    case "twitter": {
                        commonIntent = new Intent();
                        commonIntent.setType("application/twitter");
                        commonIntent.setPackage("com.twitter.android");
                        commonIntent.setAction("android.intent.action.SEND");
                        commonIntent.setType("text/plain");
                        commonIntent.putExtra(Intent.EXTRA_STREAM, appIcon);
                        commonIntent.putExtra("android.intent.extra.TEXT", msg);
                        activity.startActivity(commonIntent);
                    }
                    break;
                    case "hangouts": {
                        commonIntent = new Intent();
                        commonIntent.setPackage("com.google.android.talk");
                        commonIntent.setAction("android.intent.action.SEND");
                        commonIntent.setType("text/plain");
                        commonIntent.putExtra("android.intent.extra.TEXT", msg);
                        activity.startActivity(commonIntent);
                    }
                    break;
                    case "googleplus": {
                        commonIntent = new PlusShare.Builder(activity)
                                .setType("text/plain")
                                .setText(msg)
                                .setContentUrl(Uri.parse(link))
                                .setStream(Uri.parse(link))
                                .getIntent();
                        activity.startActivityForResult(commonIntent, 0);
                    }
                    break;
                    case "sms": {
                        commonIntent = new Intent();
                        commonIntent.setPackage("com.android.mms");
                        commonIntent.setAction("android.intent.action.SEND");
                        commonIntent.setType("text/plain");
                        commonIntent.putExtra("android.intent.extra.TEXT", msg);
                        activity.startActivity(commonIntent);
                    }
                    break;
                    case "mail": {
                        commonIntent = new Intent();
                        commonIntent.setPackage("com.google.android.gm");
                        commonIntent.setAction("android.intent.action.SEND");
                        commonIntent.setType("text/plain");
                        commonIntent.putExtra(Intent.EXTRA_TITLE, appName);
                        commonIntent.putExtra(Intent.EXTRA_STREAM, appIcon);
                        commonIntent.putExtra("android.intent.extra.TEXT", msg);
                        activity.startActivity(commonIntent);
                    }
                    break;
                    default:
                        break;
                }
            }
        });
        return layoutView;
    }
}

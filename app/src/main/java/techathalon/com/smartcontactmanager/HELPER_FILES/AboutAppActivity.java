package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class AboutAppActivity extends HelperActivity {
    WebView content;
    private final String html = "<p><b><font size=6><font face=\"times new roman\"><center>Smart Contact</font></font></center></big></b><b><big><center><font size=6><font face=\"times new roman\">Manager</font></font></center></big></b><br/><br/><b><u><font size=4><font face=\"times new roman\">WHY THIS APP</font></u></font></b><br/><br/>Transfer Contacts from Phone A to Phone B in one click is made possible now.<br/>Step 1 : Download Smart Contact Manager.<br/>Step 2 : Enter your valid details.<br/>Step 3 : Take Contact backup, Email it, your backup is saved in Backup History.<br/>Step 4 : When user gets a new cell phone he just need to install the Smart Contact Manager.<br/><br/><b><font size=4><font face=\"times new roman\">LOGIN WITH SAME EMAIL ID AND PASSWORD You get all the previous backups in this app.</font></font></b><br/><br/><font size=4><font face=\"times new roman\"><u><b>CONTACTS BACKUP</b></u></font></font><br/><br/> ★★★ DONT WORRY if you loose your smart phones YOUR CONTACT are ALWAYS THERE ON this app. LIFETIME ★★★<ul style=\"list-style-type:circle\"><li>100% Secure, Reliable, Fast </li><li>LIFE TIME BACKUP IN OUR APP</li><li>Simple 4 digits password for your safety</li><li>Easy backup your cell phone contacts.</li><li>Backup → Email → Save contacts in one Click.</li><li>View, Email, delete your Previous Backup</li><li>Backup your contacts onto Remote Server</li><li>Contact Backup file is ready.</li></ul><br/><b><font size=3><font face=\"times new roman\"><center>Enjoy...!!!</font></font></center><br/><br/></b></p>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        if(Constants.enable_tracker){
            SCMApplication.getInstance().trackScreenView("About App");
        }
        setContentView(R.layout.activity_about_app);
        content = (WebView) findViewById(R.id.content);
        content.loadDataWithBaseURL("", html, "text/html", "UTF-8", "");
        content.getSettings().setLoadWithOverviewMode(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class HelperActivity extends AppCompatActivity {
    int STATUS_PERMISSION = 123;
    private static final int ALL_PERMISSION = 0;

    public void setBackApiResponse1(String url, String object) {
    }

    public void setBackApiResponse2(String url , String object){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        SharedPreferences pref = getSharedPreferences(Constants.APP_NAME, 0);
        Constants.logged_in_fb_id = pref.getString("facebook_id", "");
        Constants.isLogin = pref.getBoolean("islogin", false);
        Constants.registered_email = pref.getString("registered_email", "");
    }

    public String getVersion() {
        try {
            PackageManager packageManager = getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "?";
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        Boolean flag = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        return flag;
    }

    public boolean isContactPermissionTaken() {
        return ContextCompat.checkSelfPermission
                (HelperActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission
                (HelperActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission
                (HelperActivity.this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED;
    }


    public boolean allPermission() {
        int readStoragePermission = ActivityCompat.checkSelfPermission(HelperActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermission = ActivityCompat.checkSelfPermission(HelperActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readContacts = ActivityCompat.checkSelfPermission(HelperActivity.this, Manifest.permission.READ_CONTACTS);
        int writeContacts = ActivityCompat.checkSelfPermission(HelperActivity.this, Manifest.permission.READ_CONTACTS);
        int readphoneState = ActivityCompat.checkSelfPermission(HelperActivity.this, Manifest.permission.READ_CONTACTS);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (writeStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (readContacts != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (writeContacts != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_CONTACTS);
        }
        if (readphoneState != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(HelperActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), ALL_PERMISSION);
            return false;
        }
        return true;
    }
}
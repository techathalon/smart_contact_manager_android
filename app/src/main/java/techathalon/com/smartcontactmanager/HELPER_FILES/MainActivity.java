package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.ContactsContract;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.ChangeBounds;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

import techathalon.com.smartcontactmanager.ADAPTERS.DrawerExpandableListAdapter;
import techathalon.com.smartcontactmanager.BACKUP_HISTORY.BackupHistoryActivity;
import techathalon.com.smartcontactmanager.BACKUP_HISTORY.PinActivity;
import techathalon.com.smartcontactmanager.CREATE_BACKUP.DisplayContactActivity;
import techathalon.com.smartcontactmanager.CREATE_BACKUP.TakeBackupActivity;
import techathalon.com.smartcontactmanager.DUPLICATE_SCAN.NewDuplicateActivity;
import techathalon.com.smartcontactmanager.MORE_APPS.MoreAppsActivity;
import techathalon.com.smartcontactmanager.MenuData.PrepareListDataDrawer;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.NOTIFICATION.NotificationBroadcastReceiver;
import techathalon.com.smartcontactmanager.SETTING.ChangePasswordActivity;
import techathalon.com.smartcontactmanager.SETTING.HelpActivity;
import techathalon.com.smartcontactmanager.SETTING.SettingsActivity;
import techathalon.com.smartcontactmanager.app.SCMApplication;  //TODO Trakers disabled

public class MainActivity extends HelperActivity {
    private ConstraintLayout constraintLayout;
    private ConstraintSet applyConstraintSet;
    private ChangeBounds first, second, third, fourth;
    private TransitionSet transitionSet;
    private DrawerLayout drawerLayout;

    ExpandableListView my_expandableListView;
    PrepareListDataDrawer prepareListDataDrawer;
    private ImageView take_backup;
    private ImageView scan_for_duplicate;
    private ImageView delete_multiple;
    private ImageView backup_history;

    private ImageView take_backup_icon;
    private ImageView take_backup_text;

    private ImageView scan_for_duplicate_icon;
    private ImageView scan_for_duplicate_text;

    private ImageView delete_multiple_icon;
    private ImageView delete_multiple_text;

    private ImageView backup_history_icon;
    private ImageView backup_history_text;
    InterstitialAd mInterstitialAd;

    //TODO this object will be need to tick and untick the the option menu of Contact Backup options
    private ContactBackup contactBackup_options;

    //TODO for tool bar
    private Toolbar my_toolbar;
    private ActionBar actionBar;
    //TODO Expanddable list view in home screen
    private ArrayList menu;
    private HashMap sub_menu;
    private DrawerExpandableListAdapter drawerExpandableListAdapter;
    public static final int SDK = Build.VERSION.SDK_INT;
    public static int expandable_menu_position = -1;

    private SharedPreferences prefs, pinPrefs, reninderPrefs, emailPrefs, reschedulePrefs, userDataPrefs, adpref;
    private SharedPreferences animation_prefer;
    ImageView settings, aboutApp, appShare, email, rate;

    //    LinearLayout createBackup, scanDuplicateContacts, backupHistory, firstHistory, secondHistory;
//    TextView backupText, scanDuplicateText, historyText, totalContacts, firstHistoryTextview, secondHistoryTextview;
    String user_id = "", firstHistoryString = "", secondHistoryString = "", emailContent, addedDateString = "";
    JSONArray jsonBackupArray;
    JSONObject jsonObject;
    SharedPreferences.Editor editEmail, reschedulePrefsEditor, userDataPrefsEditor;
    HttpHelper1 httpHelper1;
    HashMap<String, String> hashMap;
    int mygender;

    RadioGroup gender;
    Dialog loginDialog;
    SharedPreferences.Editor reminderEdit;

    PendingIntent pendingIntent;
    AlarmManager alarmManager;
    Intent alarmIntent, commonIntent;
    final static int RQS_1 = 1;
    int total = 0;
    int adcount = 0;


    @Override
    public void setBackApiResponse1(String url, String object) {
        //TODO this method is called by a asyc task inside HttpHelper1
        super.setBackApiResponse1(url, object);
        try {
            if (url.equals(Constants.REGISTER_USER_URL)) {
                if (object != null && !object.equals("")) {
                    JSONObject jsonObject = new JSONObject(object);
                    try {
                        String success = jsonObject.optString("success");
                        if (success.equalsIgnoreCase("true")) {
                            if (loginDialog != null && loginDialog.isShowing())
                                loginDialog.dismiss();
                            SharedPreferences.Editor toggleEdit = pinPrefs.edit();
                            toggleEdit.putString("pin", "set");
                            toggleEdit.putString("merge", "set");
                            toggleEdit.putString("emailCheckbox", "checked");
                            toggleEdit.putString("addressCheckbox", "checked");
                            toggleEdit.putString("noteCheckbox", "checked");
                            toggleEdit.putString("urlCheckbox", "checked");
                            toggleEdit.apply();

                            JSONArray jsonArray = jsonObject.getJSONArray("user_details");
                            JSONObject innerObject;
                            if (jsonArray.length() > 0) {
                                innerObject = jsonArray.getJSONObject(0);
                                hashMap = new HashMap<>();
                                hashMap.put("user_id", innerObject.optString("user_id"));
                                user_id = innerObject.optString("user_id");
                                httpHelper1 = new HttpHelper1(Constants.PREVIOUS_BACKUP_URL, hashMap, MainActivity.this, "");
                                hashMap = new HashMap<>();
                                hashMap.put("user_id", innerObject.optString("user_id"));
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("email-id", emailContent);
                                editor.putBoolean("isflagLogin", true);
                                editor.putString("user_id", user_id);
                                editor.apply();
                                drawerExpandableListAdapter.notifyDataSetChanged();   //TODO In order the update the email content in the drawer layout at th bottom
                                userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
                                userDataPrefsEditor = userDataPrefs.edit();
                                userDataPrefsEditor.putString("user_id", innerObject.optString("user_id"));
                                userDataPrefsEditor.putString("registered_email", innerObject.optString("registered_email"));
                                userDataPrefsEditor.putBoolean("isLogin", true);
                                userDataPrefsEditor.apply();

                                hashMap.put("u_firstname", innerObject.optString("u_firstname"));
                                hashMap.put("u_lastname", innerObject.optString("u_lastname"));
                                hashMap.put("u_email", innerObject.optString("u_email"));
                                hashMap.put("registered_email", innerObject.optString("registered_email"));
                                hashMap.put("facebook_id", innerObject.optString("facebook_id"));
                                hashMap.put("password", innerObject.optString("password"));
                                hashMap.put("birthday", innerObject.optString("u_birthday"));

                                editEmail = emailPrefs.edit();
                                editEmail.putString("user email", innerObject.optString("registered_email"));
                                editEmail.apply();

                                SharedPreferences pref = getSharedPreferences("LOGIN", 0);
                                PefUtils.saveRegisteredData(pref, hashMap);
                                Constants.registered_email = innerObject.optString("registered_email");
                                Constants.isLogin = true;
                            }
                            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            alarmIntent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                            alarmIntent.putExtra("option", "5");

                            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
                            alarmManager.cancel(pendingIntent);
                            pendingIntent.cancel();
                            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);

                            reninderPrefs = getSharedPreferences("reminder", Context.MODE_PRIVATE);
                            reminderEdit = reninderPrefs.edit();
                            reminderEdit.putString("option", "5");
                            reminderEdit.apply();

                            Calendar calendar = Calendar.getInstance();
                            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");
                            Date date = new Date();
                            String data = dateFormat.format(date);
                            calendar.add(Calendar.HOUR, 120);
//                            calendar.add(Calendar.MINUTE, 1);  //TODO for testing purpose
                            Date addedDate = calendar.getTime();
                            addedDateString = dateFormat.format(addedDate);
                            reschedulePrefs = getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                            reschedulePrefsEditor = reschedulePrefs.edit();
                            reschedulePrefsEditor.putString("alarm_time", addedDateString);
                            reschedulePrefsEditor.putString("option", "5");
                            reschedulePrefsEditor.apply();
                            if (data.contains("PM") || data.contains("pm")) {
                                calendar.set(Calendar.AM_PM, Calendar.PM);
                            } else {
                                calendar.set(Calendar.AM_PM, Calendar.AM);
                            }
                            data = calendar.get(Calendar.DAY_OF_MONTH) + "";
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            } else {
                                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.some_error_occurred_msg), Toast.LENGTH_SHORT).show();
                }
            } else {
                if (object != null && !object.equals("")) {
                    JSONObject jsonObject = new JSONObject(object);
                    this.jsonObject = jsonObject;
                    try {
                        String success = jsonObject.optString("success");
                        if (success.equalsIgnoreCase("true")) {
                            jsonBackupArray = jsonObject.getJSONArray("file_backup");
                            editEmail = emailPrefs.edit();
                            editEmail.putInt("total backups", jsonBackupArray.length());
                            editEmail.apply();
                            if (jsonBackupArray.length() > 0) {
//                                historyText.setVisibility(View.GONE);
                                JSONObject jsonObject1;
                                for (int i = 0; i < jsonBackupArray.length(); i++) {
                                    try {
                                        jsonObject1 = jsonBackupArray.getJSONObject(i);
                                        if (i == 0) {
//                                            firstHistory.setVisibility(View.VISIBLE);
                                            firstHistoryString = jsonObject1.optString("file_name");
//                                            firstHistoryTextview.setText(firstHistoryString);
                                            SharedPreferences.Editor editor = prefs.edit();
                                            editor.putString("last_backup_date", jsonObject1.optString("created_date"));
                                            editor.apply();
                                        }
                                        if (i == 1) {
//                                            secondHistory.setVisibility(View.VISIBLE);
                                            secondHistoryString = jsonObject1.optString("file_name");
//                                            secondHistoryTextview.setText(secondHistoryString);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
//                                historyText.setVisibility(View.VISIBLE);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.some_error_occurred_msg), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
        user_id = userDataPrefs.getString("user_id", "");
        if (!user_id.equalsIgnoreCase("")) {
            hashMap = new HashMap<>();
            hashMap.put("user_id", user_id);
            httpHelper1 = new HttpHelper1(Constants.PREVIOUS_BACKUP_URL, hashMap, MainActivity.this, "");
        }

        if (!Constants.isFirstTime) {
            Constants.isFirstTime = true;
            final Dialog dialog = new Dialog(this);
            if (dialog.getWindow() != null)
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.ball_hit_dialog_background);
            dialog.setCanceledOnTouchOutside(true);

//        onWindowFocusChanged(true);
            dialog.setContentView(R.layout.ballhit_dialog_layout);
            LinearLayout lin_main = dialog.findViewById(R.id.lin_main);
            ImageView img_icon = dialog.findViewById(R.id.img_icon);
            ImageView try_it = dialog.findViewById(R.id.try_it);
            ImageView free = dialog.findViewById(R.id.free);
//            Animation shake = AnimationUtils.loadAnimation(this, R.anim.cycle_7);
            Animation animation = new AlphaAnimation(1, 0);
            animation.setDuration(500);
            animation.setInterpolator(new LinearInterpolator());
            animation.setRepeatCount(Animation.INFINITE);
            animation.setRepeatMode(Animation.REVERSE);
            try_it.startAnimation(animation);
            free.startAnimation(animation);
//            img_icon.startAnimation(animation);
//            img_icon.startAnimation(shake);

            lin_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();
                    commonIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=allinonegame.techathalon.com.smashballhit"));
                    startActivity(commonIntent);
                }
            });
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
//            dialog.show();
        }

    }


    private void loadInterstitialAd() {
        if (Constants.enable_ads) {
            mInterstitialAd = new InterstitialAd(MainActivity.this);
            mInterstitialAd.setAdUnitId(getResources().getString(R.string.full_screen_ad_id));
            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadInterstitialAd();
        drawerLayout = findViewById(R.id.activity_main_drawer);
        my_toolbar = findViewById(R.id.smart_tool_bar);
        setSupportActionBar(my_toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_icon);
        getSupportActionBar().setTitle(R.string.app_name_with_space);

        adpref = getSharedPreferences("adpref", Context.MODE_PRIVATE);
        adcount = adpref.getInt("adcount", 0);
        //TODO Now for Expandable list view in action bar
        my_expandableListView = findViewById(R.id.nav_item1);
        prepareListDataDrawer = new PrepareListDataDrawer(getApplicationContext());
//        prepareListDataDrawer.prepare_data();
        prepareListDataDrawer.prepare_data2();

        //TODO related to preparing list content of Expandable list view
//        menu = prepareListDataDrawer.get_menu();
//        sub_menu = prepareListDataDrawer.get_submenu();

        menu = prepareListDataDrawer.get_menu1();
        sub_menu = prepareListDataDrawer.get_submenu1();

        contactBackup_options = new ContactBackup(this);

        //TODO Expandable listview adapter intialization
        try {
            drawerExpandableListAdapter = new DrawerExpandableListAdapter(this, menu, sub_menu);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        my_expandableListView.setAdapter(drawerExpandableListAdapter);

        //TODO Onclick listener in expandable list view menu is clicked
        my_expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int menu_position, long sub_menu_position) {
                return false;
            }
        });

        my_expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int menu_position) {
                switch (menu_position) {
                    case 0:
                        my_expandableListView.collapseGroup(menu_position);
                        prefs = getSharedPreferences("data", Context.MODE_PRIVATE);
                        emailPrefs = getSharedPreferences("email data", Context.MODE_PRIVATE);
                        Boolean flag_login = prefs.getBoolean("isflagLogin", false);
                        if (isNetworkAvailable()) {
                            if (flag_login == false) {
                                alert_dialog_for_register_email(1);
                            } else {
                                SharedPreferences pref = getSharedPreferences("LOGIN", 0);
                                my_expandableListView.collapseGroup(menu_position); //TODO this statement is essential as it is an expandable list view every view even without expand need to be collapsed
                                if (pref.getString("password", "").equalsIgnoreCase("")) {
                                    commonIntent = new Intent(MainActivity.this, PinActivity.class);
                                    commonIntent.putExtra("first time", "yes");
                                    commonIntent.putExtra("from settings", "settings");
                                    startActivity(commonIntent);
//                            finish();
                                } else {
                                    commonIntent = new Intent(MainActivity.this, ChangePasswordActivity.class);
                                    startActivity(commonIntent);
//                            finish();
                                }
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        }

//                        commonIntent = new Intent(MainActivity.this, SettingsActivity.class);
//                        startActivity(commonIntent);
//                        finish();
                        break;
                    case 1:
//                        my_expandableListView.collapseGroup(menu_position);
//                        if (expandable_menu_position != 1 && menu_position != expandable_menu_position) {
//                            my_expandableListView.collapseGroup(expandable_menu_position);
//                        }
                        if (expandable_menu_position != -1 && menu_position != expandable_menu_position) {
                            my_expandableListView.collapseGroup(expandable_menu_position);
                        }
                        expandable_menu_position = menu_position;
                        break;

                    case 2:
//                        if (expandable_menu_position != 1 && menu_position != expandable_menu_position) {
//                            my_expandableListView.collapseGroup(expandable_menu_position);
//                        }
                        my_expandableListView.collapseGroup(menu_position);
//                        expandable_menu_position = menu_position;
                        commonIntent = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(commonIntent);
//                        finish();
                        break;

                    case 3:
//                        if (expandable_menu_position != -1 && menu_position != expandable_menu_position) {
//                            my_expandableListView.collapseGroup(expandable_menu_position);
//                        }
                        my_expandableListView.collapseGroup(menu_position);
                        expandable_menu_position = menu_position;
                        my_expandableListView.collapseGroup(menu_position);
                        letDoBusiness();

                        break;

                    case 4:
                        my_expandableListView.collapseGroup(menu_position);
//                        if (expandable_menu_position != -1 && menu_position != expandable_menu_position) {
//                            my_expandableListView.collapseGroup(expandable_menu_position);
//                        }
//                        expandable_menu_position = menu_position;
                        commonIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=techathalon.com.smartcontactmanager"));
                        startActivity(commonIntent);
//                        my_expandableListView.collapseGroup(menu_position);
//                        commonIntent = new Intent(Intent.ACTION_VIEW);
//                        commonIntent.setType("plain/text");
//                        commonIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name_with_space));
//                        commonIntent.setData(Uri.parse("mailto:" + "info@techathalon.com"));
//                        startActivity(Intent.createChooser(commonIntent, "Send Feedback..."));
                        break;
                    case 5:
                        if (expandable_menu_position != -1 && menu_position != expandable_menu_position) {
                            my_expandableListView.collapseGroup(expandable_menu_position);
                        }
                        expandable_menu_position = menu_position;
                        break;
                    case 6:
                        my_expandableListView.collapseGroup(menu_position);
                        commonIntent = new Intent(MainActivity.this, MoreAppsActivity.class);
                        startActivity(commonIntent);
//                        finish();
                        break;
                }

            }
        });
        my_expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int menu_position) {

            }
        });
        my_expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int menu_position, int sub_menu_position, long l) {
                ImageView checked;
                switch (menu_position) {
                    case 1:
                        switch (sub_menu_position) {
                            case 0:
                                contactBackup_options.set_checked_value("email");
                                break;
                            case 1:
                                contactBackup_options.set_checked_value("url");
                                break;
                            case 2:
                                contactBackup_options.set_checked_value("address");
                                break;
                        }
                        drawerExpandableListAdapter.notifyDataSetChanged();
                        break;
                    case 2: //TODO means "Do You Want Reminder"
//                        checked = view.findViewById(R.id.checkable_image);
//                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
//                            checked.setBackgroundDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_check));
//                        } else {
//                            checked.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_check));
//                        }
//                        drawerExpandableListAdapter.REMINDER_CHECK_LOCATION = sub_menu_position;
//                        drawerExpandableListAdapter.notifyDataSetChanged(); //TODO this statement refreshes the view without changes in the UI that user is currently viewing`
                        break;
                    case 3://TODO means "Contact Backup Options"
//                        checked = view.findViewById(R.id.checkable_image);
//                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
//                            checked.setBackgroundDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_check));
//                        } else {
//                            checked.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_check));
//                        }
//                        switch (sub_menu_position) {
//
//                            case 0:
//                                if (drawerExpandableListAdapter.CONTACT_BACKUP_OPTION_LOCATION[0] == -1) {
//                                    drawerExpandableListAdapter.CONTACT_BACKUP_OPTION_LOCATION[0] = 1;
//                                } else {
//                                    drawerExpandableListAdapter.CONTACT_BACKUP_OPTION_LOCATION[0] = -1;
//                                }
//                                drawerExpandableListAdapter.notifyDataSetChanged();
//                                break;
//                            case 1:
//                                if (drawerExpandableListAdapter.CONTACT_BACKUP_OPTION_LOCATION[1] == -1) {
//                                    drawerExpandableListAdapter.CONTACT_BACKUP_OPTION_LOCATION[1] = 1;
//                                } else {
//                                    drawerExpandableListAdapter.CONTACT_BACKUP_OPTION_LOCATION[1] = -1;
//                                }
//                                drawerExpandableListAdapter.notifyDataSetChanged();
//                                break;
//                            case 2:
//                                if (drawerExpandableListAdapter.CONTACT_BACKUP_OPTION_LOCATION[2] == -1) {
//                                    drawerExpandableListAdapter.CONTACT_BACKUP_OPTION_LOCATION[2] = 1;
//                                } else {
//                                    drawerExpandableListAdapter.CONTACT_BACKUP_OPTION_LOCATION[2] = -1;
//                                }
//                                drawerExpandableListAdapter.notifyDataSetChanged();
//                                break;
//                        }
                        break;

                    case 4:
                        switch (sub_menu_position) {
                            case 0:

                                break;
                            case 1:
                                break;
                        }
                        break;


                    case 5:
                        switch (sub_menu_position) {
                            case 0:
                                if (isNetworkAvailable()) {
                                    commonIntent = new Intent(MainActivity.this, HelpActivity.class);
                                    commonIntent.putExtra("help", 1);
                                    startActivity(commonIntent);
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                                }
                                break;
                            case 1:
                                if (isNetworkAvailable()) {
                                    commonIntent = new Intent(MainActivity.this, HelpActivity.class);
                                    commonIntent.putExtra("help", 2);
                                    startActivity(commonIntent);
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                                }
                                break;
                        }
                        break;
                }
                return false;
            }
        });

        constraintLayout = findViewById(R.id.cont_home);
        take_backup = findViewById(R.id.img_take_backup_button);
        take_backup_icon = findViewById(R.id.img_take_backup_icon);
        take_backup_text = findViewById(R.id.img_take_backup_text);

        scan_for_duplicate = findViewById(R.id.img_scan_for_duplicate_button);
        scan_for_duplicate_icon = findViewById(R.id.img_scan_for_duplicate_icon);
        scan_for_duplicate_text = findViewById(R.id.img_scan_for_duplicate_text);

        delete_multiple = findViewById(R.id.img_delete_multiple_button);
        delete_multiple_icon = findViewById(R.id.img_delete_multiple_icon);
        delete_multiple_text = findViewById(R.id.img_delete_multiple_text);

        backup_history = findViewById(R.id.img_backup_history_button);
        backup_history_icon = findViewById(R.id.img_backup_history_icon);
        backup_history_text = findViewById(R.id.img_backup_history_text);

        applyConstraintSet = new ConstraintSet();

        //TODO ////////////////////////////////////////////////TODO/////////////////////////////////////////////////////////////////

//        if (getSupportActionBar() != null)
//            getSupportActionBar().hide();

        if (Constants.enable_tracker) {
            SCMApplication.getInstance().trackScreenView("Home Screen");   //TODO Tracker disabled for google analytics
        }


        pinPrefs = getSharedPreferences("toggle flag", Context.MODE_PRIVATE);
        userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
        animation_prefer = getSharedPreferences("AnimationDone", Context.MODE_PRIVATE);
        animation_prefer.edit().putBoolean("animation_complete", false).commit();
        jsonBackupArray = new JSONArray();
//        firstHistory = (LinearLayout) findViewById(R.id.first_history);
//        secondHistory = (LinearLayout) findViewById(R.id.second_history);
//        firstHistoryTextview = (TextView) findViewById(R.id.first_history_textview);
//        secondHistoryTextview = (TextView) findViewById(R.id.second_history_textview);
//        email = (ImageView) findViewById(R.id.more_apps);
//        email.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                commonIntent = new Intent(MainActivity.this, MoreAppsActivity.class);
//                commonIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                startActivity(commonIntent);
//                finish();
//            }
//        });

//        rate = (ImageView) findViewById(R.id.rate);
//        rate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                commonIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("https://play.google.com/store/apps/details?id=techathalon.com.smartcontactmanager"));
//                startActivity(commonIntent);
//            }
//        });

//        createBackup = (LinearLayout) findViewById(R.id.create_backup);
//        createBackup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (ContextCompat.checkSelfPermission
//                        (MainActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
//                        && ContextCompat.checkSelfPermission
//                        (MainActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
//                        && ContextCompat.checkSelfPermission
//                        (MainActivity.this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
//                    commonIntent = new Intent(MainActivity.this, TakeBackupActivity.class);
//                    startActivity(commonIntent);
//                    finish();
//                } else {
//                    Toast.makeText(MainActivity.this, "Please enable CONTACT,STORAGE & PHONE permission for this app in Setting in order to use this feature.", Toast.LENGTH_LONG).show();
//                }
//            }
//        });
        //TODO Take Backup button
//        createBackup = (LinearLayout) findViewById(R.id.create_backup);
        take_backup.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(final View v) {
                if (ContextCompat.checkSelfPermission
                        (MainActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission
                        (MainActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission
                        (MainActivity.this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

                    prefs = getSharedPreferences("data", Context.MODE_PRIVATE);
                    emailPrefs = getSharedPreferences("email data", Context.MODE_PRIVATE);
                    Boolean flag_login = prefs.getBoolean("isflagLogin", false);
                    if (isNetworkAvailable()) {
                        if (flag_login == false) {
                            alert_dialog_for_register_email(3);
                        } else {
                            if (adcount%2==1){
                                mInterstitialAd.show();
                                mInterstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        commonIntent = new Intent(MainActivity.this, TakeBackupActivity.class);
                                        animation_click(v, commonIntent);
                                    }
                                });
                                loadInterstitialAd();
                            }else {
                                commonIntent = new Intent(MainActivity.this, TakeBackupActivity.class);
                                animation_click(v, commonIntent);
                            }
                            adcount++;
                            SharedPreferences.Editor adEditor = adpref.edit();
                            adEditor.putInt("adcount", adcount);
                            adEditor.apply();

                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                    }
//                    startActivity(commonIntent);
//                    finish();
//                    overridePendingTransition(R.anim.slide_in_reverse_left, R.anim.slide_in_reverse_right);
                } else {
                    Toast.makeText(MainActivity.this, "Please enable CONTACT,STORAGE & PHONE permission for this app in Setting in order to use this feature.", Toast.LENGTH_LONG).show();
                }
            }
        });

        //TODO Scan for duplicate button new design
        scan_for_duplicate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission
                        (MainActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission
                        (MainActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission
                        (MainActivity.this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    commonIntent = new Intent(MainActivity.this, NewDuplicateActivity.class);
                    animation_click(v, commonIntent);
//                    startActivity(commonIntent);
//                    finish();
//                    overridePendingTransition(R.anim.slide_in_left , R.anim.slide_in_right);
                } else {
                    Toast.makeText(MainActivity.this, "Please enable CONTACT,STORAGE & PHONE permission for this app in Setting in order to use this feature.", Toast.LENGTH_LONG).show();
                }
            }
        });

// //TODO New Design for smart contact manager
        backup_history.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(final View v) {
//                Toast.makeText(MainActivity.this , "Its woking" , Toast.LENGTH_SHORT).show();
                prefs = getSharedPreferences("data", Context.MODE_PRIVATE);
                emailPrefs = getSharedPreferences("email data", Context.MODE_PRIVATE);
                Boolean flag_login = prefs.getBoolean("isflagLogin", false);
                if (isNetworkAvailable()) {
                    if (flag_login == false) {
                        alert_dialog_for_register_email(2);
                    } else {
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (mInterstitialAd.isLoaded()) {

                                ////

                                if (adcount%2==1){
                                    mInterstitialAd.show();
                                    mInterstitialAd.setAdListener(new AdListener() {
                                        @Override
                                        public void onAdClosed() {
                                            super.onAdClosed();
                                            commonIntent = new Intent(MainActivity.this, BackupHistoryActivity.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("history", jsonObject.toString());
                                            commonIntent.putExtras(bundle);
                                            animation_click(v, commonIntent);
                                        }
                                    });
                                    loadInterstitialAd();
                                }else {
                                    commonIntent = new Intent(MainActivity.this, BackupHistoryActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("history", jsonObject.toString());
                                    commonIntent.putExtras(bundle);
                                    animation_click(v, commonIntent);
                                }
                                adcount++;
                                SharedPreferences.Editor adEditor = adpref.edit();
                                adEditor.putInt("adcount", adcount);
                                adEditor.apply();



                            } else {
                                commonIntent = new Intent(MainActivity.this, BackupHistoryActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("history", jsonObject.toString());
                                commonIntent.putExtras(bundle);
                                animation_click(v, commonIntent);
                            }

//                        startActivity(commonIntent);
//                        finish();
                        } else {
                            if (!isNetworkAvailable()) {
                                Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(MainActivity.this, "Server Under Maintenance, Please try after some time", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
//                    if (jsonObject != null && jsonObject.length() > 0) {
//                        commonIntent = new Intent(MainActivity.this, BackupHistoryActivity.class);
//                        Bundle bundle = new Bundle();
//                        bundle.putString("history", jsonObject.toString());
//                        commonIntent.putExtras(bundle);
//                        animation_click(v, commonIntent);
////                        startActivity(commonIntent);
////                        finish();
//                    }
//                    else{
//                        Toast.makeText(getApplicationContext(), "Hey I am here as you assumed", Toast.LENGTH_LONG).show();
//                    }

                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                }
            }
        });

        delete_multiple.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(final View v) {

                if (mInterstitialAd.isLoaded()) {
                    if (adcount%2==1){
                        mInterstitialAd.show();
                        mInterstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                commonIntent = new Intent(MainActivity.this, DisplayContactActivity.class);
                                animation_click(v, commonIntent);
                            }
                        });
                        loadInterstitialAd();
                    }else {
                        commonIntent = new Intent(MainActivity.this, DisplayContactActivity.class);
                        animation_click(v, commonIntent);
                    }
                    adcount++;
                    SharedPreferences.Editor adEditor = adpref.edit();
                    adEditor.putInt("adcount", adcount);
                    adEditor.apply();

                } else {
                    commonIntent = new Intent(MainActivity.this, DisplayContactActivity.class);
                    animation_click(v, commonIntent);
                }


            }
        });

//        aboutApp = (ImageView) findViewById(R.id.about_app);
//        aboutApp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                commonIntent = new Intent(MainActivity.this, AboutAppActivity.class);
//                startActivity(commonIntent);
//                finish();
//            }
//        });

//        backupText = (TextView) findViewById(R.id.backup_textview);
//        scanDuplicateText = (TextView) findViewById(R.id.scan_duplicate_textview);
//        historyText = (TextView) findViewById(R.id.backup_history_textview);
//        totalContacts = (TextView) findViewById(R.id.total_contacts_textview);

        if (!pinPrefs.getString("last backup", "").equalsIgnoreCase("")) {
//            backupText.setText("Last Backup : " + pinPrefs.getString("last backup", ""));
//            totalContacts.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission
                        (MainActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission
                        (MainActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission
                        (MainActivity.this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    afterPermissionGranted();
                } else {
                    Toast.makeText(MainActivity.this, "Please enable CONTACT,STORAGE & PHONE permission for this app in Setting in order to use this feature.", Toast.LENGTH_LONG).show();
                }
            } else {
                afterPermissionGranted();
            }
        }

//        if (!pinPrefs.getString("scan duplicate", "").equalsIgnoreCase("")) {
//            scanDuplicateText.setText("Last scan : " + pinPrefs.getString("scan duplicate", ""));
//        }


        settings = (ImageView) findViewById(R.id.settings);
//        settings.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                commonIntent = new Intent(MainActivity.this, SettingsActivity.class);
//                startActivity(commonIntent);
//                finish();
//            }
//        });

        // TODO Flow starts from here
        prefs = getSharedPreferences("data", Context.MODE_PRIVATE);
        emailPrefs = getSharedPreferences("email data", Context.MODE_PRIVATE);
        Boolean flag_login = prefs.getBoolean("isflagLogin", false);

        pinPrefs = getSharedPreferences("toggle flag", Context.MODE_PRIVATE);
        if (!flag_login) {
            if (isPermissionGiven()) {
//                renderLoginDialog();
            } else {
                askforPermission();
            }
        }
//        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.setContentView(R.layout.ballhit_dialog_layout);
//        LinearLayout lin_main = (LinearLayout) dialog.findViewById(R.id.lin_main);
//        dialog.setCancelable(true);
//
//        lin_main.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                commonIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.smartLink));
//                startActivity(commonIntent);
//            }
//        });
//        dialog.show();

    }


    private void afterPermissionGranted() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                if (cursor != null) {
                    total = cursor.getCount();
                    cursor.close();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (total == 0) {
//                    totalContacts.setText("No Contacts");
                } else {
//                    totalContacts.setText(total + " Contacts");
                }
            }
        }.execute();
    }

    public void renderLoginDialog() {
        loginDialog = new Dialog(MainActivity.this);
        if (loginDialog.getWindow() != null)
            loginDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loginDialog.getWindow().setBackgroundDrawableResource(R.drawable.round_corner_layout_white);
        loginDialog.setCancelable(false);
        loginDialog.setContentView(R.layout.login_dialog);
//        loginDialog.setContentView(R.layout.register_user);
        final EditText email = (EditText) loginDialog.findViewById(R.id.email_edittext);
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                email.setBackgroundResource(R.drawable.round_background_login);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        final EditText phoneNumber = (EditText) loginDialog.findViewById(R.id.phone_number_edittext);
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(getApplicationContext()).getAccounts();  //TODO this code here adds google account registered to current device

        String possibleEmail = "";
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                possibleEmail = account.name;
            }
        }

        Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isfirstrun", true);
        if (isFirstRun) {
            email.setText(possibleEmail);
            getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("isfirstrun", false).apply();   // TODO now here the emeil has removed the value
        } else {
            if (emailPrefs.getString("user email", "").equalsIgnoreCase(""))
                email.setText(possibleEmail);
            else
                email.setText(emailPrefs.getString("user email", ""));
        }
        TextView ok = (TextView) loginDialog.findViewById(R.id.ok_textview);  //TODO this is actually Login button
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailContent = email.getText().toString();
                final String number = phoneNumber.getText().toString();
                if (isValidEmailid(emailContent)) {
                    email.setBackgroundResource(R.drawable.round_background_login);
                }
                if (emailContent.equals("")) {
                    email.setBackgroundResource(R.drawable.round_red_background_login);
                } else if (!isValidEmailid(emailContent)) {
                    email.setBackgroundResource(R.drawable.round_red_background_login);
                } else {
                    loginDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
                    alertBuilder.setTitle("Confirmation");
                    alertBuilder.setCancelable(false);
                    if (!number.equals(""))
                        alertBuilder.setMessage("Is this your email id '" + emailContent + "' ?\n" + " Is '" + number + "' your phone number?");
                    else
                        alertBuilder.setMessage("Is this your email id '" + emailContent + "' ?");
                    alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            gender = (RadioGroup) loginDialog.findViewById(R.id.gender);
                            String selectedGender;
                            mygender = gender.getCheckedRadioButtonId();
                            if (mygender == R.id.gender_male)
                                selectedGender = "M";
                            else
                                selectedGender = "F";
                            hashMap = new HashMap<String, String>();
                            hashMap.put("email_id", emailContent);
                            hashMap.put("phoneNo", number);
                            hashMap.put("gender", selectedGender);
                            Log.e("Check point reached", "did reached chedk point 1");
                            httpHelper1 = new HttpHelper1(Constants.REGISTER_USER_URL, hashMap, MainActivity.this,
                                    "Loading..."); //TODO send data to server
                        }
                    });
                    alertBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                }
            }
        });
        loginDialog.show();
    }

    public void askforPermission() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, STATUS_PERMISSION);
    }

    public boolean isPermissionGiven() {
        return ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 123: {
                if (!isPermissionGiven()) {
                    final Dialog newAlertDialog = new AlertDialog.Builder(this)
                            .setTitle("CAUTION")
                            .setCancelable(false)
                            .setMessage("This app requires CONTACT,STORAGE & PHONE permission in order to work please enable it to use the functionality")
                            .setNegativeButton("OK, Enable it", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
//                                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{
//                                            Manifest.permission.READ_PHONE_STATE,
//                                            Manifest.permission.READ_CONTACTS,
//                                            Manifest.permission.WRITE_CONTACTS,
//                                            Manifest.permission.READ_EXTERNAL_STORAGE,
//                                            Manifest.permission.WRITE_EXTERNAL_STORAGE
//                                    }, STATUS_PERMISSION);
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else {
//                    renderLoginDialog();
                }
//
            }
            return;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    public boolean isValidEmailid(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void animation_click(View view, final Intent intent) {
        int enter_activity_animation = 0, exit_activity_animation = 0;
        if (SDK >= Build.VERSION_CODES.LOLLIPOP) {
            switch (view.getId()) {
                case R.id.img_take_backup_button:
//                    Toast.makeText(this, "You Clicked take backup", Toast.LENGTH_LONG).show();
//                    Log.e("Clicked", "take backup button");
                    take_backup.setEnabled(false);
                    scan_for_duplicate.setEnabled(false);
                    delete_multiple.setEnabled(false);
                    backup_history.setEnabled(false);
                    second.addTarget(R.id.img_scan_for_duplicate_button).addTarget(R.id.img_scan_for_duplicate_icon).addTarget(R.id.img_scan_for_duplicate_text);
                    second.setStartDelay(100);
                    third.addTarget(R.id.img_delete_multiple_button).addTarget(R.id.img_delete_multiple_icon).addTarget(R.id.img_delete_multiple_text);
                    third.setStartDelay(150);
                    fourth.addTarget(R.id.img_backup_history_button).addTarget(R.id.img_backup_history_icon).addTarget(R.id.img_backup_history_text);
                    fourth.setStartDelay(200);
                    enter_activity_animation = R.anim.slide_right_to_left_activity_enter;
                    exit_activity_animation = R.anim.slide_right_to_left_activity_exit;
                    applyConstraintSet.connect(R.id.img_backup_history_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                    applyConstraintSet.connect(R.id.img_delete_multiple_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                    applyConstraintSet.connect(R.id.img_scan_for_duplicate_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                    break;
                case R.id.img_scan_for_duplicate_button:
//                    Toast.makeText(this, "Scan for duplicate", Toast.LENGTH_LONG).show();
//                    Log.e("Clicked", "scan for duplicate");
                    take_backup.setEnabled(false);
                    scan_for_duplicate.setEnabled(false);
                    delete_multiple.setEnabled(false);
                    backup_history.setEnabled(false);
                    first.addTarget(R.id.img_take_backup_button).addTarget(R.id.img_take_backup_icon).addTarget(R.id.img_take_backup_text);
                    first.setStartDelay(150);
                    third.addTarget(R.id.img_delete_multiple_button).addTarget(R.id.img_delete_multiple_icon).addTarget(R.id.img_delete_multiple_text);
                    third.setStartDelay(200);
                    fourth.addTarget(R.id.img_backup_history_button).addTarget(R.id.img_backup_history_icon).addTarget(R.id.img_backup_history_text);
                    fourth.setStartDelay(100);
                    enter_activity_animation = R.anim.slide_left_to_right_activity_enter;
                    exit_activity_animation = R.anim.slide_left_to_right_activity_exit;
                    applyConstraintSet.connect(R.id.img_take_backup_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                    applyConstraintSet.connect(R.id.img_delete_multiple_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                    applyConstraintSet.connect(R.id.img_backup_history_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                    break;
                case R.id.img_delete_multiple_button:
//                    Toast.makeText(this, "Delete Multiple", Toast.LENGTH_LONG).show();
//                    Log.e("Clicked", "delete multiple");
                    take_backup.setEnabled(false);
                    scan_for_duplicate.setEnabled(false);
                    delete_multiple.setEnabled(false);
                    backup_history.setEnabled(false);
                    first.addTarget(R.id.img_take_backup_button).addTarget(R.id.img_take_backup_icon).addTarget(R.id.img_take_backup_text);
                    first.setStartDelay(100);
                    second.addTarget(R.id.img_scan_for_duplicate_button).addTarget(R.id.img_scan_for_duplicate_icon).addTarget(R.id.img_scan_for_duplicate_text);
                    second.setStartDelay(150);
                    fourth.addTarget(R.id.img_backup_history_button).addTarget(R.id.img_backup_history_icon).addTarget(R.id.img_backup_history_text);
                    fourth.setStartDelay(200);
                    enter_activity_animation = R.anim.slide_right_to_left_activity_enter;
                    exit_activity_animation = R.anim.slide_right_to_left_activity_exit;
                    applyConstraintSet.connect(R.id.img_take_backup_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                    applyConstraintSet.connect(R.id.img_scan_for_duplicate_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                    applyConstraintSet.connect(R.id.img_backup_history_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                    break;
                case R.id.img_backup_history_button:
//                    Toast.makeText(this, "backup history", Toast.LENGTH_LONG).show();
//                    Log.e("Clicked", "backup History");
                    take_backup.setEnabled(false);
                    scan_for_duplicate.setEnabled(false);
                    delete_multiple.setEnabled(false);
                    backup_history.setEnabled(false);
                    first.addTarget(R.id.img_take_backup_button).addTarget(R.id.img_take_backup_icon).addTarget(R.id.img_take_backup_text);
                    first.setStartDelay(100);
                    second.addTarget(R.id.img_scan_for_duplicate_button).addTarget(R.id.img_scan_for_duplicate_icon).addTarget(R.id.img_scan_for_duplicate_text);
                    second.setStartDelay(150);
                    third.addTarget(R.id.img_delete_multiple_button).addTarget(R.id.img_delete_multiple_icon).addTarget(R.id.img_delete_multiple_text);
                    third.setStartDelay(200);
                    enter_activity_animation = R.anim.slide_left_to_right_activity_enter;
                    exit_activity_animation = R.anim.slide_left_to_right_activity_exit;
                    applyConstraintSet.connect(R.id.img_take_backup_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                    applyConstraintSet.connect(R.id.img_scan_for_duplicate_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                    applyConstraintSet.connect(R.id.img_delete_multiple_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                    break;
            }
            transitionSet.setDuration(700);
            applyConstraintSet.applyTo(constraintLayout);
            TransitionManager.beginDelayedTransition(constraintLayout, transitionSet);  //TODO TransitionManager is available on api level 14

            final int clicked_id = view.getId();
            final int finalEnter_activity_animation = enter_activity_animation;
            final int finalExit_activity_animation = exit_activity_animation;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    ActivityOptionsCompat options = ActivityOptionsCompat.
//                            makeSceneTransitionAnimation(MainActivity.this, findViewById(clicked_id), "profile");
//                    startActivity(intent, options.toBundle());
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in_left_to_right, R.anim.slide_in_left_to_left);

//                    ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(MainActivity.this, finalEnter_activity_animation, finalExit_activity_animation);
//                    startActivity(intent, options.toBundle());

                    if (Build.VERSION.SDK_INT >= 21) {
                        try {
                            ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(MainActivity.this,
                                    finalEnter_activity_animation, finalExit_activity_animation);
                            startActivity(intent, options.toBundle());
                        } catch (Exception e) {
                            startActivity(intent);
                        }

                    } else {
                        startActivity(intent);
                    }

                }
            }, 900);
        } else {
            Animation take_backup_anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.out_from_right);
            Animation scan_for_duplicate_anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.out_from_left);
            Animation delete_multiple_anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.out_from_right);
            Animation backup_history_anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.out_from_left);
//                scan_for_duplicate_anim.setDuration(100);
//                delete_multiple_anim.setDuration(150);
//                backup_history_anim.setDuration(200);

            switch (view.getId()) {
                case R.id.img_take_backup_button:
                    take_backup.setEnabled(false);
                    scan_for_duplicate.setEnabled(false);
                    delete_multiple.setEnabled(false);
                    backup_history.setEnabled(false);
                    applyConstraintSet.clone(constraintLayout);

                    enter_activity_animation = R.anim.slide_right_to_left_activity_enter;
                    exit_activity_animation = R.anim.slide_right_to_left_activity_exit;

                    scan_for_duplicate.startAnimation(scan_for_duplicate_anim);
                    scan_for_duplicate_icon.startAnimation(scan_for_duplicate_anim);
                    scan_for_duplicate_text.startAnimation(scan_for_duplicate_anim);

                    delete_multiple.startAnimation(delete_multiple_anim);
                    delete_multiple_text.startAnimation(delete_multiple_anim);
                    delete_multiple_icon.startAnimation(delete_multiple_anim);

                    backup_history.startAnimation(backup_history_anim);
                    backup_history_icon.startAnimation(backup_history_anim);
                    backup_history_text.startAnimation(backup_history_anim);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            applyConstraintSet.connect(R.id.img_backup_history_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                            applyConstraintSet.connect(R.id.img_delete_multiple_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                            applyConstraintSet.connect(R.id.img_scan_for_duplicate_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                            applyConstraintSet.applyTo(constraintLayout);
                        }
                    }, 500);
                    break;

                case R.id.img_scan_for_duplicate_button:
                    take_backup.setEnabled(false);
                    scan_for_duplicate.setEnabled(false);
                    delete_multiple.setEnabled(false);
                    backup_history.setEnabled(false);

                    enter_activity_animation = R.anim.slide_left_to_right_activity_enter;
                    exit_activity_animation = R.anim.slide_left_to_right_activity_exit;

                    take_backup.startAnimation(take_backup_anim);
                    take_backup_icon.startAnimation(take_backup_anim);
                    take_backup_text.startAnimation(take_backup_anim);

                    delete_multiple.startAnimation(delete_multiple_anim);
                    delete_multiple_text.startAnimation(delete_multiple_anim);
                    delete_multiple_icon.startAnimation(delete_multiple_anim);

                    backup_history.startAnimation(backup_history_anim);
                    backup_history_icon.startAnimation(backup_history_anim);
                    backup_history_text.startAnimation(backup_history_anim);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            applyConstraintSet.connect(R.id.img_take_backup_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                            applyConstraintSet.connect(R.id.img_delete_multiple_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                            applyConstraintSet.connect(R.id.img_backup_history_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                            applyConstraintSet.applyTo(constraintLayout);
                        }
                    }, 500);
                    break;
                case R.id.img_delete_multiple_button:
                    take_backup.setEnabled(false);
                    scan_for_duplicate.setEnabled(false);
                    delete_multiple.setEnabled(false);
                    backup_history.setEnabled(false);

                    take_backup.startAnimation(take_backup_anim);
                    take_backup_icon.startAnimation(take_backup_anim);
                    take_backup_text.startAnimation(take_backup_anim);

                    scan_for_duplicate.startAnimation(scan_for_duplicate_anim);
                    scan_for_duplicate_icon.startAnimation(scan_for_duplicate_anim);
                    scan_for_duplicate_text.startAnimation(scan_for_duplicate_anim);

                    backup_history.startAnimation(backup_history_anim);
                    backup_history_icon.startAnimation(backup_history_anim);
                    backup_history_text.startAnimation(backup_history_anim);

                    enter_activity_animation = R.anim.slide_right_to_left_activity_enter;
                    exit_activity_animation = R.anim.slide_right_to_left_activity_exit;

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            applyConstraintSet.connect(R.id.img_take_backup_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                            applyConstraintSet.connect(R.id.img_scan_for_duplicate_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                            applyConstraintSet.connect(R.id.img_backup_history_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                            applyConstraintSet.applyTo(constraintLayout);
                        }
                    }, 500);

                    break;
                case R.id.img_backup_history_button:
                    take_backup.setEnabled(false);
                    scan_for_duplicate.setEnabled(false);
                    delete_multiple.setEnabled(false);
                    backup_history.setEnabled(false);
                    enter_activity_animation = R.anim.slide_left_to_right_activity_enter;
                    exit_activity_animation = R.anim.slide_left_to_right_activity_exit;
                    take_backup.startAnimation(take_backup_anim);
                    take_backup_icon.startAnimation(take_backup_anim);
                    take_backup_text.startAnimation(take_backup_anim);

                    scan_for_duplicate.startAnimation(scan_for_duplicate_anim);
                    scan_for_duplicate_icon.startAnimation(scan_for_duplicate_anim);
                    scan_for_duplicate_text.startAnimation(scan_for_duplicate_anim);

                    delete_multiple.startAnimation(delete_multiple_anim);
                    delete_multiple_text.startAnimation(delete_multiple_anim);
                    delete_multiple_icon.startAnimation(delete_multiple_anim);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            applyConstraintSet.connect(R.id.img_take_backup_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                            applyConstraintSet.connect(R.id.img_scan_for_duplicate_button, ConstraintSet.END, R.id.cont_home, ConstraintSet.START, 0);
                            applyConstraintSet.connect(R.id.img_delete_multiple_button, ConstraintSet.START, R.id.cont_home, ConstraintSet.END, 0);
                            applyConstraintSet.applyTo(constraintLayout);
                        }
                    }, 500);
                    break;
            }

            final int finalEnter_activity_animation1 = enter_activity_animation;
            final int finalExit_activity_animation1 = exit_activity_animation;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(intent);
                    overridePendingTransition(finalEnter_activity_animation1, finalExit_activity_animation1);
                }
            }, 550);
//            startActivity(intent);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void home_screen_load_animation() {
        take_backup.setEnabled(true);
        scan_for_duplicate.setEnabled(true);
        delete_multiple.setEnabled(true);
        backup_history.setEnabled(true);

        if (SDK >= Build.VERSION_CODES.LOLLIPOP) {
            applyConstraintSet.clone(constraintLayout); //TODO this statement will clone the current state in which the view currently the constraint layout is within
            transitionSet = new TransitionSet();

            first = new ChangeBounds(); //TODO require Kitkar
            second = new ChangeBounds();
            third = new ChangeBounds();
            fourth = new ChangeBounds();

            first.setInterpolator(new AnticipateOvershootInterpolator()); //TODO requires kitkat
            second.setInterpolator(new AnticipateOvershootInterpolator());
            third.setInterpolator(new AnticipateOvershootInterpolator());
            fourth.setInterpolator(new AnticipateOvershootInterpolator());

            //TODO addTarget req kitkat
            first.addTarget(R.id.img_take_backup_button).addTarget(R.id.img_take_backup_icon).addTarget(R.id.img_take_backup_text);
            first.setStartDelay(100);
            second.addTarget(R.id.img_scan_for_duplicate_button).addTarget(R.id.img_scan_for_duplicate_icon).addTarget(R.id.img_scan_for_duplicate_text);
            second.setStartDelay(200);
            third.addTarget(R.id.img_delete_multiple_button).addTarget(R.id.img_delete_multiple_icon).addTarget(R.id.img_delete_multiple_text);
            third.addTarget(R.id.img_delete_multiple_button).addTarget(R.id.img_delete_multiple_icon).addTarget(R.id.img_delete_multiple_text);
            third.setStartDelay(300);
            fourth.addTarget(R.id.img_backup_history_button).addTarget(R.id.img_backup_history_icon).addTarget(R.id.img_backup_history_text);
            fourth.setStartDelay(400);
            transitionSet.addTransition(first).addTransition(second).addTransition(third).addTransition(fourth);
            transitionSet.setInterpolator(new AccelerateInterpolator());
            transitionSet.setDuration(800);

            applyConstraintSet.connect(R.id.img_take_backup_button, ConstraintSet.START, R.id.guide_line_35, ConstraintSet.END, 0);
            applyConstraintSet.connect(R.id.img_scan_for_duplicate_button, ConstraintSet.END, R.id.guide_line_65, ConstraintSet.START, 0);
            applyConstraintSet.connect(R.id.img_delete_multiple_button, ConstraintSet.START, R.id.guide_line_35, ConstraintSet.END, 0);
            applyConstraintSet.connect(R.id.img_backup_history_button, ConstraintSet.END, R.id.guide_line_65, ConstraintSet.START, 0);
            TransitionManager.beginDelayedTransition(constraintLayout, transitionSet);
            applyConstraintSet.applyTo(constraintLayout);
        }

        if (SDK < Build.VERSION_CODES.LOLLIPOP) {
            boolean is_animation_done = animation_prefer.getBoolean("animation_complete", false);
            animation_prefer.edit().putBoolean("animation_complete", true).commit();
            applyConstraintSet.clone(constraintLayout);
            applyConstraintSet.connect(R.id.img_take_backup_button, ConstraintSet.START, R.id.guide_line_35, ConstraintSet.END, 0);
            applyConstraintSet.connect(R.id.img_scan_for_duplicate_button, ConstraintSet.END, R.id.guide_line_65, ConstraintSet.START, 0);
            applyConstraintSet.connect(R.id.img_delete_multiple_button, ConstraintSet.START, R.id.guide_line_35, ConstraintSet.END, 0);
            applyConstraintSet.connect(R.id.img_backup_history_button, ConstraintSet.END, R.id.guide_line_65, ConstraintSet.START, 0);
            applyConstraintSet.applyTo(constraintLayout);

            if (!is_animation_done) {
                Animation slide_left = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.in_from_left);
                Animation slide_right = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.in_from_right);

                take_backup.setAnimation(slide_right);
                take_backup_text.setAnimation(slide_right);
                take_backup_icon.setAnimation(slide_right);

                scan_for_duplicate.setAnimation(slide_left);
                scan_for_duplicate_text.setAnimation(slide_left);
                scan_for_duplicate_icon.setAnimation(slide_left);

                delete_multiple.setAnimation(slide_right);
                delete_multiple_icon.setAnimation(slide_right);
                delete_multiple_text.setAnimation(slide_right);

                backup_history.setAnimation(slide_left);
                backup_history_text.setAnimation(slide_left);
                backup_history_icon.setAnimation(slide_left);
            }
//            applyConstraintSet.applyTo(constraintLayout);
        }


    }

    private void OnClick_animation() {
        //TODO Pending 1: Need to handle the button click animation over here still incomplete will handle in future
        take_backup.setEnabled(false);
        scan_for_duplicate.setEnabled(false);
        delete_multiple.setEnabled(false);
        backup_history.setEnabled(false);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
//        prefs = getSharedPreferences("data", Context.MODE_PRIVATE);
//        boolean is_login = prefs.getBoolean("isflagLogin", false);    //TODO this is added by me
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void run() {
                home_screen_load_animation();
            }
        }, 500);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void alert_dialog_for_register_email(int from) {
        String information = null;
        if (from == 1) {//TODO 1 means change security pin from drawer layout

        } else {

        }

        switch (from) {
            case 1:
                information = "You need to register your new email or existing email with us in order to change or set Pin";
                break;
            case 2:
                information = "You need to register your new email or existing email with us in order to know your backed up history.";
                break;
            case 3:
                information = "You need to register your new email or existing email so that we can back up your your contact.";
                break;
        }
        AlertDialog.Builder alert_builder = new AlertDialog.Builder(this);
        String request = "Do you want to Register your email now ?";
        alert_builder.setMessage(information + "\n\n" + request);
        alert_builder.setTitle("Register Email");
        alert_builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                renderLoginDialog();
            }
        });

        alert_builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog alertDialog = alert_builder.create();
        alertDialog.show();
    }

    public void alert_after_login(String user_email, boolean is_password_set, boolean is_previous_backup) {

        String title = "Login Successful";
        String message = null;

        int user_name_index = user_email.indexOf('@');

        String user_name = user_email.substring(user_name_index, user_email.length() - 1);

        //
        if (is_previous_backup || is_password_set) {
            message = "Welcome back " + user_name;
        } else {
            message = "Welcome New User " + user_name;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message).setCancelable(false);
        final AlertDialog my_alertDialog = builder.create();
        my_alertDialog.show();
        new CountDownTimer(3000, 1000) {

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                if (my_alertDialog != null && my_alertDialog.isShowing())
                    my_alertDialog.dismiss();
            }
        };
    }

    private void letDoBusiness() {
        System.getProperty("os.version"); // OS version
        String sdk = android.os.Build.VERSION.SDK;     // API Level
        String DEVICE = android.os.Build.DEVICE;       // Device
        String MODEL = android.os.Build.MODEL;     // Model
        String PRODUCT = android.os.Build.PRODUCT;
        String RELEASE = android.os.Build.VERSION.RELEASE;
        Log.d("TAG_VERSIONS", "TAG_VERSIONS = sdk : " + sdk + " DEVICE : " + DEVICE +
                " MODEL : " + MODEL + " Product = " + PRODUCT + " RELEASE :" + RELEASE);
        commonIntent = new Intent(Intent.ACTION_VIEW);
        commonIntent.setType("text/plain");
        commonIntent.setData(Uri.parse("mailto:"));
        commonIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{("kush.thakkar@techathalon.com")});
        commonIntent.putExtra(Intent.EXTRA_SUBJECT, "We are specialized into Web Development and Mobile development (iOS & android).");
        commonIntent.putExtra(Intent.EXTRA_TEXT, " Sent From "+ MODEL + " : Android "+ RELEASE + "\n\n");
        commonIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(Intent.createChooser(commonIntent, "Lets do business..."));
    }

}
package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import techathalon.com.smartcontactmanager.R;

public class AppSharerAdapter extends BaseAdapter {
    private Context context;
    private final ArrayList<String> mobileValues;

    public AppSharerAdapter(Context context, ArrayList<String> mobileValues) {
        this.context = context;
        this.mobileValues = mobileValues;
    }

    @Override
    public int getCount() {
        return mobileValues.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;
        ImageView imageView;
        String mobile = mobileValues.get(position);
        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate(R.layout.share_layout, null);
            imageView = (ImageView) gridView.findViewById(R.id.imageView);
            int drawableResourceId = context.getResources().getIdentifier(mobile, "drawable", context.getPackageName());
            imageView.setImageResource(drawableResourceId);
        } else {
            gridView = convertView;
        }
        gridView.setTag(mobile);
        return gridView;
    }
}
package techathalon.com.smartcontactmanager.HELPER_FILES;

import android.content.Context;
import android.content.SharedPreferences;

public class ContactBackup {
    Context context;
    SharedPreferences pinPrefs;
    SharedPreferences.Editor toggleEdit;
    boolean is_email , is_url , is_address;

    public ContactBackup(Context context){
        this.context = context;
        pinPrefs = context.getSharedPreferences("toggle flag", Context.MODE_PRIVATE);
        toggleEdit = pinPrefs.edit();
    }

    public  boolean get_checked_value(String forWhat){
        switch (forWhat){
            case "email":
                if (pinPrefs.getString("emailCheckbox", "").equalsIgnoreCase("checked")){
                    return true;
                }
                else{
                    return false;
                }
            case "url":
                if (pinPrefs.getString("urlCheckbox", "").equalsIgnoreCase("checked")){
                    return true;
                }
                else{
                    return false;
                }
            case "address":
                if (pinPrefs.getString("addressCheckbox", "").equalsIgnoreCase("checked")){
                    return true;
                }
                else {
                    return false;
                }
             default:  //TODO never give a default value i.e the switch string should lie beteween the email or url or address
                 return false;
        }
    }

    public void set_checked_value(String forWhat){

        switch (forWhat){
            case "email":
                if (pinPrefs.getString("emailCheckbox", "").equalsIgnoreCase("checked")){
                    toggleEdit.putString("emailCheckbox", "unchecked");
                }
                else{
                    toggleEdit.putString("emailCheckbox", "checked");
                }
                toggleEdit.apply();
                break;
            case "url":
                if (pinPrefs.getString("urlCheckbox", "").equalsIgnoreCase("checked")){
                    toggleEdit.putString("urlCheckbox", "unchecked");
                }
                else{
                    toggleEdit.putString("urlCheckbox", "checked");
                }
                toggleEdit.apply();
                break;
            case "address":
                if (pinPrefs.getString("addressCheckbox", "").equalsIgnoreCase("checked")){
                    toggleEdit.putString("addressCheckbox", "unchecked");
                }
                else {
                    toggleEdit.putString("addressCheckbox", "checked");
                }
                toggleEdit.apply();
                break;
        }
    }
}

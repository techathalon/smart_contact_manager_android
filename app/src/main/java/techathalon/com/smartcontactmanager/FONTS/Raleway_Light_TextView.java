package techathalon.com.smartcontactmanager.FONTS;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Raleway_Light_TextView extends TextView {
    public Raleway_Light_TextView(Context context) {
        super(context);
        setType(context);
    }

    public Raleway_Light_TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setType(context);
    }

    public Raleway_Light_TextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setType(context);
    }

    private void setType(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Raleway-Light.ttf"));
    }
}
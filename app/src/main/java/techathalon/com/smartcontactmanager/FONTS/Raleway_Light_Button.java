package techathalon.com.smartcontactmanager.FONTS;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class Raleway_Light_Button extends Button {

    public Raleway_Light_Button(Context context) {
        super(context);
        setType(context);
    }

    public Raleway_Light_Button(Context context, AttributeSet attrs) {
        super(context, attrs);
        setType(context);
    }

    public Raleway_Light_Button(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setType(context);
    }

    private void setType(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Raleway-Light.ttf"));
    }
}
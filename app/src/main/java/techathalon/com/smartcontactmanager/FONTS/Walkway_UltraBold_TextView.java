package techathalon.com.smartcontactmanager.FONTS;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Walkway_UltraBold_TextView extends TextView {
    public Walkway_UltraBold_TextView(Context context) {
        super(context);
        setType(context);
    }

    public Walkway_UltraBold_TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setType(context);
    }

    public Walkway_UltraBold_TextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setType(context);
    }

    private void setType(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Walkway_UltraBold.ttf"));
    }
}
package techathalon.com.smartcontactmanager.app;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.Locale;

public class SCMApplication extends Application {
    private static SCMApplication mInstance;
    private Locale locale = null;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Trackers.initialize(this);
        Trackers.getInstance().get(Trackers.Target.APP);
//        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
//        String lang = settings.getString("language_preference", "");
//        changeLang(lang);
    }

    public static synchronized SCMApplication getInstance() {
        return mInstance;
    }
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        if (locale != null) {
//            Locale.setDefault(locale);
//            Configuration config = new Configuration(newConfig);
//            config.locale = locale;
//            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
//        }
//    }
    public synchronized Tracker getGoogleAnalyticsTracker() {
        Trackers trackers = Trackers.getInstance();
        return trackers.get(Trackers.Target.APP);
    }

    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();
        t.setScreenName(screenName);
        t.send(new HitBuilders.ScreenViewBuilder().build());
        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }
//    public void changeLang(String lang) {
//        Configuration config = getBaseContext().getResources().getConfiguration();
//        if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
//
//            SharedPreferences.Editor ed = PreferenceManager.getDefaultSharedPreferences(this).edit();
//            ed.putString("language_preference", lang);
//            ed.commit();
//
//            locale = new Locale(lang);
//            Locale.setDefault(locale);
//            Configuration conf = new Configuration(config);
//            conf.locale = locale;
//            getBaseContext().getResources().updateConfiguration(conf, getBaseContext().getResources().getDisplayMetrics());
//        }
//    }

//    public String getLang(){
//        return PreferenceManager.getDefaultSharedPreferences(this).getString("language_preference", "");
//    }
}
package techathalon.com.smartcontactmanager.app;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;
import java.util.Map;

import techathalon.com.smartcontactmanager.R;

public final class Trackers {

    public enum Target {
        APP,
    }

    private static Trackers sInstance;

    public static synchronized void initialize(Context context) {
        if (sInstance != null)
            throw new IllegalStateException("Extra call to initialize analytics trackers");
        sInstance = new Trackers(context);
    }

    public static synchronized Trackers getInstance() {
        if (sInstance == null)
            throw new IllegalStateException("Call initialize() before getInstance()");
        return sInstance;
    }

    private final Map<Target, Tracker> mTrackers = new HashMap<Target, Tracker>();
    private final Context mContext;

    private Trackers(Context context) {
        mContext = context.getApplicationContext();
    }

    public synchronized Tracker get(Target target) {
        if (!mTrackers.containsKey(target)) {
            Tracker tracker;
            switch (target) {
                case APP:
                    tracker = GoogleAnalytics.getInstance(mContext).newTracker(R.xml.app_tracker);
                    break;
                default:
                    throw new IllegalArgumentException("Unhandled analytics target " + target);
            }
            mTrackers.put(target, tracker);
        }
        return mTrackers.get(target);
    }
}
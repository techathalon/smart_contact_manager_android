package techathalon.com.smartcontactmanager.CREATE_BACKUP;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;


import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.HELPER_FILES.HelperActivity;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class DisplayContactActivity extends HelperActivity implements
        SearchView.OnQueryTextListener, View.OnClickListener, AdapterView.OnItemClickListener {
    private ListView lst_all_contacts;
    private SearchView searchView;
    private TextView txt_delete_contacts, txt_nocontacts;
    private ImageView img_selectall;
    private LinearLayout lin_selectall;
    DisplayContactAdapter adapter;
    private String alphabet = "#";
    List<DisplayContact> contactsdata = new ArrayList<DisplayContact>();
    private String newalphabet = "";

    private Boolean selectall = false, statusDelete = false;
    Dialog deleteContact;
    TextView message;
    private int deletecount = 0;
    private static final int PERMISSION_REQUEST_CONTACT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_contact);
        if (Constants.enable_tracker) {
            SCMApplication.getInstance().trackScreenView("Display Contact");
        }
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        lst_all_contacts = findViewById(R.id.lst_all_contacts);
        lst_all_contacts.setTextFilterEnabled(true);
        lst_all_contacts.setOnItemClickListener(this);
        searchView = findViewById(R.id.searchView);
        txt_nocontacts = findViewById(R.id.txt_nocontacts);
        txt_delete_contacts = findViewById(R.id.txt_delete_contacts);
        txt_delete_contacts.setOnClickListener(this);
        img_selectall = findViewById(R.id.img_selectall);
        lin_selectall = findViewById(R.id.lin_selectall);
        lin_selectall.setOnClickListener(this);
        adapter = new DisplayContactAdapter(DisplayContactActivity.this, contactsdata);
        askForContactPermission();//TODO For marshmallow again permission is asked before contacts is read
//        new readContact().execute();
        setupSearchView();
    }

    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {
                    requestPermissions(
                            new String[]
                                    {Manifest.permission.READ_CONTACTS}
                            , PERMISSION_REQUEST_CONTACT);

                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);
                }
            } else {
                new readContact().execute();
            }
        } else {
            new readContact().execute();
        }
    }

    private void setupSearchView() {
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setSubmitButtonEnabled(true);
        searchView.setQueryHint("Search Here");
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new readContact().execute();
                } else {
                    Toast.makeText(this, "No permission for contacts", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            adapter.getFilter().filter("");
        } else {
            adapter.getFilter().filter(newText);
        }
        adapter.notifyDataSetChanged();
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v == txt_delete_contacts) {
            if (contactsdata.size() > 0) {
                for (int i = contactsdata.size() - 1; i >= 0; i--) {
                    if (contactsdata.get(i).getTick() == 1) {
                        deletecount++;
                    }
                }
                if (deletecount > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(DisplayContactActivity.this);
                    builder.setTitle("Delete Contacts");
                    builder.setCancelable(false);
                    builder.setMessage("Are you sure to Delete");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new deleteSelectedContact().execute();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog dl = builder.create();
                    dl.show();
                } else {
                    Toast.makeText(DisplayContactActivity.this, "Select Contact", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (v == lin_selectall) {
            if (selectall) {
                for (int i = 0; i < contactsdata.size(); i++) {
                    contactsdata.get(i).setTick(0);
                }
                img_selectall.setImageResource(R.drawable.new_white_round_unselect_black);
                selectall = false;
            } else {
                for (int i = 0; i < contactsdata.size(); i++) {
                    contactsdata.get(i).setTick(1);
                }
                img_selectall.setImageResource(R.drawable.new_white_round_select_black);
                selectall = true;
            }
            adapter.updateData(contactsdata);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        searchView.setQuery("", false);
        searchView.clearFocus();
        DisplayContact displayContact = (DisplayContact) parent.getAdapter().getItem(position);
        for (int i = 0; i < contactsdata.size(); i++) {
            if (displayContact.getId().toLowerCase().equals(contactsdata.get(i).getId())) {
                if (contactsdata.get(i).getTick() == 0) {
                    contactsdata.get(i).setTick(1);
                } else if (contactsdata.get(i).getTick() == 1) {
                    contactsdata.get(i).setTick(0);
                }
                adapter.notifyDataSetChanged();
                final int finalI = i;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lst_all_contacts.smoothScrollToPosition(contactsdata.indexOf(contactsdata.get(finalI)));
                    }
                }, 100);

                break;
            }
        }
    }

    private class readContact extends AsyncTask<String, Long, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(DisplayContactActivity.this);
            dialog.setMessage("Loading..");
            dialog.setIndeterminate(false);
            dialog.setMax(100);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            int count = 0;
            Long per=0L;
            ContentResolver cr = getContentResolver();
//            final Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            final Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.LOOKUP_KEY}, null, null, null);

            if (cur != null && cur.getCount() > 0) {
                if (statusDelete) {
                    statusDelete = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(DisplayContactActivity.this, cur.getCount() + " Contact Not Deleted Please Try again.", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                while (cur != null && cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));

                    String firstName = "", lastName = "", note = "";
                    ArrayList<String> phonenumber = new ArrayList<String>();
                    ArrayList<String> phoneemail = new ArrayList<String>();

                    //get the first and last name
                    Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));
                    Uri dataUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Data.CONTENT_DIRECTORY);
                    Cursor nameCursor = cr.query(dataUri, null, ContactsContract.Data.MIMETYPE + "=?", new String[]{ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE}, null);
                    while (nameCursor != null && nameCursor.moveToNext()) {
                        firstName = nameCursor.getString(nameCursor.getColumnIndex(ContactsContract.Data.DATA2));
                        lastName = nameCursor.getString(nameCursor.getColumnIndex(ContactsContract.Data.DATA3));
                    }
                    nameCursor.close();

                    //get the phone number
                    Cursor pcur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    int i = -1;
                    while (pcur != null && pcur.moveToNext()) {
                        i++;
                        String phone = pcur.getString(pcur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phonenumber.add(i, phone);
                    }
                    pcur.close();

                    // get email and type
                    Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                    int j = -1;
                    while (emailCur.moveToNext()) {
                        // This would allow you get several email addresses
                        // if the email addresses were stored in an array
                        j++;
                        String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        String emailType = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                        phoneemail.add(j, email);
                    }
                    emailCur.close();

                    // Get Photo
                    Bitmap my_btmp = null;
                    Uri my_contact_Uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(id));
                    InputStream photo_stream = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(), my_contact_Uri);
                    if (photo_stream != null) {
                        BufferedInputStream buf = new BufferedInputStream(photo_stream);
                        my_btmp = BitmapFactory.decodeStream(buf);
                    }

                    // Get note.......
                    String noteWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                    String[] noteWhereParams = new String[]{id, ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE};
                    Cursor noteCur = cr.query(ContactsContract.Data.CONTENT_URI, null, noteWhere, noteWhereParams, null);
                    if (noteCur.moveToFirst()) {
                        note = noteCur.getString(noteCur.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
                    }
                    noteCur.close();

                    while (i < j) {
                        i++;
                        phonenumber.add(i, "0");
                    }
                    while (i > j) {
                        j++;
                        phoneemail.add(j, "0");
                    }
                    alphabet = "0";
                    if (name == null) {
                        name = "";
                        firstName = "";
                        lastName = "";
                    }
                    contactsdata.add(new DisplayContact(name, firstName, lastName, phonenumber, phoneemail, id, my_btmp, note, alphabet, 0, lookupKey));

//                }
                    count++;
                    per = Long.valueOf(count * 100) / Long.valueOf(cur.getCount());
                    publishProgress(per);

                }
                Collections.sort(contactsdata, new Comparator<DisplayContact>() {
                    @Override
                    public int compare(DisplayContact lhs, DisplayContact rhs) {
                        return lhs.getName().compareToIgnoreCase(rhs.getName());
                    }
                });

                for (int i = 0; i < contactsdata.size(); i++) {
                    if (contactsdata.get(i).getName().equalsIgnoreCase("")) {
                        alphabet = "#";
                    } else {
                        alphabet = contactsdata.get(i).getName().substring(0, 1);
                    }

                    if (!alphabet.matches("^[a-zA-Z]+$")) {
                        alphabet = "#";
                    }

                    if (newalphabet.equalsIgnoreCase(alphabet)) {
                        alphabet = "0";
                    }
                    if (!alphabet.equalsIgnoreCase("0")) {
                        newalphabet = alphabet;
                    }
                    contactsdata.get(i).setAlphabet(alphabet);
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            dialog.setMessage("Loading.." + values[0] + "%");
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            adapter = new DisplayContactAdapter(DisplayContactActivity.this, contactsdata);
            adapter.setTotalcount(contactsdata.size());
            if (contactsdata.size() > 0) {
                adapter.notifyDataSetChanged();
                lst_all_contacts.setAdapter(adapter);
            } else {
                lst_all_contacts.setVisibility(View.GONE);
                txt_nocontacts.setVisibility(View.VISIBLE);
            }

            try {
                if ((this.dialog != null) && this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                // Handle or log or ignore
            } catch (final Exception e) {
                // Handle or log or ignore
            } finally {
                this.dialog = null;
            }
//            if (dialog != null && dialog.isShowing() && DisplayContactActivity.this != null && !DisplayContactActivity.this.isFinishing()) {
//                dialog.dismiss();
//            }
        }
    }

    private class deleteSelectedContact extends AsyncTask<Void, String, Void> {

        ArrayList<DisplayContact> deleteStatus;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            deleteStatus = new ArrayList<>();
            if (deleteContact != null && deleteContact.isShowing()) {
                deleteContact.dismiss();
            }
            deleteContact = new Dialog(DisplayContactActivity.this);
            if (deleteContact.getWindow() != null)
                deleteContact.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            deleteContact.getWindow().setBackgroundDrawableResource(R.drawable.custom_loader_background);
            deleteContact.setCancelable(false);
            deleteContact.setCanceledOnTouchOutside(false);
            deleteContact.setContentView(R.layout.custom_loader);
            message = (TextView) deleteContact.findViewById(R.id.msg);
            message.setText("Delete Contact...");
            deleteContact.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            int count = 0;
            Long per;
            for (int i = contactsdata.size() - 1; i >= 0; i--) {
                if (contactsdata.get(i).getTick() == 1) {
                    count++;
                    per = Long.valueOf(count * 100) / Long.valueOf(deletecount);
                    publishProgress(String.valueOf(per));
                    Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, contactsdata.get(i).getLookupkey());
                    try {
                        if (getApplicationContext().getContentResolver().delete(uri, null, null) == 0)
                            deleteStatus.add(contactsdata.get(i));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            message.setText("Delete Contact..." + values[0] + "%");
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            for (int i = contactsdata.size() - 1; i >= 0; i--) {
                if (contactsdata.get(i).getTick() == 1) {
                    if (!contactsdata.get(i).getAlphabet().equalsIgnoreCase("0")) {
                        if (i < contactsdata.size() - 1 && contactsdata.get(i + 1).getAlphabet().equalsIgnoreCase("0")) {
                            contactsdata.get(i + 1).setAlphabet(contactsdata.get(i).getAlphabet());
                        }
                    }
                    contactsdata.remove(i);
                }
            }
            adapter.updateData(contactsdata);
            if (contactsdata.size() == 0) {
                lst_all_contacts.setVisibility(View.GONE);
                txt_nocontacts.setVisibility(View.VISIBLE);
            }

//            if (deleteContact != null && deleteContact.isShowing() && DisplayContactActivity.this != null && !DisplayContactActivity.this.isFinishing())
//                deleteContact.dismiss();
            try {
                if ((deleteContact != null) && deleteContact.isShowing()) {
                    deleteContact.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                // Handle or log or ignore
            } catch (final Exception e) {
                // Handle or log or ignore
            } finally {
                deleteContact = null;
            }
            if (selectall) {
                img_selectall.setImageResource(R.drawable.new_white_round_unselect_black);
                selectall = false;
            }
            if (deleteStatus.size() > 0) {
                statusDelete = true;
                contactsdata.clear();
                deletecount = 0;
                adapter.notifyDataSetChanged();
                lst_all_contacts.setVisibility(View.VISIBLE);
                txt_nocontacts.setVisibility(View.GONE);
                new readContact().execute();
            } else {
                Toast.makeText(DisplayContactActivity.this, "Contact Deleted Successfully.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(DisplayContactActivity.this, TakeBackupActivity.class);
//        startActivity(intent);
//        finish();
    }


}
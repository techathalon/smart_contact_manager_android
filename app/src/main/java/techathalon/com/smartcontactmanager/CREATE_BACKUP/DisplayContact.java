package techathalon.com.smartcontactmanager.CREATE_BACKUP;

import android.graphics.Bitmap;

import java.util.ArrayList;

public class DisplayContact {
    private ArrayList<String> contact, email;
    private String id, firstName, lastname, note, alphabet, lookupkey, name;
    private Bitmap my_btmp;
    private int tick;

    public DisplayContact(String name, String firstName, String lastname, ArrayList<String> contact,
                          ArrayList<String> email, String id, Bitmap my_btmp, String note, String alphabet,
                          int tick, String lookupkey) {
        this.name = name;
        this.firstName = firstName;
        this.lastname = lastname;
        this.contact = contact;
        this.email = email;
        this.id = id;
        this.my_btmp = my_btmp;
        this.note = note;
        this.alphabet = alphabet;
        this.tick = tick;
        this.lookupkey = lookupkey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getContact() {
        return contact;
    }

    public void setContact(ArrayList<String> contact) {
        this.contact = contact;
    }

    public ArrayList<String> getEmail() {
        return email;
    }

    public void setEmail(ArrayList<String> email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlphabet() {
        return alphabet;
    }

    public void setAlphabet(String alphabet) {
        this.alphabet = alphabet;
    }

    public int getTick() {
        return tick;
    }

    public void setTick(int tick) {
        this.tick = tick;
    }

    public String getLookupkey() {
        return lookupkey;
    }
}
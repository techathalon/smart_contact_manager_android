package techathalon.com.smartcontactmanager.CREATE_BACKUP;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ezvcard.Ezvcard;
import ezvcard.VCard;
import ezvcard.VCardVersion;
import ezvcard.property.Address;
import ezvcard.property.Email;
import ezvcard.property.Note;
import ezvcard.property.Url;
import techathalon.com.smartcontactmanager.BACKUP_HISTORY.AlarmData;
import techathalon.com.smartcontactmanager.BACKUP_HISTORY.AlarmHandler;
import techathalon.com.smartcontactmanager.BACKUP_HISTORY.BackupHistoryActivity;
import techathalon.com.smartcontactmanager.BuildConfig;
import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.HELPER_FILES.HelperActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.HttpHelper1;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class TakeBackupActivity extends HelperActivity implements View.OnClickListener {
    TextView percentage, takeBackup;
    ImageView img_all_contacts;
    LinearLayout delete_contact_view;
    private SharedPreferences prefs, pinPrefs, emailPrefs, userDataPrefs;
    String user_id, backup_id = "";
    private final static String WHATSSAP_APP_ID = "com.whatsapp";
    private static int total_num_contacts_for_mail;

    private int total = 0;
    static String folderPath;
    private String storage_path;
    SharedPreferences.Editor toggleEdit;

    TextView backupTotalContacts, backupLastBackup;
    private ProgressBar mProgress;

    CallbackManager callbackManager;
    ShareDialog shareDialog;
    TakeBackup takeBackupAsync;
    UploadTask uploadFiles;

    AlarmHandler alarmHandler;
    ArrayList<AlarmData> alarmDatas;

    HttpHelper1 httpHelper1;
    HashMap<String, String> hashMap;
    JSONObject jsonObject;
    Intent commonIntent;
    AdView adView;
    AdRequest adRequest;

    @Override
    public void setBackApiResponse1(String url, String object) {
        super.setBackApiResponse1(url, object);
        if (object != null && !object.equals("")) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(object);
                if (isNetworkAvailable()) {
                    if (jsonObject != null && jsonObject.length() > 0) {
                        commonIntent = new Intent(TakeBackupActivity.this, BackupHistoryActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("history", jsonObject.toString());
                        commonIntent.putExtras(bundle);
                        startActivity(commonIntent);
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.jsonObject = jsonObject;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isContactPermissionTaken()) {
            Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cursor != null) {
                total = cursor.getCount();
                cursor.close();
            }
        }
        if (total == 0) {
            if (backupTotalContacts != null)
                backupTotalContacts.setText("No Contacts");
        } else {
            if (backupTotalContacts != null)
                backupTotalContacts.setText(getString(R.string.total_contacts) + total);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_backup_constraint);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        if (Constants.enable_tracker) {
            SCMApplication.getInstance().trackScreenView("Create Backup");
        }
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        //TODO Here is the google ads enable the constant to true in Constant file in project

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

        adView = findViewById(R.id.adView_backup);
        adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        emailPrefs = getSharedPreferences("email data", Context.MODE_PRIVATE);
        prefs = getSharedPreferences("data", Context.MODE_PRIVATE);
        userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
        user_id = userDataPrefs.getString("user_id", "");
        pinPrefs = getSharedPreferences("toggle flag", Context.MODE_PRIVATE);
        percentage = findViewById(R.id.percentage);
        delete_contact_view = findViewById(R.id.delete_contact_view);
        takeBackup = findViewById(R.id.take_backup);
        img_all_contacts = findViewById(R.id.img_all_contacts);

        if (isContactPermissionTaken()) {
            new AsyncTask<Void, Void, Void>() {
                @SuppressLint("WrongThread")
                @Override
                protected Void doInBackground(Void... voids) {
                    Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                    if (cursor != null) {
                        total = cursor.getCount();
                        cursor.close();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (total == 0) {
                        backupTotalContacts.setText("No Contacts");
                    } else {
                        backupTotalContacts.setText(getString(R.string.total_contacts) + total);
                    }
                }
            }.execute();
        } else {
            Toast.makeText(TakeBackupActivity.this, "Please enable CONTACT,STORAGE & PHONE permission for " +
                    "this app in Setting in order to use this feature.", Toast.LENGTH_LONG).show();
        }

        backupTotalContacts = findViewById(R.id.backup_total_contacts);
        backupLastBackup = findViewById(R.id.backup_last_backup);

        if (!pinPrefs.getString("last backup", "").equalsIgnoreCase("")) {
            backupLastBackup.setText(getString(R.string.last_backup) + pinPrefs.getString("last backup", ""));
        } else {
            if (!prefs.getString("last_backup_date", "").equalsIgnoreCase("")) {
                backupLastBackup.setText(getString(R.string.last_backup) + prefs.getString("last_backup_date", ""));
            } else {
                backupLastBackup.setText("No backup taken yet");
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                delete_contact_view.setVisibility(View.GONE);
            }
        }, 1500);

        takeBackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isContactPermissionTaken()) {
                    if (total == 0) {
                        Toast.makeText(TakeBackupActivity.this, "No Contacts for Backup", Toast.LENGTH_SHORT).show();
                    } else {
                        if (emailPrefs.getInt("total backups", 0) >= 3) {
                            final Dialog dialog = new Dialog(TakeBackupActivity.this);
                            if (dialog.getWindow() != null)
                                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            dialog.getWindow().setBackgroundDrawableResource(R.drawable.app_sharer_back);
                            dialog.setContentView(R.layout.take_back_dialog);
                            TextView ok = dialog.findViewById(R.id.ok_backup);
                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (dialog != null && dialog.isShowing() && TakeBackupActivity.this != null && !TakeBackupActivity.this.isFinishing())
                                        dialog.dismiss();
                                    userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
                                    user_id = userDataPrefs.getString("user_id", "");
                                    if (!user_id.equalsIgnoreCase("")) {
                                        hashMap = new HashMap<>();
                                        hashMap.put("user_id", user_id);
                                        httpHelper1 = new HttpHelper1(Constants.PREVIOUS_BACKUP_URL, hashMap, TakeBackupActivity.this, "Loading...");
                                    }
                                }
                            });
                            dialog.show();
                        } else {
                            takeBackup.setEnabled(false);
                            takeBackupAsync = new TakeBackup();
                            takeBackupAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                } else {
                    Toast.makeText(TakeBackupActivity.this, "Please enable CONTACT,STORAGE & PHONE permission for " +
                            "this app in Setting in order to use this feature.", Toast.LENGTH_LONG).show();
                }
            }
        });

//        img_all_contacts.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (isContactPermissionTaken()) {
//                    Intent intent = new Intent(TakeBackupActivity.this, DisplayContactActivity.class);
//                    startActivity(intent);
//                    finish();
//                } else {
//                    Toast.makeText(TakeBackupActivity.this, "Please enable CONTACT,STORAGE & PHONE permission for " +
//                            "this app in Setting in order to use this feature.", Toast.LENGTH_LONG).show();
//                }
//            }
//        });
        //TODO - onCLickListener implemented ////////
        img_all_contacts.setOnClickListener(this);

        mProgress = findViewById(R.id.circle_progress_bar);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        clearAsyncTasks();
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        startActivity(intent);
        finish();
    }

    private void clearAsyncTasks() {
        if (takeBackupAsync != null) {
            takeBackupAsync.cancel(true);
            takeBackupAsync = null;
        }
        if (uploadFiles != null) {
            uploadFiles.cancel(true);
            uploadFiles = null;
        }
    }


    private class TakeBackup extends AsyncTask<String, String, Long> {
        private boolean contact_presents = true;

        @Override
        protected Long doInBackground(String... strings) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmm", Locale.US);
            String currentDateandTime = sdf.format(new Date());
            String vfile = "SCM" + user_id + "_" + currentDateandTime + ".vcf";
            int counterAtIndex = 0;
            ArrayList<String> vCard = new ArrayList<String>();

            Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                final int totalContact = cursor.getCount();
                total_num_contacts_for_mail = totalContact;
                int i;
                try {
                    String folder_path = getFolderPath(TakeBackupActivity.this);
                    File file = new File(folder_path);
                    boolean success = true;
                    if (!file.exists()) {
                        success = file.mkdir();
                    }
                    if (success) {
                        storage_path = folder_path + vfile;
                        FileOutputStream mFileOutputStream;
                        mFileOutputStream = new FileOutputStream(storage_path, false);

                        cursor.moveToFirst();
                        for (i = 0; i < cursor.getCount(); i++) {
                            long percentage = ((long) counterAtIndex * 100 / (long) totalContact);
                            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                            counterAtIndex++;

                            String lookupKey = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                            Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);
                            String vcardstring = "";
//                            try {
//                                AssetFileDescriptor fd;
//                                fd = TakeBackupActivity.this.getContentResolver().openAssetFileDescriptor(uri, "r");
//                                FileInputStream fis = fd.createInputStream();
//                                byte[] buf = new byte[(int) fd.getDeclaredLength()];
//                                fis.read(buf);
//                                vcardstring = new String(buf);
//                                fd.close();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
                            AssetFileDescriptor fd;
                            fd = TakeBackupActivity.this.getContentResolver().openAssetFileDescriptor(uri, "r");
                            FileInputStream fis = fd.createInputStream();
                            try {
                                byte[] buf = new byte[(int) fd.getDeclaredLength()];
                                fis.read(buf);
                                vcardstring = new String(buf);
                                fd.close();
                            } catch (Exception e) {
                                byte[] buf = readBytes(fis);
                                fis.read(buf);
                                vcardstring = new String(buf);
                                fd.close();
                            }

                            List<Email> email;
                            List<Address> address;
                            List<Note> note;
                            List<Url> url;

                            VCard vCard1 = Ezvcard.parse(vcardstring).first();
                            if (pinPrefs.getString("emailCheckbox", "").equalsIgnoreCase("checked") && pinPrefs.getString("addressCheckbox", "").equalsIgnoreCase("checked")
                                    && pinPrefs.getString("noteCheckbox", "").equalsIgnoreCase("checked") && pinPrefs.getString("urlCheckbox", "").equalsIgnoreCase("checked")) {
                                vCard.add(vcardstring);
                            } else {
                                if (pinPrefs.getString("emailCheckbox", "").equalsIgnoreCase("unchecked")) {
                                    email = vCard1.getEmails();
                                    email.clear();
                                    vCard1.removeProperties(Email.class);
                                }
                                if (pinPrefs.getString("addressCheckbox", "").equals("unchecked")) {
                                    address = vCard1.getAddresses();
                                    address.clear();
                                    vCard1.removeProperties(Address.class);
                                }
                                if (pinPrefs.getString("noteCheckbox", "").equalsIgnoreCase("unchecked")) {
                                    note = vCard1.getNotes();
                                    note.clear();
                                    vCard1.removeProperties(Note.class);
                                }
                                if (pinPrefs.getString("urlCheckbox", "").equalsIgnoreCase("unchecked")) {
                                    url = vCard1.getUrls();
                                    url.clear();
                                    vCard1.removeProperties(Url.class);
                                }
                                String unselectedData = Ezvcard.write(vCard1).version(VCardVersion.V3_0).go();
                                vCard.add(unselectedData);
                            }
                            final int temp = i;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    float percentage = (float) ((temp + 1) * (100.0 / totalContact));
                                    int per = (int) Math.ceil(percentage);
                                    publishProgress(per + "");
                                }
                            });
                            cursor.moveToNext();
                            mFileOutputStream.write(vCard.get(i).getBytes());
                        }
                        mFileOutputStream.close();
                        cursor.close();
                    }
                    contact_presents = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                contact_presents = false;
            }
            return null;
        }

        public String getFolderPath(Context context) {
            if (isSdPresent()) {
                try {
                    File sdPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                            + "/SCMContactBackup");
                    if (!sdPath.exists()) {
                        sdPath.mkdirs();
                        folderPath = sdPath.getAbsolutePath();
                    } else if (sdPath.exists()) {
                        folderPath = sdPath.getAbsolutePath();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                folderPath = Environment.getExternalStorageDirectory().getPath() + "/SCMContactBackup/";
            } else {
                try {
                    File cacheDir = new File(context.getCacheDir(), "SCMContactBackup/");
                    if (!cacheDir.exists()) {
                        cacheDir.mkdirs();
                        folderPath = cacheDir.getAbsolutePath();
                    } else if (cacheDir.exists()) {
                        folderPath = cacheDir.getAbsolutePath();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return folderPath;
        }

        public boolean isSdPresent() {
            return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            percentage.setText(values[0] + " %");
            mProgress.setProgress(Integer.parseInt(values[0].toString()));
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            takeBackup.setEnabled(true);
            takeBackup.setText("Email it");
            takeBackup.setTextColor(Color.parseColor("#ffff4444"));
            takeBackup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showShareDialog();
                }
            });
        }
    }

    public void sendEmail() {
        final Dialog dialog = new Dialog(TakeBackupActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.contact_backup_dialog);
        dialog.setCancelable(false);

        final EditText userInput = dialog.findViewById(R.id.edt_email_id);
        userInput.setText(prefs.getString("email-id", ""));
        userInput.setEnabled(false);

        Button btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        final Button btn_send = dialog.findViewById(R.id.btn_send);
        if (userInput.getText().toString().equalsIgnoreCase("")) {
            btn_send.setEnabled(false);
        }

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search_label = userInput.getText().toString();
                if (search_label.length() > 0
                        && !search_label.equalsIgnoreCase(" ")) {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    send_email_with_attachement(search_label, storage_path); //TODO send email to ourself
                    uploadFiles = new UploadTask(storage_path, userInput.getText().toString(), TakeBackupActivity.this);
                    uploadFiles.execute();
                } else {
                    Toast.makeText(TakeBackupActivity.this, "Email id required", Toast.LENGTH_SHORT).show();
                }
            }
        });

        userInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (userInput.getText().toString().length() > 0 && userInput.getText().toString().contains("@") && userInput.getText().toString().contains(".")) {
                    btn_send.setEnabled(true);
                } else {
                    btn_send.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        dialog.show();
    }

    private class UploadTask extends AsyncTask<Void, JSONObject, JSONObject> {
        Dialog dialog;
        String imagePath, email;
        TakeBackupActivity activity;
        ProgressDialog uploadDialog;

        public UploadTask(String imagePath, String email, TakeBackupActivity activity) {
            this.imagePath = imagePath;
            this.activity = activity;
            this.email = email;
            uploadDialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(TakeBackupActivity.this);
            if (dialog.getWindow() != null)
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            dialog.setContentView(R.layout.fragment_http_helper);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.round_corner_layout_white);
            dialog.show();
            if (!activity.isFinishing()) { //here activity means your activity class
                dialog.show();
            }
        }

        protected JSONObject doInBackground(Void... bitmaps) {
            JSONObject object = null;
            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                File file = new File(imagePath);
                byte[] data = bos.toByteArray();
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost post = new HttpPost(Constants.BASEURL + "api_home/do_contact_backup_upload_oc_no_email/2/android");
                FileBody fileBody = new FileBody(file);
                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                builder.addPart("userfile", fileBody);
                builder.addPart("user_id", new StringBody(user_id));
                builder.addPart("email", new StringBody(email));
                post.setEntity(builder.build());
                HttpResponse response = httpClient.execute(post);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                String sResponse;
                StringBuilder s = new StringBuilder();
                while ((sResponse = reader.readLine()) != null) {
                    s = s.append(sResponse);
                }
                object = new JSONObject(s.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return object;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (result != null) {
                try {
                    if (result.getString("success").equalsIgnoreCase("true")) {
                        Toast.makeText(getApplicationContext(), "Backup successful", Toast.LENGTH_LONG).show();
                        backup_id = result.optString("cb_id");
                        alarmHandler = new AlarmHandler(getApplicationContext());
                        AlarmData alarmData;
                        alarmDatas = new ArrayList<>();
                        alarmData = new AlarmData();
                        alarmData.setBackup_id(backup_id);
                        alarmData.setCount("1");
                        alarmData.setAlarm_fire_time("");
                        alarmDatas.add(alarmData);
                        alarmHandler.syncDatabase(alarmDatas);

                        Date date = new Date();
                        String currentBackup = DateFormat.getDateTimeInstance().format(date);
                        toggleEdit = pinPrefs.edit();
                        toggleEdit.putString("last backup", currentBackup);
                        toggleEdit.apply();
//                        Intent intent = new Intent(TakeBackupActivity.this, MainActivity.class);
//                        startActivity(intent);
//                        finish();
                        onBackPressed();
                    } else if (result.getString("success").equalsIgnoreCase("false")) {
                        Toast.makeText(getApplicationContext(), "Backup unsuccessful", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else
                Toast.makeText(getApplicationContext(), "Some error occurred", Toast.LENGTH_LONG).show();
        }
    }

    public void showShareDialog() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.common_share_dialog);
        dialog.setCancelable(false);
        Button btn_share_fb = dialog.findViewById(R.id.btn_share_fb);
        Button btn_close = dialog.findViewById(R.id.close_alert);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                sendEmail();
            }
        });

        btn_share_fb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.e("facebook reach", "reached here finally");
                if (dialog.isShowing()) {
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentTitle(getString(R.string.app_name_with_space))
                                .setContentDescription("Good News...!!! Smart Contact Manager.\nIt's FREE for few days.\n" + "Download " + Constants.smartLink)
                                .setContentUrl(Uri.parse(Constants.smartLink))
                                .build();
                        shareDialog.show(linkContent);
                    }
                    shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            if (dialog != null && dialog.isShowing())
                                dialog.dismiss();
                            Log.e("facebook success", "got suceess");
                            sendEmail();
                        }

                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onError(FacebookException e) {
                            Log.e("facebook error", "Check out the error", e);
                        }
                    });

                }
            }
        });
        final Button btn_whats_app_share = dialog.findViewById(R.id.btn_share_whatsapp);
        btn_whats_app_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing() &&
                        TakeBackupActivity.this != null && !TakeBackupActivity.this.isFinishing()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sendEmail();
                        }
                    }, 2000);
                    dialog.dismiss();
                }
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.setPackage(WHATSSAP_APP_ID);
                intent.putExtra(Intent.EXTRA_TEXT, "Good News...!!! Smart Contact Manager.\nIt's FREE for few days.\n" + "Download " + Constants.smartLink1);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=" +
                                    WHATSSAP_APP_ID)));
                } catch (Exception e) {
                    Toast.makeText(TakeBackupActivity.this, "Your phone does not support this feature", Toast.LENGTH_LONG).show();
                }
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearAsyncTasks();
    }

    public byte[] readBytes(InputStream inputStream) throws IOException {
        //TODO thia function is a helper function in order to handle exception when vcf card is not created
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }

    public void send_email_with_attachement(String email, String path) {
        String to[] = {email};
        File my_vcf_file;
        Uri uri = null;
        try {
            my_vcf_file = new File(path);
            uri = FileProvider.getUriForFile(TakeBackupActivity.this, BuildConfig.APPLICATION_ID + ".provider", my_vcf_file);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Uri uri = Uri.fromFile(my_vcf_file);


        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("vnd.android.cursor.dir/email");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        if (uri != null)
            emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Smart Contact Manager contact backup");
        emailIntent.setPackage("com.google.android.gm");
        StringBuilder builder = new StringBuilder();
        builder.append("Here is your contact backup file attached to this email");
        builder.append("\n");
//        builder.append("Total number of contacts : " + total_num_contacts_for_mail +"");
//        builder.append("\n");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Here is your contact backup file attached to this email");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_all_contacts:
                if (isContactPermissionTaken()) {
                    Intent intent = new Intent(TakeBackupActivity.this, DisplayContactActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(TakeBackupActivity.this, "Please enable CONTACT,STORAGE & PHONE permission for " +
                            "this app in Setting in order to use this feature.", Toast.LENGTH_LONG).show();
                }
                break;

        }

    }


}
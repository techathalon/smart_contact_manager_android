package techathalon.com.smartcontactmanager.CREATE_BACKUP;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import techathalon.com.smartcontactmanager.R;

public class DisplayContactAdapter extends BaseAdapter implements Filterable {
    private List<DisplayContact> contactDatas;
    private List<DisplayContact> orig;
    private Context context;
    private int totalcount = 0;
    private String alphabet = "#";

    public DisplayContactAdapter(Context context, List<DisplayContact> contactsdata) {
        this.context = context;
        this.contactDatas = contactsdata;
    }

    public void setTotalcount(int totalcount) {
        this.totalcount = totalcount;
    }

    @Override
    public int getCount() {
        return contactDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return contactDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getViewTypeCount() {
        return totalcount;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public void updateData(List<DisplayContact> contactDatas) {
        this.contactDatas = contactDatas;
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;
        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.all_contact_show, parent, false);
            holder = new ViewHolder();
            holder.txt_contact_name = vi.findViewById(R.id.txt_contact_name);
            holder.txt_contact_alphabet = vi.findViewById(R.id.txt_contact_alphabet);
            holder.lin_contact_alphabet = vi.findViewById(R.id.lin_contact_alphabet);
            holder.img_select = vi.findViewById(R.id.img_select);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        final DisplayContact leaddatas = contactDatas.get(position);
        if (leaddatas.getName().equalsIgnoreCase("")) {
            holder.txt_contact_name.setText("No Name");
        } else {
            holder.txt_contact_name.setText(leaddatas.getName());
        }
        alphabet = leaddatas.getAlphabet().toUpperCase();
        if (alphabet.equalsIgnoreCase("0")) {
            holder.lin_contact_alphabet.setVisibility(View.GONE);
        } else {
            holder.lin_contact_alphabet.setVisibility(View.VISIBLE);
        }
        holder.txt_contact_alphabet.setText(alphabet);
        if (contactDatas.get(position).getTick() == 0) {
            holder.img_select.setImageResource(R.drawable.new_round_unselect_black);
        } else if (contactDatas.get(position).getTick() == 1) {
            holder.img_select.setImageResource(R.drawable.new_round_select_black);
        }
        return vi;
    }

    static class ViewHolder {
        private TextView txt_contact_name, txt_contact_alphabet;
        private LinearLayout lin_contact_alphabet;
        private ImageView img_select;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<DisplayContact> results = new ArrayList<DisplayContact>();
                if (orig == null) {
                    orig = contactDatas;
                }
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final DisplayContact g : orig) {
                            if (g.getName().toLowerCase().contains(constraint.toString())) {
                                results.add(g);
                            }
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
//                contactDatas.clear();
                contactDatas = (List<DisplayContact>) results.values;
                notifyDataSetChanged();
            }

        };
    }
}
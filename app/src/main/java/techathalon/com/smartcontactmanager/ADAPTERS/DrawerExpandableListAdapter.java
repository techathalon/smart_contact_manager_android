package techathalon.com.smartcontactmanager.ADAPTERS;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import techathalon.com.smartcontactmanager.HELPER_FILES.ContactBackup;
import techathalon.com.smartcontactmanager.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DrawerExpandableListAdapter extends BaseExpandableListAdapter {
    public static int SDK = Build.VERSION.SDK_INT;
    Context context;
    ArrayList<String> menu;
    HashMap<String, List<String>> submenu;
    //TODO Package Manager and User name related variable
    private static PackageInfo pInfo;
    private static PackageManager packageManager;
    private static String version_name;

    public int REMINDER_CHECK_LOCATION = 0;
    //TODO this will ensure where the current check reminder submenn we should take it from Shared Preference
    public int CONTACT_BACKUP_OPTION_LOCATION[];
    // TODO this also we need to check it from the user check and add the value into the array and store it in shared preference
    SharedPreferences prefs;
    ContactBackup contactBackup;


    public DrawerExpandableListAdapter(Context context, ArrayList<String> menu, HashMap<String, List<String>> submenu) throws PackageManager.NameNotFoundException {
        this.context = context;
        this.menu = menu;
        this.submenu = submenu;
        CONTACT_BACKUP_OPTION_LOCATION = new int[]{-1, -1, -1};
        packageManager = this.context.getPackageManager();
        pInfo = packageManager.getPackageInfo(this.context.getPackageName(), 0);
        version_name = pInfo.versionName;
        prefs = context.getSharedPreferences("data", 0);
        contactBackup = new ContactBackup(context);
    }

    @Override
    public int getGroupCount() {
        return this.menu.size();
    }

    @Override
    public int getChildrenCount(int menu_position) {
        if (this.submenu.get(this.menu.get(menu_position)) == null) {
            return 0;
        } else {
            return this.submenu.get(this.menu.get(menu_position)).size();
        }
    }

    @Override
    public Object getGroup(int menu_position) {
        return this.menu.get(menu_position);
    }

    @Override
    public Object getChild(int menu_position, int sub_menu_position) {
        return this.submenu.get(this.menu.get(menu_position))
                .get(sub_menu_position);
    }

    @Override
    public long getGroupId(int menu_position) {
        return menu_position;
    }

    @Override
    public long getChildId(int menu_position, int sub_menu_position) {
        return sub_menu_position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int menu_position, boolean is_expanded, View convert_view, ViewGroup viewGroup) {
        String menu_name = (String) getGroup(menu_position);
        ImageView source_image;
        ImageView drop_down;
        TextView source_menu_text;
        SwitchCompat message_merge;

        //TODO for menu_option 8 which is for foooter we have two text view
        TextView footer_email;
        TextView footer_smart_version;


        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convert_view = layoutInflater.inflate(R.layout.drawer_menu_structure, null);

//        if(menu_position == 1){
//            convert_view = layoutInflater.inflate(R.layout.show_message_for_merged_drawer, null);
//        }
        if (menu_position == 7) {
            convert_view = layoutInflater.inflate(R.layout.drawer_footer, null);
        }

        //TODO I want to Inflate the layout as per the value
        switch (menu_position) {  //TODO Change security Pin
            case 0:
                source_image = convert_view.findViewById(R.id.base_drawer_image);
                source_menu_text = convert_view.findViewById(R.id.base_drawer_text_view);
                drop_down = convert_view.findViewById(R.id.drop_down_icon);
                source_menu_text.setText(menu_name);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    source_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_dialpad));
                } else {
                    source_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_dialpad));
                }

                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    drop_down.setBackgroundDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
                } else {
                    drop_down.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent));
                }

                break;
            case 1: //TODO Contact Backup Options
                source_image = convert_view.findViewById(R.id.base_drawer_image);
                source_menu_text = convert_view.findViewById(R.id.base_drawer_text_view);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    source_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_backup));
                } else {
                    source_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_backup));
                }

                drop_down = convert_view.findViewById(R.id.drop_down_icon);
                if (is_expanded) {
                    if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                        drop_down.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_expand_less));
                    } else {
                        drop_down.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_expand_less));
                    }
                }


                source_menu_text.setText(menu_name);
//                    message_merge = convert_view.findViewById(R.id.on_off_message_merge);
//                    source_image = convert_view.findViewById(R.id.on_off_message_merge_image);
//                    if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                        source_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_merge_message));
//                    }
//                    else{
//                        source_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_merge_message));
//                    }
//                    message_merge.setText(menu_name);
                break;
            case 2: //TODO Setting
                source_image = convert_view.findViewById(R.id.base_drawer_image);
                source_menu_text = convert_view.findViewById(R.id.base_drawer_text_view);
                drop_down = convert_view.findViewById(R.id.drop_down_icon);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    source_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_setting));
                } else {
                    source_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_setting));
                }

                source_menu_text.setText(menu_name);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    drop_down.setBackgroundDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
                } else {
                    drop_down.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent));

                }
                break;
            case 3: //TODO Lets Do Business
                source_image = convert_view.findViewById(R.id.base_drawer_image);
                source_menu_text = convert_view.findViewById(R.id.base_drawer_text_view);
                drop_down = convert_view.findViewById(R.id.drop_down_icon);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    source_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_business));
                } else {
                    source_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_business));
                }

                source_menu_text.setText(menu_name);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    drop_down.setBackgroundDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
                } else {
                    drop_down.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent));
                }
                break;
            case 4://TODO Rate Us
                source_image = convert_view.findViewById(R.id.base_drawer_image);
                source_menu_text = convert_view.findViewById(R.id.base_drawer_text_view);
                drop_down = convert_view.findViewById(R.id.drop_down_icon);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    source_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_rate_us));
                } else {
                    source_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_rate_us));
                }
                source_menu_text.setText(menu_name);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    drop_down.setBackgroundDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
                } else {
                    drop_down.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent));
                }

//                source_menu_text.setText(menu_name);
                break;
            case 5: //TODO ABOUT US
                source_image = convert_view.findViewById(R.id.base_drawer_image);
                source_menu_text = convert_view.findViewById(R.id.base_drawer_text_view);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    source_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_about_us));
                } else {
                    source_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_about_us));
                }

                drop_down = convert_view.findViewById(R.id.drop_down_icon);
                if (is_expanded) {
                    if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                        drop_down.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_expand_less));
                    } else {
                        drop_down.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_expand_less));
                    }
                }
                source_menu_text.setText(menu_name);
                break;
            case 6: //TODO More apps
                source_image = convert_view.findViewById(R.id.base_drawer_image);
                source_menu_text = convert_view.findViewById(R.id.base_drawer_text_view);
                drop_down = convert_view.findViewById(R.id.drop_down_icon);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    source_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_more_apps));
                } else {
                    source_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_more_apps));
                }

                source_menu_text.setText(menu_name);
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    drop_down.setBackgroundDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
                } else {
                    drop_down.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent));
                }

                break;
//                case 7:
//                    source_image = convert_view.findViewById(R.id.base_drawer_image);
//                    source_menu_text = convert_view.findViewById(R.id.base_drawer_text_view);
//                    if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                        source_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_logout));
//                    }
//                    else{
//                        source_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_logout));
//                    }
//                    source_menu_text.setText(menu_name);
//                    drop_down = convert_view.findViewById(R.id.drop_down_icon);
//                    if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                        drop_down.setBackgroundDrawable(ContextCompat.getDrawable(context,android.R.color.transparent));
//                    }
//                    else{
//                        drop_down.setBackground(ContextCompat.getDrawable(context , android.R.color.transparent));
//                    }
//                    break;
            case 7:
                convert_view.setEnabled(false);
                convert_view.setOnClickListener(null);
                footer_email = convert_view.findViewById(R.id.email_footer);
                footer_smart_version = convert_view.findViewById(R.id.smart_contact_ver_footer);
                String user_email = prefs.getString("email-id", "");
                Log.e("Your_email_id", user_email);
                footer_email.setText(user_email);
                String version_text = this.context.getString(R.string.app_name_with_space) + " v" + version_name;
                footer_smart_version.setText(version_text);
                break;
        }

        return convert_view;
    }

    @Override
    public View getChildView(int menu_position, int submenu_position, boolean is_last_child, View convert_view, ViewGroup viewGroup) {
        String submenu_name = (String) getChild(menu_position, submenu_position);
        ImageView source_sub_menu_image;
        ImageView source_check_image;
        TextView source_sub_menu_text;

        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (menu_position) {
//                case 0:
////                    convert_view = layoutInflater.inflate(R.layout.drawer_submenu_structure , null);
////                    source_sub_menu_text = convert_view.findViewById(R.id.base_drawer_submenu_text_view);
////                    source_sub_menu_image = convert_view.findViewById(R.id.base_drawer_submenu_image);
////                    source_check_image = convert_view.findViewById(R.id.checkable_image);
////
////                    switch (submenu_position){
////                        case 0:
////                            if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
////                                source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_profile));
////                            }
////                            else{
////                                source_sub_menu_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_profile));
////                            }
////                            break;
////                        case 1:
////                            if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
////                                source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_merge_message));
////                            }
////                            else{
////                                source_sub_menu_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_merge_message));
////                            }
////                            break;
////                        case 2:
////                            if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
////                                source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_backup));
////                            }
////                            else{
////                                source_sub_menu_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_backup));
////                            }
////                            break;
////                        case 3:
////                            if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
////                                source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_reset));
////                            }
////                            else{
////                                source_sub_menu_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_reset));
////                            }
////                            break;
////                    }
////                    source_sub_menu_text.setText(submenu_name);
////                    break;

            case 1:
                convert_view = layoutInflater.inflate(R.layout.drawer_submenu_structure, null);
                source_sub_menu_text = convert_view.findViewById(R.id.base_drawer_submenu_text_view);
                source_sub_menu_image = convert_view.findViewById(R.id.base_drawer_submenu_image);
                source_check_image = convert_view.findViewById(R.id.checkable_image);
                switch (submenu_position) {
                    case 0:
                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                            source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_mail));
                        } else {
                            source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_mail));
                        }

                        if (contactBackup.get_checked_value("email")) {
                            if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                                source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check));
                            } else {
                                source_check_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_check));
                            }

                        } else {
                            if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                                source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
                            } else {
                                source_check_image.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent));
                            }
                        }

                        break;
                    case 1:
                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                            source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_url));
                        } else {
                            source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_url));
                        }

                        if (contactBackup.get_checked_value("url")) {
                            if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                                source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check));
                            } else {
                                source_check_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_check));
                            }

                        } else {
                            if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                                source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
                            } else {
                                source_check_image.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent));
                            }
                        }
                        break;
                    case 2:
                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                            source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_address));
                        } else {
                            source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_address));
                        }

                        if (contactBackup.get_checked_value("address")) {
                            if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                                source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check));
                            } else {
                                source_check_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_check));
                            }

                        } else {
                            if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                                source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
                            } else {
                                source_check_image.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent));
                            }
                        }
                        break;
                }
                source_sub_menu_text.setText(submenu_name);
                break;


            case 2:
                convert_view = layoutInflater.inflate(R.layout.drawer_submenu_structure, null);
                source_sub_menu_text = convert_view.findViewById(R.id.base_drawer_submenu_text_view);
                source_sub_menu_image = convert_view.findViewById(R.id.base_drawer_submenu_image);
                source_check_image = convert_view.findViewById(R.id.checkable_image);
                if (submenu_position == REMINDER_CHECK_LOCATION) { //TODO this will check the current position and enaable the image

                    if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                        source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check));
                    } else {
                        source_check_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_check));
                    }
                }
                if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                    source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_days));
                } else {
                    source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_days));
                }
                source_sub_menu_text.setText(submenu_name);
                break;
//                case 3:
//                    convert_view = layoutInflater.inflate(R.layout.drawer_submenu_structure, null);
//                    source_sub_menu_text = convert_view.findViewById(R.id.base_drawer_submenu_text_view);
//                    source_sub_menu_image = convert_view.findViewById(R.id.base_drawer_submenu_image);
//                    source_check_image = convert_view.findViewById(R.id.checkable_image);
//                    switch (submenu_position){
//                        case 0:
//                            if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                                source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_mail));
//                            }
//                            else{
//                                source_sub_menu_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_mail));
//                            }
//                            if(CONTACT_BACKUP_OPTION_LOCATION[0] == -1){
//                                CONTACT_BACKUP_OPTION_LOCATION[0] = 1;
//                                if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                                    source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_check));
//                                }
//                                else{
//                                    source_check_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_check));
//                                }
//                            }
//                            else{
//                                CONTACT_BACKUP_OPTION_LOCATION[0] = -1;
//                                if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                                    source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context,android.R.color.transparent));
//                                }
//                                else{
//                                    source_check_image.setBackground(ContextCompat.getDrawable(context , android.R.color.transparent));
//                                }
//
//                            }
//                            break;
//                        case 1:
//                            if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                                source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_url));
//                            }
//                            else{
//                                source_sub_menu_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_url));
//                            }
//
//                            if(CONTACT_BACKUP_OPTION_LOCATION[1] == -1){
//                                CONTACT_BACKUP_OPTION_LOCATION[1] = 1;
//                                if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                                    source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_check));
//                                }
//                                else{
//                                    source_check_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_check));
//                                }
//
//                            }
//                            else{
//                                CONTACT_BACKUP_OPTION_LOCATION[1] = -1;
//                                if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                                    source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context,android.R.color.transparent));
//                                }
//                                else{
//                                    source_check_image.setBackground(ContextCompat.getDrawable(context , android.R.color.transparent));
//                                }
//                            }
//                            break;
//                        case 2:
//                            if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                                source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_address));
//                            }
//                            else{
//                                source_sub_menu_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_address));
//                            }
//
//                            if(CONTACT_BACKUP_OPTION_LOCATION[2] == -1){
//                                CONTACT_BACKUP_OPTION_LOCATION[2] = 1;
//                                if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                                    source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.ic_check));
//                                }
//                                else{
//                                    source_check_image.setBackground(ContextCompat.getDrawable(context , R.drawable.ic_check));
//                                }
//                            }
//                            else{
//                                CONTACT_BACKUP_OPTION_LOCATION[2] = -1;
//                                if(SDK <= Build.VERSION_CODES.JELLY_BEAN){
//                                    source_check_image.setBackgroundDrawable(ContextCompat.getDrawable(context,android.R.color.transparent));
//                                }
//                                else{
//                                    source_check_image.setBackground(ContextCompat.getDrawable(context , android.R.color.transparent));
//                                }
//
//                            }
//                            break;
//                    }
//                    source_sub_menu_text.setText(submenu_name);
//                    break;

            case 4:
                convert_view = layoutInflater.inflate(R.layout.drawer_submenu_structure, null);
                source_sub_menu_text = convert_view.findViewById(R.id.base_drawer_submenu_text_view);
                source_sub_menu_image = convert_view.findViewById(R.id.base_drawer_submenu_image);
                source_check_image = convert_view.findViewById(R.id.checkable_image);
                switch (submenu_position) {
                    case 0:
                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                            source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_google_play_external));
                        } else {
                            source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_google_play_external));
                        }
                        break;
                    case 1:
                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                            source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_facebook_external));
                        } else {
                            source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_facebook_external));
                        }
                        break;
                }
                source_sub_menu_text.setText(submenu_name);
                break;
            case 5:
                convert_view = layoutInflater.inflate(R.layout.drawer_submenu_structure, null);
                source_sub_menu_text = convert_view.findViewById(R.id.base_drawer_submenu_text_view);
                source_sub_menu_image = convert_view.findViewById(R.id.base_drawer_submenu_image);
                source_check_image = convert_view.findViewById(R.id.checkable_image);
                switch (submenu_position) {
                    case 0:
                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                            source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_privacy));
                        } else {
                            source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_privacy));
                        }
                        break;
                    case 1:
                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                            source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_faq));
                        } else {
                            source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_faq));
                        }
                        break;
                    case 3:
                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                            source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_profile));
                        } else {
                            source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_profile));
                        }
                        break;

                }
                source_sub_menu_text.setText(submenu_name);
                break;


            case 6:
                convert_view = layoutInflater.inflate(R.layout.drawer_submenu_structure, null);
                source_sub_menu_text = convert_view.findViewById(R.id.base_drawer_submenu_text_view);
                source_sub_menu_image = convert_view.findViewById(R.id.base_drawer_submenu_image);

                switch (submenu_position) {
                    case 0:
                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                            source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_privacy));
                        } else {
                            source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_privacy));
                        }

                        break;
                    case 1:
                        if (SDK <= Build.VERSION_CODES.JELLY_BEAN) {
                            source_sub_menu_image.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_faq));
                        } else {
                            source_sub_menu_image.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_faq));
                        }
                        break;
                }
                source_sub_menu_text.setText(submenu_name);
                break;
        }
        return convert_view;
    }

    @Override
    public boolean isChildSelectable(int menu_position, int submenu_option) {
        return true;
    }
}

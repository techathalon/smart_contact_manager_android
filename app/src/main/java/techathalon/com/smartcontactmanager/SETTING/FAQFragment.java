package techathalon.com.smartcontactmanager.SETTING;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.R;

public class FAQFragment extends Fragment {
    Activity mActivity;
    ProgressBar progress;
    WebView wv_dialog;

    public FAQFragment() {
        // empty constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.privacy, container, false);
        progress = (ProgressBar) view.findViewById(R.id.progress);
        progress.setVisibility(View.VISIBLE);
        wv_dialog = (WebView) view.findViewById(R.id.wv_dialog);
        wv_dialog.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progress.setVisibility(View.GONE);
            }
        });
        wv_dialog.loadUrl(Constants.BASEURL +"/api_home/faq");
        return view;
    }
}
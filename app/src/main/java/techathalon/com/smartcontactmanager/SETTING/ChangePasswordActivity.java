package techathalon.com.smartcontactmanager.SETTING;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.HELPER_FILES.HelperActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.HttpHelper1;
import techathalon.com.smartcontactmanager.HELPER_FILES.MainActivity;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class ChangePasswordActivity extends HelperActivity {
    EditText currentPassword, newPassword, confirmNewPassword;
    TextView saveButton, mail_old_pwd;
    private SharedPreferences userDataPrefs;
    String currentPwd = "", newPwd = "", confirmNewPwd = "";
    HashMap<String, String> hashMap;
    HttpHelper1 httpHelper1;
    Intent commonIntent;
    AdView adView1, adView2, adView3;
    AdRequest adRequest;

    @Override
    public void setBackApiResponse1(String url, String object) {
        super.setBackApiResponse1(url, object);
        if (url.equals(Constants.CHANGE_PASSWORD_URL)) {
            if (object != null && !object.equals("")) {
                try {
                    JSONObject jsonObject = new JSONObject(object);
                    if (jsonObject.getString("success").equalsIgnoreCase("true")) {
                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
//                        commonIntent = new Intent(this, SettingsActivity.class);
//                        commonIntent = new Intent(this, MainActivity.class);
//                        startActivity(commonIntent);
                        onBackPressed();
//                        finish();
                    } else if (jsonObject.getString("success").equalsIgnoreCase("false")) {
                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (url.equals(Constants.SEND_PASSWORD_URL)) {
            if (object != null && !object.equals("")) {
                try {
                    JSONObject jsonObject = new JSONObject(object);
                    String success = jsonObject.getString("success");

                    if (success.equalsIgnoreCase("true")) {
                        Toast.makeText(this, "Password sent successfully to "
                                        + userDataPrefs.getString("registered_email", "") + ", check your inbox ",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_screen);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
//TODO Here is the google ads enable the constant to true in Constant file in project
        if(Constants.enable_ads){
            adView1 = (AdView) findViewById(R.id.adView1);
            adView2 = (AdView) findViewById(R.id.adView2);
            adView3 = (AdView) findViewById(R.id.adView3);

            adRequest = new AdRequest.Builder().build();
            adView1.loadAd(adRequest);
            adView2.loadAd(adRequest);
            adView3.loadAd(adRequest);
        }

        userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
        currentPassword = (EditText) findViewById(R.id.current_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmNewPassword = (EditText) findViewById(R.id.confirm_new_password);
        saveButton = (TextView) findViewById(R.id.save_button);
        mail_old_pwd = (TextView) findViewById(R.id.mail_old_pwd);

        if(Constants.enable_tracker){
            SCMApplication.getInstance().trackScreenView("Change Password");
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPwd = currentPassword.getText().toString();
                newPwd = newPassword.getText().toString();
                confirmNewPwd = confirmNewPassword.getText().toString();

                if (currentPwd.equalsIgnoreCase(""))
                    Toast.makeText(getApplicationContext(), "Old PIN is required", Toast.LENGTH_SHORT).show();
                else if (newPwd.equalsIgnoreCase(""))
                    Toast.makeText(getApplicationContext(), "New PIN is required", Toast.LENGTH_SHORT).show();
                else if (confirmNewPwd.equalsIgnoreCase(""))
                    Toast.makeText(getApplicationContext(), "Confirm PIN is required", Toast.LENGTH_SHORT).show();
                else if (!newPwd.equalsIgnoreCase(confirmNewPwd))
                    Toast.makeText(getApplicationContext(), "Confirm PIN doesn't matches", Toast.LENGTH_SHORT).show();
                else {
                    hashMap = new HashMap<String, String>();
                    hashMap.put("registered_email", userDataPrefs.getString("registered_email", ""));
                    hashMap.put("new_pswd", newPwd);
                    hashMap.put("old_pswd", currentPwd);
                    httpHelper1 = new HttpHelper1(Constants.CHANGE_PASSWORD_URL, hashMap, ChangePasswordActivity.this,
                            "Updating password");
                }
            }
        });
        mail_old_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hashMap = new HashMap<String, String>();
                hashMap.put("registered_email", userDataPrefs.getString("registered_email", ""));
                httpHelper1 = new HttpHelper1(Constants.SEND_PASSWORD_URL, hashMap, ChangePasswordActivity.this,
                        "Sending password...");
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        commonIntent = new Intent(this, MainActivity.class);
//        startActivity(commonIntent);
//        finish();
    }
}
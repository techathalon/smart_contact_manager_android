package techathalon.com.smartcontactmanager.SETTING;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.jsoup.Jsoup;

import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import techathalon.com.smartcontactmanager.BACKUP_HISTORY.PinActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.CommonActivity;
import techathalon.com.smartcontactmanager.TEXT_ANIMATION.HTextView;
import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.HELPER_FILES.MainActivity;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.NOTIFICATION.NotificationBroadcastReceiver;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class SettingsActivity<reset_view> extends CommonActivity {
    TextView reset;
    HTextView showEmailTextview;
    //    TextView reset, showEmailTextview;
    TextView reminderSelected, selectedBackupOptions, versionNumber, changePinText;
    ToggleButton merge_toggle_button;
    SharedPreferences pinPrefs, reninderPrefs, prefs, emailPrefs, pref, updatePreferences, reschedulePrefs,
            userDataPrefs;
    boolean check, blockButton;
    TextView pinTextview, mergeTextview;
    SharedPreferences.Editor reminderEdit, toggleEdit, updateEditor, reschedulePrefsEditor, userDataPrefsEditor;

    LinearLayout reminderOptions, backupOptions, changePinView, feedbackOptions, letsDoBusiness, privacyPolicy , log_out;
    final static int RQS_1 = 1;
    Calendar calendar;
    Intent intent, commonIntent;
    PendingIntent pendingIntent;
    AlarmManager alarmManager;
  LinearLayout reset_view;
    DateFormat dateFormat;
    Date date;

    LinearLayout five_days_reminder, ten_days_reminder, fifteen_days_reminder, thirty_days_reminder;
    TextView five_days_textview, ten_days_textview, fifteen_days_textview, thirty_days_textview;
    ImageView five_days_tick, ten_days_tick, fifteen_days_tick, thirty_days_tick;
    String selectedReminder = "", versionName = "", onlineVersion = "", addedDateString = "";
    AlertDialog alert;
    GetVerCode getVerCode;

    AdView adView1 , adView2 , adView3;
    AdRequest adRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.settings_screen);
        emailPrefs = getSharedPreferences("email data", Context.MODE_PRIVATE);
        pref = getSharedPreferences("LOGIN", 0);

        if(Constants.enable_tracker){
            SCMApplication.getInstance().trackScreenView("Setting");
        }

        if(Constants.enable_ads){
            adView1 = (AdView) findViewById(R.id.adView1);
            adView2 = (AdView) findViewById(R.id.adView2);
            adView3 = (AdView) findViewById(R.id.adView3);
            adRequest = new AdRequest.Builder().build();
            adView1.loadAd(adRequest);
            adView2.loadAd(adRequest);
            adView3.loadAd(adRequest);
        }
        pinTextview = (TextView) findViewById(R.id.pin_text);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (pInfo != null)
            versionName = pInfo.versionName;
        dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");
        date = new Date();

        selectedBackupOptions = (TextView) findViewById(R.id.selected_backup_options);
        changePinText = (TextView) findViewById(R.id.change_pin_text);

        versionNumber = (TextView) findViewById(R.id.version_number);
        versionNumber.setText(getString(R.string.app_name_with_space) + " v" + versionName);

        feedbackOptions = (LinearLayout) findViewById(R.id.feedback_options);
        letsDoBusiness = (LinearLayout) findViewById(R.id.lets_do_buisness_options);
        privacyPolicy = (LinearLayout) findViewById(R.id.privacy_options);

        feedbackOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commonIntent = new Intent(Intent.ACTION_VIEW);
                commonIntent.setType("plain/text");
                commonIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name_with_space));
                commonIntent.setData(Uri.parse("mailto:" + "info@techathalon.com"));
                startActivity(Intent.createChooser(commonIntent, "Send Feedback..."));
            }
        });

        letsDoBusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commonIntent = new Intent(Intent.ACTION_VIEW);
                commonIntent.setType("plain/text");
                commonIntent.putExtra(Intent.EXTRA_SUBJECT, "We are specialized into Web Development and Mobile development (iOS & android).");
                commonIntent.setData(Uri.parse("mailto:" + "kush.thakkar@techathalon.com"));
                startActivity(Intent.createChooser(commonIntent, "Lets do business..."));
            }
        });

        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    commonIntent = new Intent(SettingsActivity.this, HelpActivity.class);
                    startActivity(commonIntent);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                }
            }
        });

        reminderOptions = (LinearLayout) findViewById(R.id.reminder_options);
        backupOptions = (LinearLayout) findViewById(R.id.backup_options);
        backupOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commonIntent = new Intent(SettingsActivity.this, CustomizeBackupActivity.class);
                startActivity(commonIntent);
                finish();
            }
        });

        if (pref.getString("password", "").equalsIgnoreCase("")) {
            changePinText.setText("Set PIN Security");
            pinTextview.setText("This will set your security for contact backup");
        } else {
            changePinText.setText("Change Security PIN");
            pinTextview.setText("This will change your security PIN");
        }

        changePinView = (LinearLayout) findViewById(R.id.change_pin_view);
        changePinView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getString("password", "").equalsIgnoreCase("")) {
                    commonIntent = new Intent(SettingsActivity.this, PinActivity.class);
                    commonIntent.putExtra("first time", "yes");
                    commonIntent.putExtra("from settings", "settings");
                    startActivity(commonIntent);
                    finish();
                } else {
                    commonIntent = new Intent(SettingsActivity.this, ChangePasswordActivity.class);
                    startActivity(commonIntent);
                    finish();
                }
            }
        });

//        showEmailTextview = (TextView) findViewById(R.id.show_email_textview);
        showEmailTextview = (HTextView) findViewById(R.id.show_email_textview);
        prefs = getSharedPreferences("data", Context.MODE_PRIVATE);
//        showEmailTextview.setText("Your Email ID : " + prefs.getString("email-id", ""));
        showEmailTextview.animateText(getString(R.string.email_text) + prefs.getString("email-id", ""));

        calendar = Calendar.getInstance();
        intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mergeTextview = (TextView) findViewById(R.id.merge_text);
        merge_toggle_button = (ToggleButton) findViewById(R.id.merge_toggle_button);

        reminderSelected = (TextView) findViewById(R.id.reminder_selected);

        pinPrefs = getSharedPreferences("toggle flag", Context.MODE_PRIVATE);
        reninderPrefs = getSharedPreferences("reminder", Context.MODE_PRIVATE);
        reminderEdit = reninderPrefs.edit();
        check = false;
        blockButton = true;

        log_out = findViewById(R.id.logout);
        reset_view = findViewById(R.id.reset_view);

        Boolean flag_login = prefs.getBoolean("isflagLogin", false);
        if (flag_login==false){
            reset_view.setVisibility(View.GONE);
        }else {
            reset_view.setVisibility(View.VISIBLE);
        }

        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                builder.setTitle("Reset");
                builder.setMessage("Are you sure, you want to reset this app?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NotificationManager notifManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                        notifManager.cancelAll();

                        SharedPreferences prefs = getSharedPreferences("data", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putBoolean("isflagLogin", false);
                        editor.clear();
                        editor.apply();
                        reset_view.setVisibility(View.GONE);
                        SharedPreferences.Editor toggleEdit = pinPrefs.edit();
                        toggleEdit.clear();
                        toggleEdit.apply();

                        reminderEdit = reninderPrefs.edit();
                        reminderEdit.clear();
                        reminderEdit.apply();

                        userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
                        userDataPrefsEditor = userDataPrefs.edit();
                        userDataPrefsEditor.clear();
                        userDataPrefsEditor.apply();

                        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                        reschedulePrefs = getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                        intent.putExtra("option", reschedulePrefs.getString("option", ""));
                        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
                        alarmManager.cancel(pendingIntent);
                        pendingIntent.cancel();

                        Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        AccessToken.setCurrentAccessToken(null);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });
//        reset = (TextView) findViewById(R.id.reset_textview);
//        reset.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
//                builder.setTitle("Reset");
//                builder.setMessage("Are you sure, you want to reset this app?");
//                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        NotificationManager notifManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//                        notifManager.cancelAll();
//
//                        SharedPreferences prefs = getSharedPreferences("data", Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putBoolean("isflagLogin", false);
//                        editor.clear();
//                        editor.apply();
//
//                        SharedPreferences.Editor toggleEdit = pinPrefs.edit();
//                        toggleEdit.clear();
//                        toggleEdit.apply();
//
//                        reminderEdit = reninderPrefs.edit();
//                        reminderEdit.clear();
//                        reminderEdit.apply();
//
//                        userDataPrefs = getSharedPreferences("userData", Context.MODE_PRIVATE);
//                        userDataPrefsEditor = userDataPrefs.edit();
//                        userDataPrefsEditor.clear();
//                        userDataPrefsEditor.apply();
//
//                        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//                        intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
//                        reschedulePrefs = getSharedPreferences("reschedule", Context.MODE_PRIVATE);
//                        intent.putExtra("option", reschedulePrefs.getString("option", ""));
//                        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
//                        alarmManager.cancel(pendingIntent);
//                        pendingIntent.cancel();
//
//                        Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
//                        startActivity(intent);
//                        finish();
//                        AccessToken.setCurrentAccessToken(null);
//                    }
//                });
//
//                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
//            }
//        });

        selectedReminder = reninderPrefs.getString("option", "");
        Log.d("SELECT TIMER", "SELECTED REMONDER = " + selectedReminder);
        if (!selectedReminder.equals("")) {
            if (selectedReminder.equalsIgnoreCase("daily")) {
                reminderSelected.setText(getString(R.string.sett2));
            } else if (selectedReminder.equalsIgnoreCase("weekly")) {
                reminderSelected.setText(getString(R.string.sett2_10));
            } else if (selectedReminder.equalsIgnoreCase("monthly")) {
                reminderSelected.setText(getString(R.string.sett2_30));
            } else if (selectedReminder.equals("5") || selectedReminder.equals("10") || selectedReminder.equals("15") || selectedReminder.equals("30")) {
                switch (Integer.parseInt(selectedReminder)) {
                    case 5:
                        reminderSelected.setText(getString(R.string.sett2));
                        break;
                    case 10:
                        reminderSelected.setText(getString(R.string.sett2_10));
                        break;
                    case 15:
                        reminderSelected.setText(getString(R.string.sett2_15));
                        break;
                    case 30:
                        reminderSelected.setText(getString(R.string.sett2_30));
                        break;
                    default:
                        Log.d("defaultcase", "SettingsActivity " + reninderPrefs.getString("option", ""));
                }
            } else {
                reminderSelected.setText(getString(R.string.sett2));
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent alarmIntent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                alarmIntent.putExtra("option", "5");

                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
                alarmManager.cancel(pendingIntent);
                pendingIntent.cancel();
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, alarmIntent, PendingIntent.FLAG_ONE_SHOT);

                reminderEdit = reninderPrefs.edit();
                reminderEdit.putString("option", "5");
                reminderEdit.apply();

                Calendar calendar = Calendar.getInstance();
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");
                Date date = new Date();
                String data = dateFormat.format(date);
                calendar.add(Calendar.HOUR, 120);
//                calendar.add(Calendar.MINUTE, 1);

                Date addedDate = calendar.getTime();
                addedDateString = dateFormat.format(addedDate);
                Log.d("grtederee", "backup time = " + addedDateString);
                reschedulePrefs = getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                reschedulePrefsEditor = reschedulePrefs.edit();
                reschedulePrefsEditor.putString("alarm_time", addedDateString);
                reschedulePrefsEditor.putString("option", "5");
                reschedulePrefsEditor.apply();

                if (data.contains("PM") || data.contains("pm")) {
                    calendar.set(Calendar.AM_PM, Calendar.PM);
                } else {
                    calendar.set(Calendar.AM_PM, Calendar.AM);
                }
                data = calendar.get(Calendar.DAY_OF_MONTH) + "";
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                } else {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                }
            }
        }

        reminderOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callReminderDialog();
            }
        });

        toggleEdit = pinPrefs.edit();
        if (pinPrefs.getString("merge", "").equalsIgnoreCase("set")) {
            merge_toggle_button.setChecked(true);
            mergeTextview.setText(getString(R.string.sett1));
        } else {
            merge_toggle_button.setChecked(false);
        }

        merge_toggle_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mergeTextview.setText(getString(R.string.sett1));
                    toggleEdit.putString("merge", "set");
                } else {
                    mergeTextview.setText("No more alert message will ask you for\nconfirmation");
                    toggleEdit.putString("merge", "");
                }
                toggleEdit.apply();
            }
        });

        String attach = "";
        if (pinPrefs.getString("emailCheckbox", "").equalsIgnoreCase("checked")) {
            if (attach.equalsIgnoreCase("")) {
                attach = attach + "Email";
                selectedBackupOptions.setText(attach);
            }
        }

        if (pinPrefs.getString("addressCheckbox", "").equalsIgnoreCase("checked")) {
            if (attach.equalsIgnoreCase("")) {
                attach = attach + "Address";
                selectedBackupOptions.setText(attach);
            } else {
                if (attach.endsWith(", ")) {
                    attach = attach + "Address";
                    selectedBackupOptions.setText(attach);
                } else {
                    attach = attach + ", Address";
                    selectedBackupOptions.setText(attach);
                }
            }
        }

        if (pinPrefs.getString("noteCheckbox", "").equalsIgnoreCase("checked")) {
            if (attach.equalsIgnoreCase("")) {
                attach = attach + "Note";
                selectedBackupOptions.setText(attach);
            } else {
                if (attach.endsWith(", ")) {
                    attach = attach + "Note";
                    selectedBackupOptions.setText(attach);
                } else {
                    attach = attach + ", Note";
                    selectedBackupOptions.setText(attach);
                }
            }
        }

        if (pinPrefs.getString("urlCheckbox", "").equalsIgnoreCase("checked")) {
            if (attach.equalsIgnoreCase("")) {
                attach = attach + "URL";
                selectedBackupOptions.setText(attach);
            } else {
                if (attach.endsWith(", ")) {
                    attach = attach + "URL";
                    selectedBackupOptions.setText(attach);
                } else {
                    attach = attach + ", URL";
                    selectedBackupOptions.setText(attach);
                }
            }
        }

        if (pinPrefs.getString("emailCheckbox", "").equalsIgnoreCase("unchecked") && pinPrefs.getString("addressCheckbox", "").equalsIgnoreCase("unchecked") &&
                pinPrefs.getString("noteCheckbox", "").equalsIgnoreCase("unchecked") && pinPrefs.getString("urlCheckbox", "").equalsIgnoreCase("unchecked")) {
            selectedBackupOptions.setText("Not selected, tap to select");
        }

        updatePreferences = getSharedPreferences("UpdateData", Context.MODE_PRIVATE);
        if (updatePreferences.getInt("cancelCount", 0) < 3) {
            if (isNetworkAvailable()) {
                getVerCode = new GetVerCode();
                getVerCode.execute();
            }
        }
    }

    public void callReminderDialog() {
        final Dialog reminderDialog = new Dialog(SettingsActivity.this);
        if (reminderDialog.getWindow() != null)
            reminderDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        reminderDialog.getWindow().setBackgroundDrawableResource(R.drawable.no_corner_layout);
        reminderDialog.setContentView(R.layout.reminder_selection_view);

        five_days_reminder = (LinearLayout) reminderDialog.findViewById(R.id.five_days_reminder);
        ten_days_reminder = (LinearLayout) reminderDialog.findViewById(R.id.ten_days_reminder);
        fifteen_days_reminder = (LinearLayout) reminderDialog.findViewById(R.id.fifteen_days_reminder);
        thirty_days_reminder = (LinearLayout) reminderDialog.findViewById(R.id.thirty_days_reminder);

        five_days_textview = (TextView) reminderDialog.findViewById(R.id.five_days_textview);
        ten_days_textview = (TextView) reminderDialog.findViewById(R.id.ten_days_textview);
        fifteen_days_textview = (TextView) reminderDialog.findViewById(R.id.fifteen_days_textview);
        thirty_days_textview = (TextView) reminderDialog.findViewById(R.id.thirty_days_textview);

        five_days_tick = (ImageView) reminderDialog.findViewById(R.id.five_days_tick);
        ten_days_tick = (ImageView) reminderDialog.findViewById(R.id.ten_days_tick);
        fifteen_days_tick = (ImageView) reminderDialog.findViewById(R.id.fifteen_days_tick);
        thirty_days_tick = (ImageView) reminderDialog.findViewById(R.id.thirty_days_tick);

        selectedReminder = reninderPrefs.getString("option", "");
        if (!selectedReminder.equals("")) {
            if (selectedReminder.equalsIgnoreCase("daily")) {
                five_days_textview.setTextColor(Color.parseColor("#2196f3"));
                ten_days_textview.setTextColor(Color.parseColor("#000000"));
                fifteen_days_textview.setTextColor(Color.parseColor("#000000"));
                thirty_days_textview.setTextColor(Color.parseColor("#000000"));
                five_days_tick.setVisibility(View.VISIBLE);
                ten_days_tick.setVisibility(View.GONE);
                fifteen_days_tick.setVisibility(View.GONE);
                thirty_days_tick.setVisibility(View.GONE);
            } else if (selectedReminder.equalsIgnoreCase("weekly")) {
                five_days_textview.setTextColor(Color.parseColor("#000000"));
                ten_days_textview.setTextColor(Color.parseColor("#2196f3"));
                fifteen_days_textview.setTextColor(Color.parseColor("#000000"));
                thirty_days_textview.setTextColor(Color.parseColor("#000000"));
                five_days_tick.setVisibility(View.GONE);
                ten_days_tick.setVisibility(View.VISIBLE);
                fifteen_days_tick.setVisibility(View.GONE);
                thirty_days_tick.setVisibility(View.GONE);
            } else if (selectedReminder.equalsIgnoreCase("monthly")) {
                five_days_textview.setTextColor(Color.parseColor("#000000"));
                ten_days_textview.setTextColor(Color.parseColor("#000000"));
                fifteen_days_textview.setTextColor(Color.parseColor("#000000"));
                thirty_days_textview.setTextColor(Color.parseColor("#2196f3"));
                five_days_tick.setVisibility(View.GONE);
                ten_days_tick.setVisibility(View.GONE);
                fifteen_days_tick.setVisibility(View.GONE);
                thirty_days_tick.setVisibility(View.VISIBLE);
            } else if (selectedReminder.equals("5") || selectedReminder.equals("10") || selectedReminder.equals("15") || selectedReminder.equals("30")) {
                switch (Integer.parseInt(selectedReminder)) {
                    case 5:
                        five_days_textview.setTextColor(Color.parseColor("#2196f3"));
                        ten_days_textview.setTextColor(Color.parseColor("#000000"));
                        fifteen_days_textview.setTextColor(Color.parseColor("#000000"));
                        thirty_days_textview.setTextColor(Color.parseColor("#000000"));
                        five_days_tick.setVisibility(View.VISIBLE);
                        ten_days_tick.setVisibility(View.GONE);
                        fifteen_days_tick.setVisibility(View.GONE);
                        thirty_days_tick.setVisibility(View.GONE);
                        break;
                    case 10:
                        five_days_textview.setTextColor(Color.parseColor("#000000"));
                        ten_days_textview.setTextColor(Color.parseColor("#2196f3"));
                        fifteen_days_textview.setTextColor(Color.parseColor("#000000"));
                        thirty_days_textview.setTextColor(Color.parseColor("#000000"));
                        five_days_tick.setVisibility(View.GONE);
                        ten_days_tick.setVisibility(View.VISIBLE);
                        fifteen_days_tick.setVisibility(View.GONE);
                        thirty_days_tick.setVisibility(View.GONE);
                        break;
                    case 15:
                        five_days_textview.setTextColor(Color.parseColor("#000000"));
                        ten_days_textview.setTextColor(Color.parseColor("#000000"));
                        fifteen_days_textview.setTextColor(Color.parseColor("#2196f3"));
                        thirty_days_textview.setTextColor(Color.parseColor("#000000"));
                        five_days_tick.setVisibility(View.GONE);
                        ten_days_tick.setVisibility(View.GONE);
                        fifteen_days_tick.setVisibility(View.VISIBLE);
                        thirty_days_tick.setVisibility(View.GONE);
                        break;
                    case 30:
                        five_days_textview.setTextColor(Color.parseColor("#000000"));
                        ten_days_textview.setTextColor(Color.parseColor("#000000"));
                        fifteen_days_textview.setTextColor(Color.parseColor("#000000"));
                        thirty_days_textview.setTextColor(Color.parseColor("#2196f3"));
                        five_days_tick.setVisibility(View.GONE);
                        ten_days_tick.setVisibility(View.GONE);
                        fifteen_days_tick.setVisibility(View.GONE);
                        thirty_days_tick.setVisibility(View.VISIBLE);
                        break;
                }
            } else {
                five_days_textview.setTextColor(Color.parseColor("#2196f3"));
                ten_days_textview.setTextColor(Color.parseColor("#000000"));
                fifteen_days_textview.setTextColor(Color.parseColor("#000000"));
                thirty_days_textview.setTextColor(Color.parseColor("#000000"));
                five_days_tick.setVisibility(View.VISIBLE);
                ten_days_tick.setVisibility(View.GONE);
                fifteen_days_tick.setVisibility(View.GONE);
                thirty_days_tick.setVisibility(View.GONE);
            }
        }

        five_days_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                intent.putExtra("option", "5");
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
                alarmManager.cancel(pendingIntent);
                pendingIntent.cancel();

                intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                intent.putExtra("option", "5");
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                if (reminderDialog != null && reminderDialog.isShowing())
                reminderDialog.dismiss();
                reminderSelected.setText(getString(R.string.sett2));
                reminderEdit.putString("option", "5");
                reminderEdit.apply();
                Calendar calendar = Calendar.getInstance();
                String data = dateFormat.format(date);
                calendar.add(Calendar.HOUR, 120);
                //TODO by sonali
//                calendar.add(Calendar.MINUTE, 1);

                Date addedDate = calendar.getTime();
                addedDateString = dateFormat.format(addedDate);
                reschedulePrefs = getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                reschedulePrefsEditor = reschedulePrefs.edit();
                reschedulePrefsEditor.putString("alarm_time", addedDateString);
                reschedulePrefsEditor.putString("option", "5");
                reschedulePrefsEditor.apply();

                if (data.contains("PM") || data.contains("pm")) {
                    calendar.set(Calendar.AM_PM, Calendar.PM);
                } else {
                    calendar.set(Calendar.AM_PM, Calendar.AM);
                }
                data = calendar.get(Calendar.DAY_OF_MONTH) + "";
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                } else {
                     alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                }
            }
        });

        ten_days_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                intent.putExtra("option", "10");
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
                alarmManager.cancel(pendingIntent);
                pendingIntent.cancel();

                intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                intent.putExtra("option", "10");
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                if (reminderDialog != null && reminderDialog.isShowing())
                reminderDialog.dismiss();
                reminderSelected.setText(getString(R.string.sett2_10));
                reminderEdit.putString("option", "10");
                reminderEdit.apply();
                Calendar calendar = Calendar.getInstance();

                String data = dateFormat.format(date);
                calendar.add(Calendar.HOUR, 240);
//                calendar.add(Calendar.MINUTE, 2);

                Date addedDate = calendar.getTime();
                addedDateString = dateFormat.format(addedDate);
                reschedulePrefs = getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                reschedulePrefsEditor = reschedulePrefs.edit();
                reschedulePrefsEditor.putString("alarm_time", addedDateString);
                reschedulePrefsEditor.putString("option", "10");
                reschedulePrefsEditor.apply();

                if (data.contains("pm") || data.contains("PM")) {
                    calendar.set(Calendar.AM_PM, Calendar.PM);
                } else {
                    calendar.set(Calendar.AM_PM, Calendar.AM);
                }
                data = calendar.get(Calendar.DAY_OF_MONTH) + "";
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                } else {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                }
            }
        });

        fifteen_days_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                intent.putExtra("option", "15");
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
                alarmManager.cancel(pendingIntent);
                pendingIntent.cancel();

                intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                intent.putExtra("option", "15");
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                if (reminderDialog != null && reminderDialog.isShowing())
                reminderDialog.dismiss();
                reminderSelected.setText(getString(R.string.sett2_15));
                reminderEdit.putString("option", "15");
                reminderEdit.apply();
                Calendar calendar = Calendar.getInstance();

                String data = dateFormat.format(date);
                calendar.add(Calendar.HOUR, 360);
//                calendar.add(Calendar.MINUTE, 3);

                Date addedDate = calendar.getTime();
                addedDateString = dateFormat.format(addedDate);
                reschedulePrefs = getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                reschedulePrefsEditor = reschedulePrefs.edit();
                reschedulePrefsEditor.putString("alarm_time", addedDateString);
                reschedulePrefsEditor.putString("option", "15");
                reschedulePrefsEditor.apply();

                if (data.contains("pm") || data.contains("PM")) {
                    calendar.set(Calendar.AM_PM, Calendar.PM);
                } else {
                    calendar.set(Calendar.AM_PM, Calendar.AM);
                }
                data = calendar.get(Calendar.DAY_OF_MONTH) + "  " + calendar.get(Calendar.MONTH);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                } else {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                }
            }
        });

        thirty_days_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                intent.putExtra("option", "30");
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
                alarmManager.cancel(pendingIntent);
                pendingIntent.cancel();

                intent = new Intent(getApplicationContext(), NotificationBroadcastReceiver.class);
                intent.putExtra("option", "30");
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                if (reminderDialog != null && reminderDialog.isShowing())
                reminderDialog.dismiss();
                reminderSelected.setText(getString(R.string.sett2_30));
                reminderEdit.putString("option", "30");
                reminderEdit.apply();
                Calendar calendar = Calendar.getInstance();

                String data = dateFormat.format(date);
                calendar.add(Calendar.HOUR, 720);
//                calendar.add(Calendar.MINUTE, 4);

                Date addedDate = calendar.getTime();
                addedDateString = dateFormat.format(addedDate);
                reschedulePrefs = getSharedPreferences("reschedule", Context.MODE_PRIVATE);
                reschedulePrefsEditor = reschedulePrefs.edit();
                reschedulePrefsEditor.putString("alarm_time", addedDateString);
                reschedulePrefsEditor.putString("option", "30");
                reschedulePrefsEditor.apply();

                if (data.contains("pm") || data.contains("PM")) {
                    calendar.set(Calendar.AM_PM, Calendar.PM);
                } else {
                    calendar.set(Calendar.AM_PM, Calendar.AM);
                }
                data = calendar.get(Calendar.DAY_OF_MONTH) + "  " + calendar.get(Calendar.MONTH);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                } else {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                }
            }
        });

        reminderDialog.show();
    }

    private class GetVerCode extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                onlineVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=techathalon.com.smartcontactmanager&hl=en_GB")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select(".hAyfc .htlgb")
                        .get(7)
                        .ownText();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return onlineVersion;
        }

        @Override
        protected void onPostExecute(String s) {
            if (onlineVersion != null && !onlineVersion.equals("") && !versionName.equals(onlineVersion)) {
                Log.d("version", "number = " + onlineVersion);
                if (!isRestricted() && !isFinishing())
                    showUpdateDialog();
            }
        }
    }

    private void showUpdateDialog() {
        updatePreferences = getSharedPreferences("UpdateData", Context.MODE_PRIVATE);
        updateEditor = updatePreferences.edit();
        final AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setTitle("Update Available");
        builder.setMessage("The new version of Smart Contact Manager is available on Play Store.");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                commonIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.smartLink1));
                startActivity(commonIntent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                updateEditor.putInt("cancelCount", updatePreferences.getInt("cancelCount", 0) + 1);
                updateEditor.apply();
            }
        });
        alert = builder.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        commonIntent = new Intent(this, MainActivity.class);
//        startActivity(commonIntent);
        finish();
    }


    private void SetAlarm() {
//        Calendar c = Calendar.getInstance();
//        Intent intent = new Intent(this, NotificationBroadcastReceiver.class);
//        for (i in 0..notificationList.size - 1) {
//            var alarmPendingIntent = PendingIntent.getBroadcast(
//                    applicationContext, i, intent, 0
//            )
//            var hour = notificationList.get(i).twentyFourHour.substringBefore(":").toInt()
//            var minute = notificationList.get(i).twentyFourHour.substringAfter(":").toInt()
//
//            c.set(Calendar.HOUR_OF_DAY, hour)
//            c.set(Calendar.MINUTE, minute)
//            c.set(Calendar.SECOND, 0)
//            pendingArray.add(alarmPendingIntent)
//            if (notificationList[i].switch == 1) {
//                alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.timeInMillis, alarmPendingIntent)
//            } else {
//                Log.d("Cancel_Alarm", "Cancel Alarm")
//            }
//        }
    }


}
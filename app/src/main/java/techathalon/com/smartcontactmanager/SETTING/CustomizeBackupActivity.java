package techathalon.com.smartcontactmanager.SETTING;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import techathalon.com.smartcontactmanager.HELPER_FILES.CommonActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.HELPER_FILES.MainActivity;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class CustomizeBackupActivity extends CommonActivity {
    ImageView backButton;
    SharedPreferences pinPrefs;
    SharedPreferences.Editor toggleEdit;
    LinearLayout email_option, address_option, note_option, url_option;
    ImageView email_tick, address_tick, note_tick, url_tick;
    Intent commonIntent;
    AdView adView1, adView2, adView3, adView4;
    AdRequest adRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vcf_file_options);
        adView1 = (AdView) findViewById(R.id.adView1);
        adView2 = (AdView) findViewById(R.id.adView2);
        adView3 = (AdView) findViewById(R.id.adView3);
        adView4 = (AdView) findViewById(R.id.adView4);

        //TODO Here is the google ads enable the constant to true in Constant file in project
        if(Constants.enable_ads){
            adRequest = new AdRequest.Builder().build();
            adView1.loadAd(adRequest);
            adView2.loadAd(adRequest);
            adView3.loadAd(adRequest);
            adView4.loadAd(adRequest);
        }

        if(Constants.enable_tracker){
            SCMApplication.getInstance().trackScreenView("Custom Backup");
        }
        pinPrefs = getSharedPreferences("toggle flag", Context.MODE_PRIVATE);
        toggleEdit = pinPrefs.edit();

        email_option = (LinearLayout) findViewById(R.id.email_option);
        address_option = (LinearLayout) findViewById(R.id.address_option);
        note_option = (LinearLayout) findViewById(R.id.note_option);
        url_option = (LinearLayout) findViewById(R.id.url_option);
        email_tick = (ImageView) findViewById(R.id.email_tick);
        address_tick = (ImageView) findViewById(R.id.address_tick);
        note_tick = (ImageView) findViewById(R.id.note_tick);
        url_tick = (ImageView) findViewById(R.id.url_tick);
        backButton = (ImageView) findViewById(R.id.back);

        if (pinPrefs.getString("emailCheckbox", "").equalsIgnoreCase("checked"))
            email_tick.setVisibility(View.VISIBLE);
        else
            email_tick.setVisibility(View.GONE);
        if (pinPrefs.getString("addressCheckbox", "").equalsIgnoreCase("checked"))
            address_tick.setVisibility(View.VISIBLE);
        else
            address_tick.setVisibility(View.GONE);
        if (pinPrefs.getString("noteCheckbox", "").equalsIgnoreCase("checked"))
            note_tick.setVisibility(View.VISIBLE);
        else
            note_tick.setVisibility(View.GONE);
        if (pinPrefs.getString("urlCheckbox", "").equalsIgnoreCase("checked"))
            url_tick.setVisibility(View.VISIBLE);
        else
            url_tick.setVisibility(View.GONE);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                commonIntent = new Intent(CustomizeBackupActivity.this, SettingsActivity.class);
                commonIntent = new Intent(CustomizeBackupActivity.this, MainActivity.class);
                commonIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(commonIntent);
                finish();
            }
        });
        email_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email_tick.getVisibility() == View.GONE) {
                    email_tick.setVisibility(View.VISIBLE);
                    toggleEdit.putString("emailCheckbox", "checked");
                } else {
                    email_tick.setVisibility(View.GONE);
                    toggleEdit.putString("emailCheckbox", "unchecked");
                }
                toggleEdit.apply();
            }
        });
        address_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (address_tick.getVisibility() == View.GONE) {
                    address_tick.setVisibility(View.VISIBLE);
                    toggleEdit.putString("addressCheckbox", "checked");
                } else {
                    address_tick.setVisibility(View.GONE);
                    toggleEdit.putString("addressCheckbox", "unchecked");
                }
                toggleEdit.apply();
            }
        });
        note_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (note_tick.getVisibility() == View.GONE) {
                    note_tick.setVisibility(View.VISIBLE);
                    toggleEdit.putString("noteCheckbox", "checked");
                } else {
                    note_tick.setVisibility(View.GONE);
                    toggleEdit.putString("noteCheckbox", "unchecked");
                }
                toggleEdit.apply();
            }
        });
        url_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (url_tick.getVisibility() == View.GONE) {
                    url_tick.setVisibility(View.VISIBLE);
                    toggleEdit.putString("urlCheckbox", "checked");
                } else {
                    url_tick.setVisibility(View.GONE);
                    toggleEdit.putString("urlCheckbox", "unchecked");
                }
                toggleEdit.apply();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonIntent = new Intent(this, SettingsActivity.class);
//        commonIntent = new Intent(this, MainActivity.class);
        startActivity(commonIntent);
        finish();
    }
}
package techathalon.com.smartcontactmanager.SETTING;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import techathalon.com.smartcontactmanager.HELPER_FILES.CommonActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class HelpActivity extends CommonActivity {
    AdView adView1;
    AdRequest adRequest;
    TextView title, faq_title, privacy_title;
    Intent myIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_activity);
        adView1 = (AdView) findViewById(R.id.adView1);
        title = (TextView) findViewById(R.id.title);
        faq_title = (TextView) findViewById(R.id.faq_title);
        privacy_title = (TextView) findViewById(R.id.privacy_title);
        myIntent = getIntent();
        int menu_id = myIntent.getIntExtra("help", 0);

        if(Constants.enable_tracker){
            SCMApplication.getInstance().trackScreenView("HelpActivity");
        }

        //TODO Here is the google ads enable the constant to true in Constant file in project
        if (Constants.enable_ads) {
            adRequest = new AdRequest.Builder().build();
            adView1.loadAd(adRequest);
        }

        switch (menu_id) {
            case 1:
                title.setText("Privacy Policy");
                getFragmentManager().beginTransaction().replace(R.id.frame_history, new PrivacyPolicyFragment()).commitAllowingStateLoss();
                break;
            case 2:
                title.setText("FAQ");
                getFragmentManager().beginTransaction().replace(R.id.frame_history, new FAQFragment()).commitAllowingStateLoss();
                break;
        }

//        faq_title.setTypeface(null, Typeface.BOLD);
//        getFragmentManager().beginTransaction().replace(R.id.frame_history, new FAQFragment())
//                .commitAllowingStateLoss();
//
////        adRequest = new AdRequest.Builder().build();  //TODO do not uncomment it for google ads
////        adView1.loadAd(adRequest);                   //TODO do not uncomment this for google ads
//
//        faq_title.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                title.setText("FAQ");
//                faq_title.setTypeface(null, Typeface.BOLD);
//                privacy_title.setTypeface(null, Typeface.NORMAL);
//                getFragmentManager().beginTransaction().replace(R.id.frame_history, new FAQFragment())
//                        .commitAllowingStateLoss();
//            }
//        });
//        privacy_title.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                title.setText("Privacy Policy");
//                privacy_title.setTypeface(null, Typeface.BOLD);
//                faq_title.setTypeface(null, Typeface.NORMAL);
//                getFragmentManager().beginTransaction().replace(R.id.frame_history, new PrivacyPolicyFragment())
//                        .commitAllowingStateLoss();
//            }
//        });
    }
}
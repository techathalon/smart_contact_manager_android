package techathalon.com.smartcontactmanager.DUPLICATE_SCAN;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import techathalon.com.smartcontactmanager.R;

public class MergedContactNameAdapter extends BaseAdapter {
    ArrayList<Contact> contactArrayList;
    ContactNameFragment fragment;
    Activity activity;

    public MergedContactNameAdapter(ArrayList<Contact> contactArrayList, Activity activity) {
        this.contactArrayList = contactArrayList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return contactArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return contactArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        Contact contact = contactArrayList.get(i);
        return contactArrayList.indexOf(contact);
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater;
            if (activity == null)
                layoutInflater = LayoutInflater.from(fragment.getActivity());
            else
                layoutInflater = LayoutInflater.from(activity);
            convertView = layoutInflater.inflate(R.layout.eye_listview_item, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.eye_name);
            viewHolder.info = (TextView) convertView.findViewById(R.id.eye_info);
            viewHolder.profilePicture = (ImageView) convertView.findViewById(R.id.eye_profilePic);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Contact contact = contactArrayList.get(i);
        final String name = contact.getName();
        if (name.equals(""))
            viewHolder.name.setText("");
        else
            viewHolder.name.setText(name);
        String contactString = "";
        String emailString = "";

        ArrayList<String> contacs = contact.getContact();
        if (contacs != null) {
            if (contacs.size() > 0) {
                for (String contacts : contact.getContact())
                    if (!contacts.equals(""))
                        contactString += contacts + "\n";
            }
        }

        ArrayList<String> allEmails = contact.getEmail();
        if (allEmails != null) {
            if (allEmails.size() > 0) {
                for (String emails : contact.getEmail())
                    if (!emails.equals(""))
                        emailString += emails + "\n";
            }
        }
        if (emailString.trim().equals("") && contactString.trim().equals(""))
            viewHolder.info.setText("No information added");
        else {
            String result = "";
            if (!emailString.equals(""))
                result = emailString.trim() + "\n" + contactString.trim();
            else
                result = contactString.trim();
            viewHolder.info.setText(result);
        }

        Bitmap my_btmp = contact.getBitmap();

        if (my_btmp != null)
            viewHolder.profilePicture.setImageBitmap(my_btmp);
        else {
            if (activity != null)
                viewHolder.profilePicture.setImageDrawable(ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.newprofileimage));
            else
                viewHolder.profilePicture.setImageDrawable(ContextCompat.getDrawable(fragment.getActivity().getApplicationContext(), R.drawable.newprofileimage));
        }
        return convertView;
    }

    class ViewHolder {
        TextView name, info;
        ImageView profilePicture;
    }

}

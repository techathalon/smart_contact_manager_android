package techathalon.com.smartcontactmanager.DUPLICATE_SCAN;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentProviderOperation;
import android.content.OperationApplicationException;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import techathalon.com.smartcontactmanager.R;

public class DuplicateNumberAdapter extends BaseAdapter {
    ArrayList<Contact> contactArrayList;
    Activity activity;
    MergedContactNumberAdapter adapter;
    ContactNumberFragment fragment;
    ArrayList<Contact> mergeEyeContacts = new ArrayList<>();
    ArrayList<String> deleted;
    ArrayList<Contact> mergeContacts;

    public DuplicateNumberAdapter(Activity activity, ContactNumberFragment fragment, ArrayList<Contact> contactArrayList) {
        this.activity = activity;
        this.fragment = fragment;
        adapter = new MergedContactNumberAdapter(mergeEyeContacts, activity);
        this.contactArrayList = contactArrayList;
    }

    @Override
    public int getCount() {
        return contactArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return contactArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(activity);
            convertView = layoutInflater.inflate(R.layout.contact_item, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.info = (TextView) convertView.findViewById(R.id.info);
            viewHolder.profilepic = (ImageView) convertView.findViewById(R.id.profilePic);
            viewHolder.tick = (ImageView) convertView.findViewById(R.id.tick);
            viewHolder.cross = (ImageView) convertView.findViewById(R.id.cross);
            viewHolder.options = (LinearLayout) convertView.findViewById(R.id.options);
            viewHolder.dividerView = (LinearLayout) convertView.findViewById(R.id.dividerView);
            viewHolder.checked = (ImageView) convertView.findViewById(R.id.profile_check);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Contact contact = null;
        if (i <= contactArrayList.size())
            contact = contactArrayList.get(i);
        if (contact != null) {
            final String name = contact.getDisplay_name();
            if (contact.isChecked)
                viewHolder.checked.setImageDrawable(ContextCompat.getDrawable(activity.getApplicationContext(), android.R.drawable.checkbox_on_background));
            else
                viewHolder.checked.setImageDrawable(ContextCompat.getDrawable(activity.getApplicationContext(), android.R.drawable.checkbox_off_background));
            if (name.equals(""))
                viewHolder.name.setText("");
            else
                viewHolder.name.setText(contact.getDisplay_name());
            viewHolder.cross.setTag(contact.getDisplay_name());
            viewHolder.cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleted = new ArrayList<String>();
                    for (int j = 0; j < contactArrayList.size(); j++) {
                        if (contactArrayList.get(j).getDisplay_name().equalsIgnoreCase(view.getTag().toString())) {
                            deleted.add(i + "");
                        }
                    }
                    for (String key : deleted) {
                        if (Integer.parseInt(key) < contactArrayList.size()) {
                            contactArrayList.remove(Integer.parseInt(key));
                        }
                    }
                    notifyDataSetChanged();
                    Toast.makeText(activity.getApplicationContext(), name + " skipped", Toast.LENGTH_SHORT).show();
                    TextView txt_contact_img = (TextView) activity.findViewById(R.id.count_number);
                    if (contactArrayList.size() == 0) {
                        if (fragment != null) {
                            fragment.msg.setVisibility(View.VISIBLE);
                            fragment.listView.setVisibility(View.GONE);
                        }
                        activity.findViewById(R.id.actionLayout).setVisibility(View.GONE);
                    }
                    int counts = Integer.parseInt(txt_contact_img.getText().toString());
                    txt_contact_img.setText(--counts + "");
                }
            });
            viewHolder.tick.setTag(contact.getDisplay_name());
            viewHolder.tick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleted = new ArrayList<String>();
                    mergeContacts = new ArrayList<Contact>();
                    for (int j = 0; j < contactArrayList.size(); j++) {
                        if (contactArrayList.get(j).getDisplay_name().equalsIgnoreCase(view.getTag().toString())) {
                            deleted.add(i + "");
                            if (contactArrayList.get(j).isChecked)
                                mergeContacts.add(contactArrayList.get(j));
                        }
                    }
                    for (String key : deleted) {
                        if (Integer.parseInt(key) < contactArrayList.size()) {
                            contactArrayList.remove(Integer.parseInt(key));
                        }
                    }
                    notifyDataSetChanged();
                    if (mergeContacts.size() > 0) {
                        Toast.makeText(activity, name + " merged successfully", Toast.LENGTH_SHORT).show();
                        MergeDeleteAddContacts CONTACT = new MergeDeleteAddContacts(mergeContacts);
                        CONTACT.execute();
                        if (contactArrayList.size() == 0) {
                            if (fragment != null) {
                                fragment.msg.setVisibility(View.VISIBLE);
                                fragment.listView.setVisibility(View.GONE);
                            }
                            activity.findViewById(R.id.actionLayout).setVisibility(View.GONE);
                            final Dialog eyeDialog = new Dialog(activity);
                            eyeDialog.setTitle("Merged Contacts");
                            eyeDialog.setCancelable(false);
                            eyeDialog.setContentView(R.layout.eye_listview);
                            ListView eyeListview = (ListView) eyeDialog.findViewById(R.id.eye_listView);
                            if (CONTACT.getStatus() == AsyncTask.Status.RUNNING) {
                                eyeListview.setAdapter(adapter);
                            }
                            TextView okButton = (TextView) eyeDialog.findViewById(R.id.ok_button);
                            okButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (eyeDialog != null && eyeDialog.isShowing())
                                    eyeDialog.dismiss();
                                }
                            });
                            eyeDialog.show();
                        }
                    } else
                        Toast.makeText(activity.getApplicationContext(), name + " skipped", Toast.LENGTH_SHORT).show();
                    TextView txt_contact_img = (TextView) activity.findViewById(R.id.count_number);
                    int counts = Integer.parseInt(txt_contact_img.getText().toString());
                    txt_contact_img.setText(--counts + "");
                }
            });
            String contactString = contact.getName();
            String emailString = "";
            if (contact.isShouldCheck()) {
                viewHolder.options.setVisibility(View.VISIBLE);
                viewHolder.dividerView.setVisibility(View.VISIBLE);
            } else {
                viewHolder.options.setVisibility(View.GONE);
                viewHolder.dividerView.setVisibility(View.GONE);
            }
            if (emailString.trim().equals("") && contactString.trim().equals(""))
                viewHolder.info.setText("No information added");
            else {
                String result = "";
                if (!emailString.equals(""))
                    result = emailString.trim() + "\n" + contactString.trim();
                else
                    result = contactString.trim();
                viewHolder.info.setText(result);
            }
            Bitmap my_btmp = contact.getBitmap();
            if (my_btmp != null)
                viewHolder.profilepic.setImageBitmap(my_btmp);
            else
                viewHolder.profilepic.setImageDrawable(ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.newprofileimage));
        }
        return convertView;
    }

    private class ViewHolder {
        TextView name;
        TextView info;
        ImageView profilepic, tick, cross, checked;
        LinearLayout options, dividerView;
    }

    private class MergeDeleteAddContacts extends AsyncTask<Void, String, Contact> {
        ArrayList<Contact> temp = new ArrayList<>();

        public MergeDeleteAddContacts(ArrayList<Contact> mergeContacts) {
            temp = mergeContacts;
        }

        @Override
        protected Contact doInBackground(Void... voids) {
            Contact contact1 = new Contact();
            ArrayList<Contact> deleteStatus = new ArrayList<>();
            ArrayList<String> allContacts = new ArrayList<>();
            ArrayList<String> allEmails = new ArrayList<>();
            String name1 = "";
            Bitmap image = null;
            for (int i = 0; i < temp.size(); i++) {
                Contact contact = temp.get(i);
                name1 = temp.get(i).getName();
                ArrayList<String> mergedContactList = contact.getContact();
                if (mergedContactList != null) {
                    for (int i1 = 0; i1 < mergedContactList.size(); i1++) {
                        if (!allContacts.contains(mergedContactList.get(i1)))
                            allContacts.add(mergedContactList.get(i1));
                    }
                }
                ArrayList<String> mergedEmailList = contact.getEmail();
                if (mergedEmailList != null) {
                    for (int i1 = 0; i1 < mergedEmailList.size(); i1++) {
                        if (!allEmails.contains(mergedEmailList.get(i1)))
                            allEmails.add(mergedEmailList.get(i1));
                    }
                }
                image = contact.getBitmap();
                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, contact.getLookup());
                try {
                    if (activity.getContentResolver().delete(uri, null, null) == 0) {
                        deleteStatus.add(contact);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            contact1.setName(name1);
            contact1.setContact(allContacts);
            contact1.setEmail(allEmails);
            contact1.setBitmap(image);
            addContact(contact1.getName(), contact1.getContact(), contact1.getEmail(), contact1.getBitmap());
            return contact1;
        }

        @Override
        protected void onPostExecute(Contact aVoid) {
            super.onPostExecute(aVoid);
            mergeEyeContacts.add(aVoid);
            adapter.notifyDataSetChanged();
        }
    }

    private void addContact(String name, ArrayList<String> phone, ArrayList<String> email, Bitmap mBitmap) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        int rawContactID = ops.size();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());
        if (name == null)
            name = "";
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name)
                .build());
        if (phone == null)
            phone = new ArrayList<>();
        for (String numberAtIndex : phone) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, numberAtIndex)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }
        if (email == null)
            email = new ArrayList<>();
        for (String emailAtIndex : email) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, emailAtIndex)
                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                    .build());
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (mBitmap != null) {
            mBitmap.compress(Bitmap.CompressFormat.PNG, 75, stream);
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, stream.toByteArray())
                    .build());
            try {
                stream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            if (activity != null)
                activity.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }
}
package techathalon.com.smartcontactmanager.DUPLICATE_SCAN;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

import ezvcard.Ezvcard;
import ezvcard.VCard;
import ezvcard.VCardVersion;
import ezvcard.property.Address;
import ezvcard.property.Email;
import ezvcard.property.Note;
import ezvcard.property.Url;
import techathalon.com.smartcontactmanager.CREATE_BACKUP.DisplayContactActivity;
import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.HELPER_FILES.MainActivity;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class NewDuplicateActivity extends FragmentActivity {
    private final static String WHATSSAP_APP_ID = "com.whatsapp";
    ViewPager viewPager;
    ScreenSlidePagerAdapter mPagerAdapter;
    LinearLayout line_number, line_name, lin_contact, lin_number, actionLayout;
    TextView count_name, count_number;
    TextView proceed, rescan, message;
    Dialog backupDialog, allContactsMergerDialog;
    ShareDialog shareDialog;
    private SharedPreferences pinPrefs;
    private SharedPreferences emailPrefs;
    private SharedPreferences prefs;
    CallbackManager callbackManager;
    private String storage_path = "";
    static String folderPath;
    private String user_id;
    private TreeMap<String, ArrayList<Contact>> selectedMapData = new TreeMap<>();
    private TreeMap<String, ArrayList<Contact>> selectedMapData2 = new TreeMap<>();
    MergeDeleteNameContacts namecontact;
    MergeDeleteNumberContacts numberContact;
    UploadFiles uploadFiles;
    int page = 0;
    boolean name, number;

    AdView adView1;
    AdRequest adRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_duplicate_activity);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Constants.enable_tracker) {
            SCMApplication.getInstance().trackScreenView("Duplicate Scan");
        }
        FacebookSdk.sdkInitialize(getApplicationContext());
        pinPrefs = getSharedPreferences("toggle flag", Context.MODE_PRIVATE);
        emailPrefs = getSharedPreferences("email data", Context.MODE_PRIVATE);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        prefs = getSharedPreferences("data", Context.MODE_PRIVATE);
        user_id = prefs.getString("user_id", "");
        actionLayout = findViewById(R.id.actionLayout);
        line_name = findViewById(R.id.line_name);
        line_number = (LinearLayout) findViewById(R.id.line_number);
        lin_contact = (LinearLayout) findViewById(R.id.lin_contact);
        lin_number = (LinearLayout) findViewById(R.id.lin_number);
        count_name = (TextView) findViewById(R.id.count_contact);
        count_number = (TextView) findViewById(R.id.count_number);
        proceed = (TextView) findViewById(R.id.proceed_button);
        rescan = (TextView) findViewById(R.id.rescan);

        if (Constants.enable_ads) {
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }

        rescan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionLayout.setVisibility(View.GONE);
                if (viewPager.getCurrentItem() == 0) {
                    if (ContactNameFragment.currentInstance() != null && ContactNameFragment.currentInstance().extractRecords == null) {
                        ContactNameFragment.currentInstance().refresh();
                        count_name.setText("0");
                    } else
                        Toast.makeText(NewDuplicateActivity.this, "Duplicate contact search by name already is in progress", Toast.LENGTH_SHORT).show();
                } else if (viewPager.getCurrentItem() == 1) {
                    if (ContactNumberFragment.currentInstance() != null && ContactNumberFragment.currentInstance().extractRecords == null) {
                        ContactNumberFragment.currentInstance().refresh();
                        count_number.setText("0");
                    } else
                        Toast.makeText(NewDuplicateActivity.this, "Duplicate contact search by number already is in progress", Toast.LENGTH_SHORT).show();
                }
            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (page) {
                    case 1:
                        if (!name)
                            showShareDialog();
                        break;
                    case 2:
                        if (!number)
                            showShareDialog();
                        break;
                    default:
                        Log.d("defaultcase", "NewDuplicateActivity " + page);
                }
            }
        });
        lin_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 1;
                viewPager.setCurrentItem(0);
//                line_name.setVisibility(View.VISIBLE);
//                line_number.setVisibility(View.INVISIBLE);
            }
        });
        lin_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 2;
                viewPager.setCurrentItem(1);
//                line_name.setVisibility(View.INVISIBLE);
//                line_number.setVisibility(View.VISIBLE);
            }
        });
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(2);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mPagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                if (position == 0) {
                    if (ContactNameFragment.currentInstance() != null && ContactNameFragment.currentInstance().listItems.size() == 0)
                        actionLayout.setVisibility(View.GONE);
                    else
                        actionLayout.setVisibility(View.VISIBLE);
                    line_name.setVisibility(View.VISIBLE);
                    line_number.setVisibility(View.INVISIBLE);
                } else {
                    if (ContactNumberFragment.currentInstance() != null && ContactNumberFragment.currentInstance().listItems.size() == 0)
                        actionLayout.setVisibility(View.GONE);
                    else
                        actionLayout.setVisibility(View.VISIBLE);
                    line_name.setVisibility(View.VISIBLE);
                    line_name.setVisibility(View.INVISIBLE);
                    line_number.setVisibility(View.VISIBLE);
                }
                // Check if this is the page you want.
            }
        });
        lin_contact.performClick();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (namecontact != null) {
            namecontact.cancel(true);
            namecontact = null;
            Toast.makeText(this, "Merge operation proportionally completed", Toast.LENGTH_SHORT).show();
        }
        if (numberContact != null) {
            numberContact.cancel(true);
            numberContact = null;
            Toast.makeText(this, "Merge operation proportionally completed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ContactNumberFragment.clearStaticInstance();
        ContactNameFragment.clearStaticInstance();
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
        finish();
//        this.finish();

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return ContactNameFragment.getInstance();
            else
                return ContactNumberFragment.getInstance();
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    public void showShareDialog() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.common_share_dialog);

        dialog.setCancelable(true);
        Button btn_share_fb = (Button) dialog.findViewById(R.id.btn_share_fb);
        Button btn_close = dialog.findViewById(R.id.close_alert);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                mergeDialog();
            }
        });
        btn_share_fb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentTitle(getString(R.string.app_name_with_space))
                                .setContentDescription("Good News...!!! Smart Contact Manager.\nIt's FREE for few days.\n" + "Download " + Constants.smartLink)
                                .setContentUrl(Uri.parse(Constants.smartLink))
                                .build();

                        shareDialog.show(linkContent);
                    }

                    shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            if (dialog != null && dialog.isShowing())
                                dialog.dismiss();

                            if (pinPrefs.getString("merge", "").equalsIgnoreCase("set")) {
                                showAlertDialog();
                            } else {
                                mergeDialog();
                            }
                        }

                        @Override
                        public void onCancel() {
                            if (pinPrefs.getString("merge", "").equalsIgnoreCase("set")) {
                                showAlertDialog();
                            } else {
                                mergeDialog();
                            }
                        }

                        @Override
                        public void onError(FacebookException e) {
                            e.printStackTrace();
                        }
                    });

                }
            }
        });
        final Button btn_whats_app_share = (Button) dialog.findViewById(R.id.btn_share_whatsapp);

        btn_whats_app_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageInfo pInfo = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    Log.d("TAG", "Exception in whatsapp share = " + e);
                    e.printStackTrace();
                }
                final String versionName = pInfo.versionName;
                // TODO Auto-generated method stub
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.setPackage(WHATSSAP_APP_ID);
                intent.putExtra(Intent.EXTRA_TEXT, "Good News...!!! Smart Contact Manager.\nIt's FREE for few days.\n" + "Download " + Constants.smartLink1);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (pinPrefs.getString("merge", "").equalsIgnoreCase("set")) {
                            showAlertDialog();
                        } else {
                            mergeDialog();
                        }
                    }
                }, 1000);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=" +
                                    WHATSSAP_APP_ID)));
                } catch (Exception e) {
                    Toast.makeText(NewDuplicateActivity.this, "Your phone does not support this feature", Toast.LENGTH_LONG).show();
                }
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });


        dialog.show();
    }

    public void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(NewDuplicateActivity.this);
        builder.setTitle("Caution");
        builder.setCancelable(false);
        builder.setMessage("We strongly recommend you to take backup of contacts before proceeding with duplicate contact merge. Would you like to take backup now ?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (emailPrefs.getInt("total backups", 0) >= 3) {
                    final Dialog dialog = new Dialog(NewDuplicateActivity.this);
                    if (dialog.getWindow() != null)
                        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawableResource(R.drawable.app_sharer_back);
                    dialog.setContentView(R.layout.take_back_dialog);

                    TextView ok = (TextView) dialog.findViewById(R.id.ok_backup);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (dialog != null && dialog.isShowing())
                                dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    TakeBackup takeBackup = new TakeBackup();
                    takeBackup.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    backupDialog = new Dialog(NewDuplicateActivity.this);
                    if (backupDialog.getWindow() != null)
                        backupDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    backupDialog.getWindow().setBackgroundDrawableResource(R.drawable.custom_loader_background);
                    backupDialog.setCancelable(false);
                    backupDialog.setCanceledOnTouchOutside(false);
                    backupDialog.setContentView(R.layout.custom_loader);
                    message = (TextView) backupDialog.findViewById(R.id.msg);
                    backupDialog.show();
                }
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                mergeDialog();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void mergeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(NewDuplicateActivity.this);
        builder.setTitle("Merge Contacts ?");
        builder.setCancelable(false);
        builder.setMessage("Do you want to proceed with merge ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (viewPager.getCurrentItem() == 0) {
                    if (ContactNameFragment.currentInstance() != null && ContactNameFragment.currentInstance().listItems != null) {
                        namecontact = new MergeDeleteNameContacts(ContactNameFragment.currentInstance().listItems);
                        namecontact.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                } else if (viewPager.getCurrentItem() == 1) {
                    if (ContactNumberFragment.currentInstance() != null && ContactNumberFragment.currentInstance().listItems != null) {
                        numberContact = new MergeDeleteNumberContacts(ContactNumberFragment.currentInstance().listItems);
                        numberContact.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }

               /* allContactsMergerDialog = new Dialog(NewDuplicateActivity.this);
                allContactsMergerDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                allContactsMergerDialog.getWindow().setBackgroundDrawableResource(R.drawable.custom_loader_background);
                allContactsMergerDialog.setCancelable(false);
                allContactsMergerDialog.setCanceledOnTouchOutside(false);
                allContactsMergerDialog.setContentView(R.layout.custom_loader);
                //message = (TextView) allContactsMergerDialog.findViewById(R.id.msg);
                allContactsMergerDialog.show();*/
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(NewDuplicateActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private class MergeDeleteNameContacts extends AsyncTask<Void, String, Void> {
        ArrayList<Contact> temp = new ArrayList<>();
        ArrayList<Contact> mergeEyeContacts = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (ContactNameFragment.currentInstance() != null)
                ContactNameFragment.currentInstance().progress_message.setText("Please wait...");
        }

        public MergeDeleteNameContacts(ArrayList<Contact> allContacts) {
            temp = allContacts;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            name = true;
            ArrayList<Contact> deleteStatus = new ArrayList<Contact>();
            ArrayList<String> keyArray = new ArrayList<String>();
            for (Contact contact : new ArrayList<>(temp)) {
                if (!contact.isChecked)
                    temp.remove(contact);
            }
            ArrayList<Contact> subchildList;
            for (Contact contact : new ArrayList<>(temp)) {
                long percentage = 0;
                if (!temp.isEmpty())
                    percentage = ((long) temp.indexOf(contact) * 50 / (long) temp.size());
                publishProgress("Merging contact \n" + percentage + "% completed");
                subchildList = new ArrayList<Contact>();
                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, contact.getLookup());
                try {
                    if (getApplicationContext().getContentResolver().delete(uri, null, null) == 0)
                        deleteStatus.add(contact);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!selectedMapData.containsKey(contact.getName())) {
                    subchildList.add(contact);
                    keyArray.add(contact.getName());
                    selectedMapData.put(contact.getName(), subchildList);
                } else {
                    subchildList = selectedMapData.get(contact.getName());
                    subchildList.add(contact);
                    keyArray.add(contact.getName());
                    selectedMapData.put(contact.getName(), subchildList);
                }
            }

            ArrayList<String> contactsListFinal, emailsListFinal, contactsList, emailsList;
            ArrayList<Contact> unSelectedContact;
            for (String key : selectedMapData.keySet()) {
                long strt = 50;
                long percentage = 0;
                if (!temp.isEmpty())
                    percentage = ((long) keyArray.indexOf(key) * 50 / (long) temp.size());
                percentage = percentage + strt;
                Bitmap bitmap = null;
                publishProgress("Merging contact \n" + percentage + "% completed");
                ArrayList<Contact> children = selectedMapData.get(key);
                contactsListFinal = new ArrayList<String>();
                emailsListFinal = new ArrayList<String>();
                unSelectedContact = new ArrayList<Contact>();
                for (Contact contact : children) {
                    contactsList = contact.getContact();
                    emailsList = contact.getEmail();
                    if (contactsList != null) {
                        for (String contacts : contactsList) {
                            if (!contactsListFinal.contains(contacts))
                                contactsListFinal.add(contacts);
                        }
                    }
                    if (emailsList != null) {
                        for (String emails : emailsList) {
                            if (!emailsListFinal.contains(emails))
                                emailsListFinal.add(emails);
                        }
                    }
                    if (!contact.isShouldCheck())
                        unSelectedContact.add(contact);
                }
                if (unSelectedContact.size() > 0) {
                    for (Contact contact : unSelectedContact)
                        if (contact.getBitmap() != null)
                            bitmap = contact.getBitmap();
                    Contact meregeContacts = new Contact();
                    meregeContacts.setName(key);
                    meregeContacts.setBitmap(bitmap);
                    meregeContacts.setContact(contactsListFinal);
                    meregeContacts.setEmail(emailsListFinal);
                    mergeEyeContacts.add(meregeContacts);
                    addContact(key, contactsListFinal, emailsListFinal, bitmap);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (ContactNameFragment.currentInstance() != null) {
                ContactNameFragment.currentInstance().progress_message.setText("Contacts merged succesfully");
                ContactNameFragment.currentInstance().progressbar.setVisibility(View.GONE);
                ContactNameFragment.currentInstance().listItems.clear();
            }
            name = false;
            actionLayout.setVisibility(View.GONE);
            count_name.setText("0");
            //count_name.setVisibility(View.GONE);
            if (allContactsMergerDialog != null && allContactsMergerDialog.isShowing())
                allContactsMergerDialog.dismiss();

            // listView.setVisibility(View.GONE);
//            listView2.setVisibility(View.GONE);

            // proceedView.setVisibility(View.GONE);
            final Dialog eyeDialog = new Dialog(NewDuplicateActivity.this);
            eyeDialog.setTitle("Merged Contacts");
            eyeDialog.setCancelable(false);

            eyeDialog.setContentView(R.layout.eye_listview);
            ListView eyeListview = (ListView) eyeDialog.findViewById(R.id.eye_listView);
            MergedContactNameAdapter adapter = new MergedContactNameAdapter(mergeEyeContacts, NewDuplicateActivity.this);
            eyeListview.setAdapter(adapter);

            TextView okButton = (TextView) eyeDialog.findViewById(R.id.ok_button);
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (eyeDialog != null && eyeDialog.isShowing())
                        eyeDialog.dismiss();

//                    Intent intent = new Intent(NewDuplicateActivity.this, MainActivity.class);
//                    startActivity(intent);
//                    finish();
                }
            });

            eyeDialog.show();
            namecontact = null;
//            rescanView.setVisibility(View.VISIBLE);

        }

        @Override
        protected void onProgressUpdate(String... values) {
            //  message.setText(values[0]);
            //Log.d("OK",values[0]+"");
            if (ContactNameFragment.currentInstance() != null && ContactNameFragment.currentInstance().linearLayout.getVisibility() == View.GONE) {
                ContactNameFragment.currentInstance().linearLayout.setVisibility(View.VISIBLE);
                ContactNameFragment.currentInstance().progressbar.setVisibility(View.VISIBLE);
            }
            if (ContactNameFragment.currentInstance() != null)
                ContactNameFragment.currentInstance().progress_message.setText((values[0]));
            super.onProgressUpdate(values);
        }

    }

    private class MergeDeleteNumberContacts extends AsyncTask<Void, String, Void> {
        ArrayList<Contact> temp = new ArrayList<>();
        ArrayList<Contact> mergeEyeContacts = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (ContactNumberFragment.currentInstance() != null)
                ContactNumberFragment.currentInstance().progress_message.setText("Please wait...");
        }

        public MergeDeleteNumberContacts(ArrayList<Contact> allContacts) {
            temp = allContacts;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            number = true;
            ArrayList<Contact> deleteStatus = new ArrayList<Contact>();
            ArrayList<String> keyArray = new ArrayList<String>();
            for (Contact contact : new ArrayList<>(temp)) {
                if (!contact.isChecked)
                    temp.remove(contact);
            }
            ArrayList<Contact> subchildList;
            for (Contact contact : new ArrayList<>(temp)) {
                long percentage = 0;
                if (!temp.isEmpty())
                    percentage = ((long) temp.indexOf(contact) * 50 / (long) temp.size());
                publishProgress("Merging contact \n" + percentage + "% completed");
                subchildList = new ArrayList<Contact>();
                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, contact.getLookup());
                try {
                    if (getApplicationContext().getContentResolver().delete(uri, null, null) == 0)
                        deleteStatus.add(contact);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!selectedMapData2.containsKey(contact.getDisplay_name())) {
                    subchildList.add(contact);
                    keyArray.add(contact.getName());
                    selectedMapData2.put(contact.getDisplay_name(), subchildList);
                } else {
                    subchildList = selectedMapData2.get(contact.getDisplay_name());
                    subchildList.add(contact);
                    keyArray.add(contact.getDisplay_name());
                    selectedMapData2.put(contact.getDisplay_name(), subchildList);
                }
            }

            ArrayList<String> contactsListFinal, emailsListFinal, contactsList, emailsList;
            ArrayList<Contact> unSelectedContact;
            for (String key : selectedMapData2.keySet()) {
                long strt = 50;
                long percentage = 0;
                if (!temp.isEmpty())
                    percentage = ((long) keyArray.indexOf(key) * 50 / (long) temp.size());
                percentage = percentage + strt;
                Bitmap bitmap = null;
                publishProgress("Merging contact \n" + percentage + "% completed");
                ArrayList<Contact> children = selectedMapData2.get(key);
                contactsListFinal = new ArrayList<String>();
                emailsListFinal = new ArrayList<String>();
                unSelectedContact = new ArrayList<Contact>();
                String contactName = "";
                for (Contact contact : children) {
                    contactsList = contact.getContact();
                    emailsList = contact.getEmail();
                    contactName = contact.getName();
                    if (contactsList != null) {
                        for (String contacts : contactsList) {
                            if (!contactsListFinal.contains(contacts))
                                contactsListFinal.add(contacts);
                        }
                    }
                    if (emailsList != null) {
                        for (String emails : emailsList) {
                            if (!emailsListFinal.contains(emails))
                                emailsListFinal.add(emails);
                        }
                    }
                    if (!contact.isShouldCheck())
                        unSelectedContact.add(contact);
                }
                if (unSelectedContact.size() > 0) {
                    for (Contact contact : unSelectedContact)
                        if (contact.getBitmap() != null)
                            bitmap = contact.getBitmap();
                    Contact meregeContacts = new Contact();
                    meregeContacts.setName(contactName);
                    meregeContacts.setBitmap(bitmap);
                    meregeContacts.setContact(contactsListFinal);
                    meregeContacts.setEmail(emailsListFinal);
                    mergeEyeContacts.add(meregeContacts);
                    addContact(contactName, contactsListFinal, emailsListFinal, bitmap);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            number = false;
            if (ContactNumberFragment.currentInstance() != null) {
                ContactNumberFragment.currentInstance().progress_message.setText("Contacts merged succesfully");
                ContactNumberFragment.currentInstance().listItems.clear();
                ContactNumberFragment.currentInstance().progressbar.setVisibility(View.GONE);
            }
            count_number.setText("0");
            //count_number.setVisibility(View.GONE);
            actionLayout.setVisibility(View.GONE);
            if (allContactsMergerDialog != null && allContactsMergerDialog.isShowing())
                allContactsMergerDialog.dismiss();

            // listView.setVisibility(View.GONE);
//            listView2.setVisibility(View.GONE);

            // proceedView.setVisibility(View.GONE);
            final Dialog eyeDialog = new Dialog(NewDuplicateActivity.this);
            eyeDialog.setTitle("Merged Contacts");
            eyeDialog.setCancelable(false);

            eyeDialog.setContentView(R.layout.eye_listview);
            ListView eyeListview = (ListView) eyeDialog.findViewById(R.id.eye_listView);
            EyeNumberAdapter adapter = new EyeNumberAdapter(mergeEyeContacts, NewDuplicateActivity.this);
            eyeListview.setAdapter(adapter);

            TextView okButton = (TextView) eyeDialog.findViewById(R.id.ok_button);
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (eyeDialog != null && eyeDialog.isShowing())
                        eyeDialog.dismiss();
                }
            });
            eyeDialog.show();
            numberContact = null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            if (ContactNumberFragment.currentInstance() != null && ContactNumberFragment.currentInstance().linearLayout.getVisibility() == View.GONE) {
                ContactNumberFragment.currentInstance().linearLayout.setVisibility(View.VISIBLE);
                ContactNumberFragment.currentInstance().progressbar.setVisibility(View.GONE);
            }
            if (ContactNumberFragment.currentInstance() != null)
                ContactNumberFragment.currentInstance().progress_message.setText((values[0]));
            super.onProgressUpdate(values);
        }
    }

    private void addContact(String name, ArrayList<String> phone, ArrayList<String> email, Bitmap mBitmap) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        int rawContactID = ops.size();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());
        if (name == null)
            name = "";
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name)
                .build());
        if (phone == null)
            phone = new ArrayList<>();
        for (String numberAtIndex : phone) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, numberAtIndex)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }
        if (email == null)
            email = new ArrayList<>();
        for (String emailAtIndex : email) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, emailAtIndex)
                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                    .build());
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (mBitmap != null) {
            mBitmap.compress(Bitmap.CompressFormat.PNG, 75, stream);
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, stream.toByteArray())
                    .build());
            try {
                stream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            getApplicationContext().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private class TakeBackup extends AsyncTask<String, String, Long> {
        @Override
        protected Long doInBackground(String... strings) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmm", Locale.US);
                String currentDateandTime = sdf.format(new Date());
                String vfile = "SCM" + user_id + "_" + currentDateandTime + ".vcf";
                int counterAtIndex = 0;
                ArrayList<String> vCard = new ArrayList<String>();
                Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                if (cursor != null && cursor.getCount() > 0) {
                    final int totalContact = cursor.getCount();
                    int i;
                    try {
                        String folder_path = getFolderPath(NewDuplicateActivity.this);
                        File file = new File(folder_path);
                        boolean success = true;
                        if (!file.exists()) {
                            success = file.mkdir();
                        }
                        if (success) {
                            storage_path = folder_path + vfile;
                            FileOutputStream mFileOutputStream;
                            mFileOutputStream = new FileOutputStream(storage_path, false);
                            cursor.moveToFirst();
                            for (i = 0; i < cursor.getCount(); i++) {
                                counterAtIndex++;
                                long percentage = ((long) counterAtIndex * 100 / (long) totalContact);
                                publishProgress("Backup " + percentage + "% completed");

                                String lookupKey = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);
                                String vcardstring = "";
                                try {
                                    AssetFileDescriptor fd;
                                    fd = NewDuplicateActivity.this.getContentResolver().openAssetFileDescriptor(uri, "r");
                                    FileInputStream fis = fd.createInputStream();
                                    byte[] buf = new byte[(int) fd.getDeclaredLength()];
                                    fis.read(buf);
                                    vcardstring = new String(buf);
                                    fd.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                List<Email> email;
                                List<Address> address;
                                List<Note> note;
                                List<Url> url;

                                VCard vCard1 = Ezvcard.parse(vcardstring).first();
                                if (pinPrefs.getString("emailCheckbox", "").equalsIgnoreCase("checked") && pinPrefs.getString("addressCheckbox", "").equalsIgnoreCase("checked")
                                        && pinPrefs.getString("noteCheckbox", "").equalsIgnoreCase("checked") && pinPrefs.getString("urlCheckbox", "").equalsIgnoreCase("checked")) {
                                    vCard.add(vcardstring);
                                } else {
                                    if (pinPrefs.getString("emailCheckbox", "").equalsIgnoreCase("unchecked")) {
                                        email = vCard1.getEmails();
                                        email.clear();
                                        vCard1.removeProperties(Email.class);
                                    }
                                    if (pinPrefs.getString("addressCheckbox", "").equals("unchecked")) {
                                        address = vCard1.getAddresses();

                                        address.clear();
                                        vCard1.removeProperties(Address.class);
                                    }
                                    if (pinPrefs.getString("noteCheckbox", "").equalsIgnoreCase("unchecked")) {
                                        note = vCard1.getNotes();

                                        note.clear();
                                        vCard1.removeProperties(Note.class);
                                    }
                                    if (pinPrefs.getString("urlCheckbox", "").equalsIgnoreCase("unchecked")) {
                                        url = vCard1.getUrls();

                                        url.clear();
                                        vCard1.removeProperties(Url.class);
                                    }
                                    String unselectedData = Ezvcard.write(vCard1).version(VCardVersion.V3_0).go();
                                    vCard.add(unselectedData);
                                }
                                cursor.moveToNext();
                                mFileOutputStream.write(vCard.get(i).getBytes());
                            }
                            mFileOutputStream.close();

                            cursor.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //message.setText(values[0]);
            message.setText(values[0]);
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            if (backupDialog != null && backupDialog.isShowing() && !isFinishing()) {
                backupDialog.dismiss();
            }
            sendEmail();
        }
    }

    public static String getFolderPath(Context context) {
        if (isSdPresent()) {
            try {
                File sdPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SCMContactBackup");
                if (!sdPath.exists()) {
                    sdPath.mkdirs();
                    folderPath = sdPath.getAbsolutePath();
                } else if (sdPath.exists()) {
                    folderPath = sdPath.getAbsolutePath();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            folderPath = Environment.getExternalStorageDirectory().getPath() + "/SCMContactBackup/";
        } else {
            try {
                File cacheDir = new File(context.getCacheDir(), "SCMContactBackup/");
                if (!cacheDir.exists()) {
                    cacheDir.mkdirs();
                    folderPath = cacheDir.getAbsolutePath();
                } else if (cacheDir.exists()) {
                    folderPath = cacheDir.getAbsolutePath();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return folderPath;
    }

    public static boolean isSdPresent() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    public void sendEmail() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.contact_backup_dialog);
        dialog.setCancelable(false);

        final EditText userInput = (EditText) dialog.findViewById(R.id.edt_email_id);
        userInput.setText(prefs.getString("email-id", ""));
        userInput.setEnabled(false);

        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing() && NewDuplicateActivity.this != null && !NewDuplicateActivity.this.isFinishing())
                    dialog.dismiss();
            }
        });

        final Button btn_send = (Button) dialog.findViewById(R.id.btn_send);
        if (userInput.getText().toString().equalsIgnoreCase("")) {
            btn_send.setEnabled(false);
        }

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search_label = userInput.getText().toString();
                if (search_label.length() > 0 && !search_label.equalsIgnoreCase(" ")) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();
                    if (!storage_path.equals("")) {
                        uploadFiles = new UploadFiles(storage_path, userInput.getText().toString(), NewDuplicateActivity.this);
                        uploadFiles.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        Toast.makeText(NewDuplicateActivity.this, "File not created, please try again!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(NewDuplicateActivity.this, "Email id required", Toast.LENGTH_SHORT).show();
                }
            }
        });

        userInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                if (userInput.getText().toString().length() > 0 && userInput.getText().toString().contains("@") && userInput.getText().toString().contains(".")) {
                    btn_send.setEnabled(true);
                } else {
                    btn_send.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
    }

    private class UploadFiles extends AsyncTask<String, Integer, Long> {
        String imagePath, email;
        NewDuplicateActivity activity;
        ProgressDialog uploadDialog;

        public UploadFiles(String imagePath, String email, NewDuplicateActivity activity) {
            this.imagePath = imagePath;
            this.activity = activity;
            this.email = email;
            uploadDialog = new ProgressDialog(activity);
        }

        protected void onPreExecute() {
            if (uploadDialog != null) {
                uploadDialog.setMessage("Uploading please wait.Please use 3G/WIFI for better performance.");
                uploadDialog.show();
            }
        }

        @Override
        protected Long doInBackground(String... strings) {

            // TODO Auto-generated method stub
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(Constants.BASEURL + "api_home/do_contact_backup_upload_oc/2/android");
            File file = new File(imagePath);
            MultipartEntity reqEntity = new MultipartEntity();
            FileBody fileBody = new FileBody(file);
            reqEntity.addPart("userfile", fileBody);
            try {
                reqEntity.addPart("user_id", new StringBody(user_id));
                reqEntity.addPart("email", new StringBody(email));
            } catch (UnsupportedEncodingException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            post.setEntity(reqEntity);
            try {
                HttpResponse response = client.execute(post);
                HttpEntity responseEntity = response.getEntity();
//                final String response_str = EntityUtils
//                        .toString(responseEntity);
                if (responseEntity != null) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                File file = new File(imagePath);
                                try {
                                    if (file.exists()) {
                                        file.delete();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            if (uploadDialog != null && uploadDialog.isShowing()) {
                uploadDialog.dismiss();
            }
            if (!isFinishing()) {
                mergeDialog();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            Toast.makeText(NewDuplicateActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
package techathalon.com.smartcontactmanager.DUPLICATE_SCAN;


import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class ContactNumberFragment extends Fragment {

    public ContactNumberFragment() {
    }

    public static ContactNumberFragment contactNameFragment;
    TreeMap<String, ArrayList<Contact>> contactData = new TreeMap<>();
    ArrayList<Contact> contactArrayList = new ArrayList<>();
    ListView listView;
    TextView msg, progress_message;
    Activity activity;
    LinearLayout linearLayout;
    public ArrayList<Contact> listItems = new ArrayList<>();
    DuplicateNumberAdapter duplicateContactAdapter1;
    ExtractRecords extractRecords;
    ProgressBar progressbar;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (Activity) context;
    }

    public static ContactNumberFragment getInstance() {
        contactNameFragment = new ContactNumberFragment();
        return contactNameFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public static ContactNumberFragment currentInstance() {
        return contactNameFragment;
    }

    public static void clearStaticInstance() {
        contactNameFragment = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layoutView = inflater.inflate(R.layout.fragment_contact_number, container, false);
        if (Constants.enable_tracker) {
            SCMApplication.getInstance().trackScreenView("Duplicate Scan by Number");
        }
        listView = (ListView) layoutView.findViewById(R.id.listView);
        progressbar = (ProgressBar) layoutView.findViewById(R.id.progressBar1);
        linearLayout = (LinearLayout) layoutView.findViewById(R.id.progressLayout);
        msg = (TextView) layoutView.findViewById(R.id.msg);
        progress_message = (TextView) layoutView.findViewById(R.id.message);
        duplicateContactAdapter1 = new DuplicateNumberAdapter(activity, contactNameFragment, listItems);
        listView.setAdapter(duplicateContactAdapter1);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Contact contact = listItems.get(position);
                if (contact.isChecked)
                    contact.setChecked(false);
                else
                    contact.setChecked(true);
                for (Contact contact1 : listItems) {
                    if (contact.getLookup().equals(contact1.getLookup())) {
                        contact1.setChecked(contact.isChecked);
                    }
                }
                duplicateContactAdapter1.notifyDataSetChanged();
            }
        });
        refresh();
        // customLoader1.show(activity.getFragmentManager(), "");
        return layoutView;
    }

    public void refresh() {

        if (extractRecords != null) {
            extractRecords.cancel(true);
            extractRecords = null;
        }
        progress_message.setText("Please wait...");
        extractRecords = new ExtractRecords();
        extractRecords.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class ExtractRecords extends AsyncTask<Void, String, TreeMap<String, ArrayList<Contact>>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linearLayout.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            contactData.clear();
            listItems.clear();
            duplicateContactAdapter1.notifyDataSetChanged();
            // (activity.findViewById(R.id.rescan)).setEnabled(false);
        }


        public boolean checkForID(String number, String id) {
            try {
                if (contactData != null && contactData.containsKey(number)) {
                    ArrayList<Contact> arrayList = contactData.get(number);
                    if (arrayList != null) {
                        for (Contact Contact : arrayList) {
                            if (Contact.getDisplay_name().trim().equals(number.trim()) && Contact.getId().trim().equals(id))
                                return true;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected TreeMap<String, ArrayList<Contact>> doInBackground(Void... voids) {
            try {
                ContentResolver cr = activity.getContentResolver();
                int count = 0;

                Cursor cur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                int counterAtIndex = 0;

                if (cur != null && cur.getCount() > 0) {
                    count = cur.getCount();
                    while (cur != null && cur.moveToNext()) {
                        String name = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                        if (name == null)
                            name = "";

                        String phoneNumber = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String id = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
                        String contact_id = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                        String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY));
                        if (phoneNumber == null)
                            phoneNumber = "";
                        if (contact_id == null)
                            contact_id = "";
//                    Log.d("cursor", name + ":" + phoneNumber + ":" + id + ":" + contact_id + ":" + lookupKey);
                        ArrayList<Contact> data = new ArrayList<>();
                        if (!checkForID(phoneNumber, contact_id)) {
                            counterAtIndex++;
                            long percentage = ((long) counterAtIndex * 100 / (long) count);
                            publishProgress("Scanning " + percentage + "% completed");
                            ArrayList nameList = new ArrayList();

                            if (name != null)
                                nameList.add(name);
                            else
                                nameList.add("");

                            Contact contact = new Contact();
                            contact.setChecked(true);
                            contact.setDisplay_name(phoneNumber);
                            contact.setName(name);
                            contact.setLookup(lookupKey);
                            contact.setId(contact_id);
                            ArrayList<String> emailArray = new ArrayList<String>();
                            Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{contact_id}, null);
                            if (emailCur != null) {
                                while (emailCur.moveToNext()) {
                                    String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                                    if (email != null) {
                                        emailArray.add(email);

                                        if (emailArray != null)
                                            contact.setEmail(emailArray);
                                        else
                                            contact.setEmail(new ArrayList<String>());
                                    }
                                }
                                emailCur.close();
                            }
                            if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                                Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                        new String[]{contact_id}, null);
                                if (pCur != null) {
                                    ArrayList<String> numberlist = new ArrayList<>();
                                    while (pCur.moveToNext()) {
                                        String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                        if (phone != null)
                                            numberlist.add(phone);
                                    }
                                    contact.setContact(numberlist);
                                    pCur.close();
                                }
                            }
                            Bitmap bitmap = null;
                            InputStream photo_stream = null;
                            BufferedInputStream buf = null;
                            try {
                                Uri my_contact_Uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contact_id));
                                if (my_contact_Uri != null) {
                                    photo_stream = ContactsContract.Contacts.openContactPhotoInputStream(activity.getContentResolver(), my_contact_Uri);

                                    if (photo_stream != null)
                                        buf = new BufferedInputStream(photo_stream);
                                    bitmap = BitmapFactory.decodeStream(buf);
                                    if (buf != null)
                                        buf.close();
                                    if (photo_stream != null)
                                        photo_stream.close();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            contact.setBitmap(bitmap);
                            if (contact != null)
                                data.add(contact);
                            if (contact.getDisplay_name() != null) {
                                if (!contactData.containsKey(contact.getDisplay_name())) {
                                    contactData.put(contact.getDisplay_name(), data);
                                } else {
                                    ArrayList<Contact> newList = contactData.get(contact.getDisplay_name());
                                    newList.add(contact);
                                    contactData.put(contact.getDisplay_name(), newList);
                                }
                            }
                        }
                    }
                }

                cur.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return contactData;

        }

        @Override
        protected void onProgressUpdate(String... values) {
            if (ContactNumberFragment.currentInstance() != null)
                ContactNumberFragment.currentInstance().progress_message.setText(values[0]);
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(TreeMap<String, ArrayList<Contact>> contacts) {
            super.onPostExecute(contacts);
            //(activity.findViewById(R.id.rescan)).setEnabled(true);
            Iterator myIterator = contacts.keySet().iterator();
            long count = 0;
            listItems.clear();
            contactArrayList.clear();
            while (myIterator.hasNext()) {
                String key = myIterator.next().toString();
                contactArrayList = contacts.get(key);

                if (contactArrayList.size() > 1) {
                    for (int i = 0; i < contactArrayList.size(); i++) {
                        Contact contact = contactArrayList.get(i);
                        if (contactArrayList.indexOf(contact) == 0) {
                            contact.setShouldCheck(true);
                            count = count + 1;
                        } else {
                            contact.setShouldCheck(false);
                        }
                        listItems.add(contact);
                    }
                }
            }
            duplicateContactAdapter1.notifyDataSetChanged();
            listView.setVisibility(View.VISIBLE);
            //TODO handled illegal state exception
            listView.requestLayout();
            linearLayout.setVisibility(View.GONE);

            TextView txt_contact_img = (TextView) activity.findViewById(R.id.count_number);
            txt_contact_img.setText(count + "");
            if (listItems.size() == 0) {
                txt_contact_img.setVisibility(View.GONE);
                msg.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
            } else {
                txt_contact_img.setVisibility(View.VISIBLE);
                if (((NewDuplicateActivity) activity).viewPager.getCurrentItem() == 1)
                    ((NewDuplicateActivity) activity).actionLayout.setVisibility(View.VISIBLE);
            }
            extractRecords = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (extractRecords != null) {
            extractRecords.cancel(true);
            extractRecords = null;
        }
    }
}
package techathalon.com.smartcontactmanager.DUPLICATE_SCAN;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import techathalon.com.smartcontactmanager.HELPER_FILES.Constants;
import techathalon.com.smartcontactmanager.R;
import techathalon.com.smartcontactmanager.app.SCMApplication;

public class ContactNameFragment extends Fragment {
    TreeMap<String, ArrayList<Contact>> contactData = new TreeMap<>();
    ArrayList<Contact> contactArrayList = new ArrayList<>();
    ListView listView;
    public ArrayList<Contact> listItems = new ArrayList<>();
    Activity activity;
    TextView msg, progress_message;
    public static ContactNameFragment contactNameFragment;
    LinearLayout linearLayout;
    DuplicateNameAdapter duplicateContactAdapter1;
    ExtractRecords extractRecords;
    ProgressBar progressbar;

    public static ContactNameFragment getInstance() {
        contactNameFragment = new ContactNameFragment();
        return contactNameFragment;
    }

    public static ContactNameFragment currentInstance() {
        return contactNameFragment;
    }

    public ContactNameFragment() {
    }

    public static void clearStaticInstance() {
        contactNameFragment = null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (Activity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (extractRecords != null) {
            extractRecords.cancel(true);
            extractRecords = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (Constants.enable_tracker) {
            SCMApplication.getInstance().trackScreenView("Duplicate Scan by Name");
        }
        View layoutView = inflater.inflate(R.layout.fragment_contact_name, container, false);
        listView = (ListView) layoutView.findViewById(R.id.listView);
        progressbar = (ProgressBar) layoutView.findViewById(R.id.progressBar1);
        linearLayout = (LinearLayout) layoutView.findViewById(R.id.progressLayout);
        msg = (TextView) layoutView.findViewById(R.id.msg);
        progress_message = (TextView) layoutView.findViewById(R.id.message);
        duplicateContactAdapter1 = new DuplicateNameAdapter(activity, contactNameFragment, listItems);

        listView.setAdapter(duplicateContactAdapter1);
        listView.setVisibility(View.GONE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Contact contact = listItems.get(position);
                if (contact.isChecked)
                    contact.setChecked(false);
                else
                    contact.setChecked(true);
                for (Contact contact1 : listItems) {
                    if (contact.getLookup().equals(contact1.getLookup())) {
                        contact1.setChecked(contact.isChecked);
                    }
                }
                duplicateContactAdapter1.notifyDataSetChanged();
            }
        });

        refresh();
        // customLoader1.show(activity.getFragmentManager(), "");
        return layoutView;
    }

    public void refresh() {
        if (extractRecords != null) {
            extractRecords.cancel(true);
            extractRecords = null;
        }
        progress_message.setText("Please wait...");
        extractRecords = new ExtractRecords();
        extractRecords.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public String makeInitialCapital(String name) {
        if (name != null && !name.trim().equals("")) {
            String[] wordSplit;
            String wordCapital = "";
            wordSplit = name.trim().replace("  ", " ").split(" ");
            for (int i = 0; i < wordSplit.length; i++) {
                try {
                    if (!wordSplit[i].equals(" ") || !wordSplit[i].equals(""))
                        wordCapital += wordSplit[i].substring(0, 1).toUpperCase() + wordSplit[i].substring(1) + " ";
                } catch (Exception e) {
                    Log.d("print", wordSplit.toString());
                }
            }
            return wordCapital;
        } else
            return "";

    }

    private class ExtractRecords extends AsyncTask<Void, String, TreeMap<String, ArrayList<Contact>>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linearLayout.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            contactData.clear();
            listItems.clear();
            duplicateContactAdapter1.notifyDataSetChanged();
            //(activity.findViewById(R.id.rescan)).setEnabled(false);
        }

        @Override
        protected TreeMap<String, ArrayList<Contact>> doInBackground(Void... voids) {
            try {

                ContentResolver cr = activity.getContentResolver();
                int count = 0;
                Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                int counterAtIndex = 0;

                if (cur != null && cur.getCount() > 0) {
                    count = cur.getCount();

                    while (cur != null && cur.moveToNext()) {
                        Contact contact = new Contact();
                        contact.setChecked(true);
                        counterAtIndex++;
                        long percentage = ((long) counterAtIndex * 100 / (long) count);
                        publishProgress("Scanning " + percentage + "% completed");
                        ArrayList<Contact> data = new ArrayList<>();
                        String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = "";
                        name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        if (name == null)
                            name = "";

                        name = makeInitialCapital(name);
                        String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));

                        contact.setName(name);
                        ArrayList<String> emailArray = new ArrayList<String>();
                        ArrayList numberlist = new ArrayList();
                        contact.setId(id);
                        contact.setLookup(lookupKey);
                        Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                        while (emailCur != null && emailCur.moveToNext()) {
                            String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                            if (email != null) {
                                if (!emailArray.contains(email)) {
                                    emailArray.add(email);

                                    if (emailArray != null)
                                        contact.setEmail(emailArray);
                                    else
                                        contact.setEmail(new ArrayList<String>());
                                } else {
                                    Contact newEmailData = new Contact();
                                    newEmailData.setId(contact.getId());
                                    newEmailData.setLookup(contact.getLookup());
                                    newEmailData.setChecked(true);
                                    ArrayList newEmailsList = new ArrayList();
                                    newEmailsList.add(email);
                                    newEmailData.setName(name);

                                    if (newEmailsList != null)
                                        newEmailData.setEmail(newEmailsList);
                                    else
                                        newEmailData.setEmail(new ArrayList<String>());
                                    data.add(newEmailData);
                                }
                            }
                        }

                        emailCur.close();

                        if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                            Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{id}, null);

                            while (pCur.moveToNext()) {
                                String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                if (phone != null) {
                                    if (!numberlist.contains(phone)) {
                                        numberlist.add(phone);

                                        if (numberlist != null)
                                            contact.setContact(numberlist);
                                        else
                                            contact.setContact(new ArrayList<String>());

                                    } else {
                                        Contact newContactList = new Contact();
                                        newContactList.setId(contact.getId());
                                        newContactList.setLookup(contact.getLookup());
                                        newContactList.setChecked(true);
                                        ArrayList newNumberList = new ArrayList();
                                        newNumberList.add(phone);
                                        newContactList.setName(name);

                                        if (newNumberList != null)
                                            newContactList.setContact(newNumberList);
                                        else
                                            newContactList.setContact(new ArrayList<String>());
                                        data.add(newContactList);
                                    }
                                }

                                Bitmap bitmap = null;
                                InputStream photo_stream = null;
                                BufferedInputStream buf = null;

                                try {
                                    Uri my_contact_Uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(id));
                                    if (my_contact_Uri != null) {
                                        if (activity != null)
                                            photo_stream = ContactsContract.Contacts.openContactPhotoInputStream(activity.getContentResolver(), my_contact_Uri);
                                        else
                                            photo_stream = ContactsContract.Contacts.openContactPhotoInputStream(activity.getContentResolver(), my_contact_Uri);

                                        if (photo_stream != null)
                                            buf = new BufferedInputStream(photo_stream);
                                        bitmap = BitmapFactory.decodeStream(buf);
                                        if (buf != null)
                                            buf.close();
                                        if (photo_stream != null)
                                            photo_stream.close();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                contact.setBitmap(bitmap);

                            }
                            pCur.close();
                        }
                        if (contact != null)
                            data.add(contact);

                        if (name != null) {
                            if (!contactData.containsKey(name)) {
                                contactData.put(name, data);
                            } else {
                                ArrayList<Contact> newList = contactData.get(name);
                                newList.add(contact);
                                contactData.put(name, newList);
                            }
                        }
                    }
                }
                cur.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return contactData;

        }

        @Override
        protected void onProgressUpdate(String... values) {
            if (ContactNameFragment.currentInstance() != null)
                ContactNameFragment.currentInstance().progress_message.setText(values[0]);
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(TreeMap<String, ArrayList<Contact>> contacts) {
            super.onPostExecute(contacts);
            long count = 0;
            listItems.clear();
            //(activity.findViewById(R.id.rescan)).setEnabled(true);
            contactArrayList.clear();
            Iterator myIterator = contacts.keySet().iterator();
            while (myIterator.hasNext()) {
                String key = myIterator.next().toString();
                contactArrayList = contacts.get(key);

                if (contactArrayList.size() > 1) {
                    for (int i = 0; i < contactArrayList.size(); i++) {
//                    count = count + 1;
                        Contact contact = contactArrayList.get(i);
                        if (contactArrayList.indexOf(contact) == 0) {
                            contact.setShouldCheck(true);
                            count = count + 1;
                        } else {
                            contact.setShouldCheck(false);
                        }
                        listItems.add(contact);
                    }
                }
            }
            duplicateContactAdapter1.notifyDataSetChanged();

            listView.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.GONE);

            //TODO handled illegal state exception
            listView.requestLayout();
            TextView txt_contact_img = (TextView) activity.findViewById(R.id.count_contact);
            txt_contact_img.setText(count + "");

            if (listItems.size() == 0) {
                txt_contact_img.setVisibility(View.GONE);
                msg.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
            } else {
                txt_contact_img.setVisibility(View.VISIBLE);
                if (((NewDuplicateActivity) activity).viewPager.getCurrentItem() == 0)
                    ((NewDuplicateActivity) activity).actionLayout.setVisibility(View.VISIBLE);
            }
            extractRecords = null;
        }
    }
}